import { Enums, ServiceEnums, PromiseTypes, ServiceTypes, Types as CoreTypes } from '../../../bundles/core';
import AbortablePromise = PromiseTypes.AbortablePromise;

export namespace EmployeeServiceTypes { 
    export namespace Requests {
        export interface All extends ServiceTypes.CoreRequestBase {
            IncludeInactive?: boolean;
        }
        export interface ById extends ServiceTypes.CoreRequestBase {
            EmployeeSid: number;
        }
        export interface ByIds extends ServiceTypes.CoreRequestBase {
            EmployeeSids: number[];
        }
    }
    export namespace Responses {
        export interface All extends ServiceTypes.CoreResponseBase_<CoreTypes.EmployeeBase[]> {}
        export interface ById extends ServiceTypes.CoreResponseBase_<CoreTypes.EmployeeBase> {}
        export interface ByIds extends ServiceTypes.CoreResponseBase_<CoreTypes.EmployeeBase[]> {}
    }
    export interface IEmployeeService {
        All?: (request: Requests.All) => AbortablePromise<Responses.All>;
        ById?: (request: Requests.ById) => AbortablePromise<Responses.ById>;
        ByIds?: (request: Requests.ByIds) => AbortablePromise<Responses.ByIds>;
    }
}
