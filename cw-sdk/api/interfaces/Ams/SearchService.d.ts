import { Enums, ServiceEnums, PromiseTypes, ServiceTypes, Types as CoreTypes } from '../../../bundles/core';
import AbortablePromise = PromiseTypes.AbortablePromise;

export namespace SearchServiceTypes { 
    export namespace Requests {
        export interface Definition extends ServiceTypes.CoreRequestBase {
            EmployeeSid?: number;
            SearchId: number;
        }
        export interface Definitions extends ServiceTypes.CoreRequestBase {
            EmployeeSid?: number;
            SearchIds: number[];
        }
        export interface Execute extends ServiceTypes.CoreRequestBase {
            EmployeeSid?: number;
            ExcludeEmptyXY?: boolean;
            Extent?: CoreTypes.GISExtent;
            Frequency?: boolean;
            IdsOnly?: boolean;
            MaxResults?: number;
            SearchId: number;
            TotalOnly?: boolean;
        }
        export interface GISSaved extends ServiceTypes.CoreRequestBase {
        }
        export interface InspectionSaved extends ServiceTypes.CoreRequestBase {
        }
        export interface MaterialSaved extends ServiceTypes.CoreRequestBase {
        }
        export interface PllSaved extends ServiceTypes.CoreRequestBase {
        }
        export interface RecentActivity extends ServiceTypes.CoreRequestBase {
        }
        export interface RequestSaved extends ServiceTypes.CoreRequestBase {
        }
        export interface RequisitionSaved extends ServiceTypes.CoreRequestBase {
        }
        export interface SavedByType extends ServiceTypes.CoreRequestBase {
            DomainId?: number;
            EmployeeSid?: number;
            SearchType?: number;
        }
        export interface StoreTransactionSaved extends ServiceTypes.CoreRequestBase {
        }
        export interface Types extends ServiceTypes.CoreRequestBase {
        }
        export interface WorkActivitySaved extends ServiceTypes.CoreRequestBase {
        }
        export interface WorkOrderEntitySaved extends ServiceTypes.CoreRequestBase {
        }
        export interface WorkOrderSaved extends ServiceTypes.CoreRequestBase {
        }
    }
    export namespace Responses {
        export interface Definition extends ServiceTypes.CoreResponseBase_obsolete_<CoreTypes.SearchDefinition> {}
        export interface Definitions extends ServiceTypes.CoreResponseBase_<CoreTypes.SearchDefinition[]> {}
        export interface Execute extends ServiceTypes.CoreResponseBase_obsolete_<{[key: string]: Object}[]> {}
        export interface Saved extends ServiceTypes.CoreResponseBase_obsolete_<CoreTypes.SearchDefinitionName[]> {}
        export interface RecentActivity extends ServiceTypes.CoreResponseBase_obsolete_<CoreTypes.RecentActivity[]> {}
        export interface Types extends ServiceTypes.CoreResponseBase_obsolete_<CoreTypes.SearchTypeInfo[]> {}
    }
    export interface ISearchService {
        Definition?: (request: Requests.Definition) => AbortablePromise<Responses.Definition>;
        Definitions?: (request: Requests.Definitions) => AbortablePromise<Responses.Definitions>;
        Execute?: (request: Requests.Execute) => AbortablePromise<Responses.Execute>;
        GISSaved?: (request: Requests.GISSaved) => AbortablePromise<Responses.Saved>;
        InspectionSaved?: (request: Requests.InspectionSaved) => AbortablePromise<Responses.Saved>;
        MaterialSaved?: (request: Requests.MaterialSaved) => AbortablePromise<Responses.Saved>;
        PllSaved?: (request: Requests.PllSaved) => AbortablePromise<Responses.Saved>;
        RecentActivity?: (request: Requests.RecentActivity) => AbortablePromise<Responses.RecentActivity>;
        RequestSaved?: (request: Requests.RequestSaved) => AbortablePromise<Responses.Saved>;
        RequisitionSaved?: (request: Requests.RequisitionSaved) => AbortablePromise<Responses.Saved>;
        SavedByType?: (request: Requests.SavedByType) => AbortablePromise<Responses.Saved>;
        StoreTransactionSaved?: (request: Requests.StoreTransactionSaved) => AbortablePromise<Responses.Saved>;
        Types?: (request: Requests.Types) => AbortablePromise<Responses.Types>;
        WorkActivitySaved?: (request: Requests.WorkActivitySaved) => AbortablePromise<Responses.Saved>;
        WorkOrderEntitySaved?: (request: Requests.WorkOrderEntitySaved) => AbortablePromise<Responses.Saved>;
        WorkOrderSaved?: (request: Requests.WorkOrderSaved) => AbortablePromise<Responses.Saved>;
    }
}
