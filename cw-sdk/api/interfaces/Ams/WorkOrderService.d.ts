import { Enums, ServiceEnums, PromiseTypes, ServiceTypes, Types as CoreTypes } from '../../../bundles/core';
import AbortablePromise = PromiseTypes.AbortablePromise;

export namespace WorkOrderServiceTypes { 
    export namespace Requests {
        export interface AddComments extends ServiceTypes.CoreRequestBase {
            Comments: string;
            WorkOrderId: string;
        }
        export interface AddEntities extends ServiceTypes.CoreRequestBase {
            EntityType: string;
            EntityUids: string[];
            UpdateXY?: boolean;
            WorkOrderId: string;
        }
        export interface ById extends ServiceTypes.CoreRequestBase {
            WorkOrderId: string;
        }
        export interface ByIds extends ServiceTypes.CoreRequestBase {
            WorkOrderIds: string[];
        }
        export interface Cancel extends ServiceTypes.CoreRequestBase {
            CancelReason?: string;
            DateCancelled?: Date;
            WorkOrderIds: string[];
        }
        export interface Categories extends ServiceTypes.CoreRequestBase {
        }
        export interface Close extends ServiceTypes.CoreRequestBase {
            WorkOrderIds: string[];
        }
        export interface Comments extends ServiceTypes.CoreRequestBase {
            WorkOrderId: string;
        }
        export interface CommentsByWorkOrderIds extends ServiceTypes.CoreRequestBase {
            WorkOrderIds: string[];
        }
        export interface Create extends ServiceTypes.CoreRequestBase {
            Address?: string;
            City?: string;
            Comments?: string;
            CustomFieldValues?: {[key: number]: string};
            Date1?: Date;
            Date2?: Date;
            Date3?: Date;
            Date4?: Date;
            Date5?: Date;
            District?: string;
            Entities?: CoreTypes.WorkOrderEntity[];
            EntityType: string;
            ExpenseType?: string;
            GetGisData?: boolean;
            GroupEntities?: boolean;
            InitiatedByApp?: string;
            Instructions?: string;
            Location?: string;
            MapPage?: string;
            Num1?: number;
            Num2?: number;
            Num3?: number;
            Num4?: number;
            Num5?: number;
            Priority?: string;
            ProjectedStartDate?: Date;
            Reactive?: boolean;
            RequestedBy?: number;
            RequestedBySid?: number;
            Shop?: string;
            Stage?: string;
            Status?: string;
            StreetName?: string;
            SubmitTo?: number;
            SubmitToSid?: number;
            Supervisor?: number;
            SupervisorSid?: number;
            Text1?: string;
            Text10?: string;
            Text11?: string;
            Text12?: string;
            Text13?: string;
            Text14?: string;
            Text15?: string;
            Text16?: string;
            Text17?: string;
            Text18?: string;
            Text19?: string;
            Text2?: string;
            Text20?: string;
            Text3?: string;
            Text4?: string;
            Text5?: string;
            Text6?: string;
            Text7?: string;
            Text8?: string;
            Text9?: string;
            TileNo?: string;
            WebServiceRequestId?: string;
            WOTemplateId: string;
            X?: number;
            Y?: number;
            Zip?: string;
        }
        export interface CreateFromInspection extends ServiceTypes.CoreRequestBase {
            Address?: string;
            Comments?: string;
            CustomFieldValues?: {[key: number]: string};
            Date1?: Date;
            Date2?: Date;
            Date3?: Date;
            Date4?: Date;
            Date5?: Date;
            Entities?: CoreTypes.WorkOrderEntity[];
            EntityType: string;
            ExpenseType?: string;
            GetGisData?: boolean;
            GroupEntities?: boolean;
            InitiatedByApp?: string;
            InspectionId: number;
            Instructions?: string;
            Location?: string;
            Num1?: number;
            Num2?: number;
            Num3?: number;
            Num4?: number;
            Num5?: number;
            Priority?: string;
            ProjectedStartDate?: Date;
            Reactive?: boolean;
            RequestedBy?: number;
            RequestedBySid?: number;
            Stage?: string;
            Status?: string;
            SubmitTo?: number;
            SubmitToSid?: number;
            Supervisor?: number;
            SupervisorSid?: number;
            Text1?: string;
            Text10?: string;
            Text11?: string;
            Text12?: string;
            Text13?: string;
            Text14?: string;
            Text15?: string;
            Text16?: string;
            Text17?: string;
            Text18?: string;
            Text19?: string;
            Text2?: string;
            Text20?: string;
            Text3?: string;
            Text4?: string;
            Text5?: string;
            Text6?: string;
            Text7?: string;
            Text8?: string;
            Text9?: string;
            WebServiceRequestId?: string;
            WOTemplateId: string;
        }
        export interface CreateFromParent extends ServiceTypes.CoreRequestBase {
            Address?: string;
            Comments?: string;
            CustomFieldValues?: {[key: number]: string};
            Date1?: Date;
            Date2?: Date;
            Date3?: Date;
            Date4?: Date;
            Date5?: Date;
            Entities?: CoreTypes.WorkOrderEntity[];
            EntityType: string;
            ExpenseType?: string;
            GetGisData?: boolean;
            GroupEntities?: boolean;
            InitiatedByApp?: string;
            Instructions?: string;
            Location?: string;
            Num1?: number;
            Num2?: number;
            Num3?: number;
            Num4?: number;
            Num5?: number;
            Priority?: string;
            ProjectedStartDate?: Date;
            Reactive?: boolean;
            RequestedBy?: number;
            RequestedBySid?: number;
            Stage?: string;
            Status?: string;
            SubmitTo?: number;
            SubmitToSid?: number;
            Supervisor?: number;
            SupervisorSid?: number;
            Text1?: string;
            Text10?: string;
            Text11?: string;
            Text12?: string;
            Text13?: string;
            Text14?: string;
            Text15?: string;
            Text16?: string;
            Text17?: string;
            Text18?: string;
            Text19?: string;
            Text2?: string;
            Text20?: string;
            Text3?: string;
            Text4?: string;
            Text5?: string;
            Text6?: string;
            Text7?: string;
            Text8?: string;
            Text9?: string;
            WebServiceRequestId?: string;
            WorkOrderId: string;
            WOTemplateId: string;
        }
        export interface CreateFromServiceRequest extends ServiceTypes.CoreRequestBase {
            Address?: string;
            Comments?: string;
            CustomFieldValues?: {[key: number]: string};
            Date1?: Date;
            Date2?: Date;
            Date3?: Date;
            Date4?: Date;
            Date5?: Date;
            Entities?: CoreTypes.WorkOrderEntity[];
            EntityType: string;
            ExpenseType?: string;
            GetGisData?: boolean;
            GroupEntities?: boolean;
            InitiatedByApp?: string;
            Instructions?: string;
            Location?: string;
            Num1?: number;
            Num2?: number;
            Num3?: number;
            Num4?: number;
            Num5?: number;
            Priority?: string;
            ProjectedStartDate?: Date;
            Reactive?: boolean;
            RequestedBy?: number;
            RequestedBySid?: number;
            RequestId: number;
            Stage?: string;
            Status?: string;
            SubmitTo?: number;
            SubmitToSid?: number;
            Supervisor?: number;
            SupervisorSid?: number;
            Text1?: string;
            Text10?: string;
            Text11?: string;
            Text12?: string;
            Text13?: string;
            Text14?: string;
            Text15?: string;
            Text16?: string;
            Text17?: string;
            Text18?: string;
            Text19?: string;
            Text2?: string;
            Text20?: string;
            Text3?: string;
            Text4?: string;
            Text5?: string;
            Text6?: string;
            Text7?: string;
            Text8?: string;
            Text9?: string;
            WebServiceRequestId?: string;
            WOTemplateId: string;
        }
        export interface CustomFields extends ServiceTypes.CoreRequestBase {
            WorkOrderIds: string[];
        }
        export interface CycleFrom extends ServiceTypes.CoreRequestBase {
        }
        export interface CycleIntervals extends ServiceTypes.CoreRequestBase {
        }
        export interface CycleTypes extends ServiceTypes.CoreRequestBase {
        }
        export interface Entities extends ServiceTypes.CoreRequestBase {
            GetGisData?: boolean;
            WorkOrderId?: string;
            WorkOrderIds?: string[];
        }
        export interface ExpenseTypes extends ServiceTypes.CoreRequestBase {
        }
        export interface InstructionsByWorkOrderIds extends ServiceTypes.CoreRequestBase {
            WorkOrderIds: string[];
        }
        export interface LinkInspections extends ServiceTypes.CoreRequestBase {
            InspectionIds: number[];
            WorkOrderId: string;
        }
        export interface LinkServiceRequests extends ServiceTypes.CoreRequestBase {
            RequestIds: number[];
            WorkOrderId: string;
        }
        export interface Move extends ServiceTypes.CoreRequestBase {
            WKID?: number;
            WKT?: string;
            WorkOrderId: string;
            X: number;
            Y: number;
        }
        export interface Priorities extends ServiceTypes.CoreRequestBase {
        }
        export interface RemoveEntities extends ServiceTypes.CoreRequestBase {
            EntityType?: string;
            EntityUids?: string[];
            ObjectIds?: number[];
            UpdateXY?: boolean;
            WorkOrderId: string;
        }
        export interface Search extends ServiceTypes.CoreRequestBase {
            ActualFinishDateBegin?: Date;
            ActualFinishDateEnd?: Date;
            ActualFinishDateIsNull?: boolean;
            ActualFinishDateNotInRange?: boolean;
            ActualFinishDateRangeIncludeCurrent?: boolean;
            ActualFinishDateRangeLast?: number;
            ActualFinishDateRangeNext?: number;
            ActualFinishDateRangeUnits?: number;
            ActualStartDateBegin?: Date;
            ActualStartDateEnd?: Date;
            ActualStartDateIsNull?: boolean;
            ActualStartDateNotInRange?: boolean;
            ActualStartDateRangeIncludeCurrent?: boolean;
            ActualStartDateRangeLast?: number;
            ActualStartDateRangeNext?: number;
            ActualStartDateRangeUnits?: number;
            Canceled?: boolean;
            Closed?: boolean;
            CreatedByCycle?: boolean;
            DateSubmitToBegin?: Date;
            DateSubmitToEnd?: Date;
            DateSubmitToIsNull?: boolean;
            DateSubmitToNotInRange?: boolean;
            DateSubmitToRangeIncludeCurrent?: boolean;
            DateSubmitToRangeLast?: number;
            DateSubmitToRangeNext?: number;
            DateSubmitToRangeUnits?: number;
            District?: string[];
            DomainIds?: number[];
            EntityType?: string;
            EntityUids?: string[];
            Extent?: CoreTypes.GISExtent;
            FeatureType?: string;
            FeatureUids?: string[];
            HasAttachment?: boolean;
            HasCase?: boolean;
            HasInspection?: boolean;
            HasRequest?: boolean;
            InitiatedByApp?: string[];
            MapPage?: string[];
            MaxResults?: number;
            Num1IncludeNulls?: boolean;
            Num1IsMaxValueExclusive?: boolean;
            Num1IsMinValueExclusive?: boolean;
            Num1MaxValue?: number;
            Num1MinValue?: number;
            Num1RangeType?: number;
            Num1Values?: number[];
            Num2IncludeNulls?: boolean;
            Num2IsMaxValueExclusive?: boolean;
            Num2IsMinValueExclusive?: boolean;
            Num2MaxValue?: number;
            Num2MinValue?: number;
            Num2RangeType?: number;
            Num2Values?: number[];
            Num3IncludeNulls?: boolean;
            Num3IsMaxValueExclusive?: boolean;
            Num3IsMinValueExclusive?: boolean;
            Num3MaxValue?: number;
            Num3MinValue?: number;
            Num3RangeType?: number;
            Num3Values?: number[];
            Num4IncludeNulls?: boolean;
            Num4IsMaxValueExclusive?: boolean;
            Num4IsMinValueExclusive?: boolean;
            Num4MaxValue?: number;
            Num4MinValue?: number;
            Num4RangeType?: number;
            Num4Values?: number[];
            Num5IncludeNulls?: boolean;
            Num5IsMaxValueExclusive?: boolean;
            Num5IsMinValueExclusive?: boolean;
            Num5MaxValue?: number;
            Num5MinValue?: number;
            Num5RangeType?: number;
            Num5Values?: number[];
            PastDue?: boolean;
            ProjectedFinishDateBegin?: Date;
            ProjectedFinishDateEnd?: Date;
            ProjectedFinishDateIsNull?: boolean;
            ProjectedFinishDateNotInRange?: boolean;
            ProjectedFinishDateRangeIncludeCurrent?: boolean;
            ProjectedFinishDateRangeLast?: number;
            ProjectedFinishDateRangeNext?: number;
            ProjectedFinishDateRangeUnits?: number;
            ProjectedStartDateBegin?: Date;
            ProjectedStartDateEnd?: Date;
            ProjectedStartDateIsNull?: boolean;
            ProjectedStartDateNotInRange?: boolean;
            ProjectedStartDateRangeIncludeCurrent?: boolean;
            ProjectedStartDateRangeLast?: number;
            ProjectedStartDateRangeNext?: number;
            ProjectedStartDateRangeUnits?: number;
            Resolution?: string[];
            Shop?: string[];
            SourceWOId?: string[];
            Status?: string[];
            SubmitTo?: number[];
            TaskActualFinishDateBegin?: Date;
            TaskActualFinishDateEnd?: Date;
            TaskActualFinishDateIsNull?: boolean;
            TaskActualFinishDateNotInRange?: boolean;
            TaskActualFinishDateRangeIncludeCurrent?: boolean;
            TaskActualFinishDateRangeLast?: number;
            TaskActualFinishDateRangeNext?: number;
            TaskActualFinishDateRangeUnits?: number;
            TaskAssignedTo?: number[];
            TaskStatus?: string[];
            Text1?: string[];
            Text10?: string[];
            Text11?: string[];
            Text12?: string[];
            Text13?: string[];
            Text14?: string[];
            Text15?: string[];
            Text16?: string[];
            Text17?: string[];
            Text18?: string[];
            Text19?: string[];
            Text2?: string[];
            Text20?: string[];
            Text3?: string[];
            Text4?: string[];
            Text5?: string[];
            Text6?: string[];
            Text7?: string[];
            Text8?: string[];
            Text9?: string[];
            TileNo?: string[];
            TransToWOId?: string[];
            Unattached?: boolean;
            UnitsAccompDesc?: string[];
            UnitsAccomplishedIncludeNulls?: boolean;
            UnitsAccomplishedIsMaxValueExclusive?: boolean;
            UnitsAccomplishedIsMinValueExclusive?: boolean;
            UnitsAccomplishedMaxValue?: number;
            UnitsAccomplishedMinValue?: number;
            UnitsAccomplishedRangeType?: number;
            UnitsAccomplishedValues?: number[];
            WOCostIncludeNulls?: boolean;
            WOCostIsMaxValueExclusive?: boolean;
            WOCostIsMinValueExclusive?: boolean;
            WOCostMaxValue?: number;
            WOCostMinValue?: number;
            WOCostRangeType?: number;
            WOCostValues?: number[];
            WOEquipCostIncludeNulls?: boolean;
            WOEquipCostIsMaxValueExclusive?: boolean;
            WOEquipCostIsMinValueExclusive?: boolean;
            WOEquipCostMaxValue?: number;
            WOEquipCostMinValue?: number;
            WOEquipCostRangeType?: number;
            WOEquipCostValues?: number[];
            WOLaborCostIncludeNulls?: boolean;
            WOLaborCostIsMaxValueExclusive?: boolean;
            WOLaborCostIsMinValueExclusive?: boolean;
            WOLaborCostMaxValue?: number;
            WOLaborCostMinValue?: number;
            WOLaborCostRangeType?: number;
            WOLaborCostValues?: number[];
            WOMatCostIncludeNulls?: boolean;
            WOMatCostIsMaxValueExclusive?: boolean;
            WOMatCostIsMinValueExclusive?: boolean;
            WOMatCostMaxValue?: number;
            WOMatCostMinValue?: number;
            WOMatCostRangeType?: number;
            WOMatCostValues?: number[];
            WorkOrderIds?: string[];
            WOTemplateIds?: string[];
        }
        export interface SearchObject extends ServiceTypes.CoreRequestBase {
            WorkOrderId: string;
        }
        export interface Stages extends ServiceTypes.CoreRequestBase {
        }
        export interface Statuses extends ServiceTypes.CoreRequestBase {
        }
        export interface SubmitTos extends ServiceTypes.CoreRequestBase {
            DomainIds?: number[];
            IncludeInactiveEmployees?: boolean;
        }
        export interface Template extends ServiceTypes.CoreRequestBase {
            WOTemplateId: string;
        }
        export interface TemplateCustomFields extends ServiceTypes.CoreRequestBase {
            WOTemplateId: string;
        }
        export interface Templates extends ServiceTypes.CoreRequestBase {
            CanCreate?: boolean;
            Category?: string;
            EntityType: string;
            EntityTypes: string[];
            WOTemplateIds?: string[];
        }
        export interface UnlinkInspections extends ServiceTypes.CoreRequestBase {
            InspectionIds?: number[];
            WorkOrderId: string;
        }
        export interface UnlinkServiceRequests extends ServiceTypes.CoreRequestBase {
            RequestIds?: number[];
            WorkOrderId: string;
        }
        export interface Update extends ServiceTypes.CoreRequestBase {
            Account?: string;
            ActualFinishDate?: Date;
            ActualStartDate?: Date;
            Address?: string;
            ApplyToEntity?: string;
            AssetGroup?: string;
            CompletedBy?: number;
            CompletedBySid?: number;
            ContractorSid?: number;
            ContractWOId?: string;
            ContrBillable?: boolean;
            CustomFieldValues?: {[key: number]: string};
            CycleFrom?: string;
            CycleIntervalNum?: number;
            CycleIntervalUnit?: string;
            CycleType?: string;
            Date1?: Date;
            Date2?: Date;
            Date3?: Date;
            Date4?: Date;
            Date5?: Date;
            DatePrinted?: Date;
            DateSubmitTo?: Date;
            DateSubmitToOpen?: Date;
            DateToBePrinted?: Date;
            Description?: string;
            District?: string;
            Effort?: number;
            ExpenseType?: string;
            FromDate?: Date;
            InitiateDate?: Date;
            InitiatedByApp?: string;
            InitiatedBySid?: number;
            Instructions?: string;
            LegalBillable?: boolean;
            Location?: string;
            MapPage?: string;
            Num1?: number;
            Num2?: number;
            Num3?: number;
            Num4?: number;
            Num5?: number;
            PrimaryContractId?: number;
            Priority?: string;
            Project?: number;
            ProjectedFinishDate?: Date;
            ProjectedStartDate?: Date;
            Reactive?: boolean;
            RequestedBySid?: number;
            Resolution?: string;
            ScheduleDate?: Date;
            Shop?: string;
            SourceWOId?: string;
            Stage?: string;
            Status?: string;
            StreetName?: string;
            SubmitTo?: number;
            SubmitToOpenBy?: number;
            SubmitToOpenBySid?: number;
            SubmitToSid?: number;
            Supervisor?: number;
            SupervisorSid?: number;
            SuppressCycle?: boolean;
            Text1?: string;
            Text10?: string;
            Text11?: string;
            Text12?: string;
            Text13?: string;
            Text14?: string;
            Text15?: string;
            Text16?: string;
            Text17?: string;
            Text18?: string;
            Text19?: string;
            Text2?: string;
            Text20?: string;
            Text3?: string;
            Text4?: string;
            Text5?: string;
            Text6?: string;
            Text7?: string;
            Text8?: string;
            Text9?: string;
            TileNo?: string;
            UnitsAccompDescLock?: boolean;
            UnitsAccomplished?: number;
            UnitsAccomplishedDescription?: string;
            UpdateMap?: boolean;
            WOCategory?: string;
            WorkOrderId: string;
            WOTemplateId?: string;
        }
        export interface UpdateEntity extends ServiceTypes.CoreRequestBase {
            EntityType: string;
            EntityUid: string;
            WorkComplete?: boolean;
            WorkOrderId: string;
        }
        export interface WorkOrderInspections extends ServiceTypes.CoreRequestBase {
            WorkOrderIds: string[];
        }
        export interface WorkOrderServiceRequests extends ServiceTypes.CoreRequestBase {
            WorkOrderIds: string[];
        }
        export interface WorkOrderWorkOrders extends ServiceTypes.CoreRequestBase {
            CreatedByCycle?: boolean;
            WorkOrderIds: string[];
        }
    }
    export namespace Responses {
        export interface AddComments extends ServiceTypes.CoreResponseBase_<string> {}
        export interface AddEntities extends ServiceTypes.CoreResponseBase_<CoreTypes.WorkOrderEntity[]> {}
        export interface ById extends ServiceTypes.CoreResponseBase_<CoreTypes.WorkOrderBase> {}
        export interface ByIds extends ServiceTypes.CoreResponseBase_<CoreTypes.WorkOrderBase[]> {}
        export interface Cancel extends ServiceTypes.CoreResponseBase_<CoreTypes.WorkOrderBase[]> {}
        export interface Categories extends ServiceTypes.CoreResponseBase_<CoreTypes.CodeDesc[]> {}
        export interface Close extends ServiceTypes.CoreResponseBase_<CoreTypes.WorkOrderBase[]> {}
        export interface Comments extends ServiceTypes.CoreResponseBase_<string> {}
        export interface CommentsByWorkOrderIds extends ServiceTypes.CoreResponseBase_<{[key: string]: string}> {}
        export interface Create extends ServiceTypes.CoreResponseVerified_<CoreTypes.WorkOrderBase[]> {}
        export interface CreateFromInspection extends ServiceTypes.CoreResponseBase_<CoreTypes.WorkOrderBase[]> {}
        export interface CreateFromParent extends ServiceTypes.CoreResponseBase_<CoreTypes.WorkOrderBase[]> {}
        export interface CreateFromServiceRequest extends ServiceTypes.CoreResponseBase_<CoreTypes.WorkOrderBase[]> {}
        export interface CustomFields extends ServiceTypes.CoreResponseBase_<{[key: string]: CoreTypes.WOCustField[]}> {}
        export interface CycleFrom extends ServiceTypes.CoreResponseBase_<{[key: string]: string}> {}
        export interface CycleIntervals extends ServiceTypes.CoreResponseBase_<{[key: string]: string}> {}
        export interface CycleTypes extends ServiceTypes.CoreResponseBase_<{[key: string]: string}> {}
        export interface Entities extends ServiceTypes.CoreResponseBase_<CoreTypes.WorkOrderEntity[]> {}
        export interface ExpenseTypes extends ServiceTypes.CoreResponseBase_<{[key: string]: string}> {}
        export interface InstructionsByWorkOrderIds extends ServiceTypes.CoreResponseBase_<{[key: string]: string}> {}
        export interface LinkInspections extends ServiceTypes.CoreResponseBase_<number[]> {}
        export interface LinkServiceRequests extends ServiceTypes.CoreResponseBase_<number[]> {}
        export interface Move extends ServiceTypes.CoreResponseBase_<CoreTypes.GISPoint> {}
        export interface Priorities extends ServiceTypes.CoreResponseBase_<CoreTypes.CodeDesc[]> {}
        export interface RemoveEntities extends ServiceTypes.CoreResponseBase_<string[]> {}
        export interface Search extends ServiceTypes.CoreResponseBase_<string[]> {}
        export interface SearchObject extends ServiceTypes.CoreResponseBase_<CoreTypes.WorkOrderBase[]> {}
        export interface Stages extends ServiceTypes.CoreResponseBase_<{[key: string]: string}> {}
        export interface Statuses extends ServiceTypes.CoreResponseBase_<CoreTypes.CodeDesc[]> {}
        export interface SubmitTos extends ServiceTypes.CoreResponseBase_<CoreTypes.EmployeeNameId[]> {}
        export interface Template extends ServiceTypes.CoreResponseBase_<CoreTypes.WOTemplateBase> {}
        export interface TemplateCustomFields extends ServiceTypes.CoreResponseBase_<CoreTypes.CategoryCustField[]> {}
        export interface Templates extends ServiceTypes.CoreResponseBase_<CoreTypes.WOTemplateName[]> {}
        export interface UnlinkInspections extends ServiceTypes.CoreResponseBase_<number[]> {}
        export interface UnlinkServiceRequests extends ServiceTypes.CoreResponseBase_<number[]> {}
        export interface Update extends ServiceTypes.CoreResponseBase_<CoreTypes.WorkOrderBase> {}
        export interface UpdateEntity extends ServiceTypes.CoreResponseBase_<CoreTypes.WorkOrderEntity> {}
        export interface WorkOrderInspections extends ServiceTypes.CoreResponseBase_<CoreTypes.WorkOrderInspection[]> {}
        export interface WorkOrderServiceRequests extends ServiceTypes.CoreResponseBase_<CoreTypes.RequestWorkOrder[]> {}
        export interface WorkOrderWorkOrders extends ServiceTypes.CoreResponseBase_<CoreTypes.WorkOrderRelatedWorkOrder[]> {}
    }
    export interface IWorkOrderService {
        AddComments?: (request: Requests.AddComments) => AbortablePromise<Responses.AddComments>;
        AddEntities?: (request: Requests.AddEntities) => AbortablePromise<Responses.AddEntities>;
        ById?: (request: Requests.ById) => AbortablePromise<Responses.ById>;
        ByIds?: (request: Requests.ByIds) => AbortablePromise<Responses.ByIds>;
        Cancel?: (request: Requests.Cancel) => AbortablePromise<Responses.Cancel>;
        Categories?: (request: Requests.Categories) => AbortablePromise<Responses.Categories>;
        Close?: (request: Requests.Close) => AbortablePromise<Responses.Close>;
        Comments?: (request: Requests.Comments) => AbortablePromise<Responses.Comments>;
        CommentsByWorkOrderIds?: (request: Requests.CommentsByWorkOrderIds) => AbortablePromise<Responses.CommentsByWorkOrderIds>;
        Create?: (request: Requests.Create) => AbortablePromise<Responses.Create>;
        CreateFromInspection?: (request: Requests.CreateFromInspection) => AbortablePromise<Responses.CreateFromInspection>;
        CreateFromParent?: (request: Requests.CreateFromParent) => AbortablePromise<Responses.CreateFromParent>;
        CreateFromServiceRequest?: (request: Requests.CreateFromServiceRequest) => AbortablePromise<Responses.CreateFromServiceRequest>;
        CustomFields?: (request: Requests.CustomFields) => AbortablePromise<Responses.CustomFields>;
        CycleFrom?: (request: Requests.CycleFrom) => AbortablePromise<Responses.CycleFrom>;
        CycleIntervals?: (request: Requests.CycleIntervals) => AbortablePromise<Responses.CycleIntervals>;
        CycleTypes?: (request: Requests.CycleTypes) => AbortablePromise<Responses.CycleTypes>;
        Entities?: (request: Requests.Entities) => AbortablePromise<Responses.Entities>;
        ExpenseTypes?: (request: Requests.ExpenseTypes) => AbortablePromise<Responses.ExpenseTypes>;
        InstructionsByWorkOrderIds?: (request: Requests.InstructionsByWorkOrderIds) => AbortablePromise<Responses.InstructionsByWorkOrderIds>;
        LinkInspections?: (request: Requests.LinkInspections) => AbortablePromise<Responses.LinkInspections>;
        LinkServiceRequests?: (request: Requests.LinkServiceRequests) => AbortablePromise<Responses.LinkServiceRequests>;
        Move?: (request: Requests.Move) => AbortablePromise<Responses.Move>;
        Priorities?: (request: Requests.Priorities) => AbortablePromise<Responses.Priorities>;
        RemoveEntities?: (request: Requests.RemoveEntities) => AbortablePromise<Responses.RemoveEntities>;
        Search?: (request: Requests.Search) => AbortablePromise<Responses.Search>;
        SearchObject?: (request: Requests.SearchObject) => AbortablePromise<Responses.SearchObject>;
        Stages?: (request: Requests.Stages) => AbortablePromise<Responses.Stages>;
        Statuses?: (request: Requests.Statuses) => AbortablePromise<Responses.Statuses>;
        SubmitTos?: (request: Requests.SubmitTos) => AbortablePromise<Responses.SubmitTos>;
        Template?: (request: Requests.Template) => AbortablePromise<Responses.Template>;
        TemplateCustomFields?: (request: Requests.TemplateCustomFields) => AbortablePromise<Responses.TemplateCustomFields>;
        Templates?: (request: Requests.Templates) => AbortablePromise<Responses.Templates>;
        UnlinkInspections?: (request: Requests.UnlinkInspections) => AbortablePromise<Responses.UnlinkInspections>;
        UnlinkServiceRequests?: (request: Requests.UnlinkServiceRequests) => AbortablePromise<Responses.UnlinkServiceRequests>;
        Update?: (request: Requests.Update) => AbortablePromise<Responses.Update>;
        UpdateEntity?: (request: Requests.UpdateEntity) => AbortablePromise<Responses.UpdateEntity>;
        WorkOrderInspections?: (request: Requests.WorkOrderInspections) => AbortablePromise<Responses.WorkOrderInspections>;
        WorkOrderServiceRequests?: (request: Requests.WorkOrderServiceRequests) => AbortablePromise<Responses.WorkOrderServiceRequests>;
        WorkOrderWorkOrders?: (request: Requests.WorkOrderWorkOrders) => AbortablePromise<Responses.WorkOrderWorkOrders>;
    }
}
