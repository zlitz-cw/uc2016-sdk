import { Enums, ServiceEnums, PromiseTypes, ServiceTypes, Types as CoreTypes } from '../../../bundles/core';
import AbortablePromise = PromiseTypes.AbortablePromise;

export namespace EquipmentServiceTypes { 
    export namespace Requests {
        export interface All extends ServiceTypes.CoreRequestBase {
            ViewableOnly?: boolean;
        }
        export interface ById extends ServiceTypes.CoreRequestBase {
            EquipmentSid: number;
        }
        export interface ByIds extends ServiceTypes.CoreRequestBase {
            EquipmentSids?: number[];
        }
    }
    export namespace Responses {
        export interface All extends ServiceTypes.CoreResponseBase_obsolete_<CoreTypes.EquipmentBase[]> {}
        export interface ById extends ServiceTypes.CoreResponseBase_obsolete_<CoreTypes.EquipmentBase> {}
        export interface ByIds extends ServiceTypes.CoreResponseBase_obsolete_<CoreTypes.EquipmentBase[]> {}
    }
    export interface IEquipmentService {
        All?: (request: Requests.All) => AbortablePromise<Responses.All>;
        ById?: (request: Requests.ById) => AbortablePromise<Responses.ById>;
        ByIds?: (request: Requests.ByIds) => AbortablePromise<Responses.ByIds>;
    }
}
