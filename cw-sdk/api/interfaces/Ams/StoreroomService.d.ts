import { Enums, ServiceEnums, PromiseTypes, ServiceTypes, Types as CoreTypes } from '../../../bundles/core';
import AbortablePromise = PromiseTypes.AbortablePromise;

export namespace StoreroomServiceTypes { 
    export namespace Requests {
        export interface Accounts extends ServiceTypes.CoreRequestBase {
            IncludeInactive?: boolean;
        }
    }
    export namespace Responses {
        export interface Accounts extends ServiceTypes.CoreResponseBase_<CoreTypes.CodeDesc[]> {}
    }
    export interface IStoreroomService {
        Accounts?: (request: Requests.Accounts) => AbortablePromise<Responses.Accounts>;
    }
}
