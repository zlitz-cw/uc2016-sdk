import { Enums, ServiceEnums, PromiseTypes, ServiceTypes, Types as CoreTypes } from '../../../bundles/core';
import AbortablePromise = PromiseTypes.AbortablePromise;

export namespace EntityServiceTypes { 
    export namespace Requests {
        export interface AddAlias extends ServiceTypes.CoreRequestBase {
            EntityType?: string;
            LayerName?: string;
        }
        export interface AliasAssets extends ServiceTypes.CoreRequestBase {
            Aliases?: string[];
        }
        export interface Aliases extends ServiceTypes.CoreRequestBase {
            EntityType?: string;
        }
        export interface AllAliases extends ServiceTypes.CoreRequestBase {
        }
        export interface Attributes extends ServiceTypes.CoreRequestBase {
            EntityType?: string;
        }
        export interface Configuration extends ServiceTypes.CoreRequestBase {
            EntityType: string;
            EntityTypes: string[];
            MaximumDateModified?: Date;
            MinimumDateModified?: Date;
        }
        export interface CostHistory extends ServiceTypes.CoreRequestBase {
            EntityType?: string;
            EntityUids?: string[];
        }
        export interface CostTotal extends ServiceTypes.CoreRequestBase {
            EntityType?: string;
            EntityUids?: string[];
        }
        export interface EntityUidField extends ServiceTypes.CoreRequestBase {
            EntityType?: string;
        }
        export interface Groups extends ServiceTypes.CoreRequestBase {
            DomainId?: number;
        }
        export interface RemoveAlias extends ServiceTypes.CoreRequestBase {
            EntityType?: string;
            LayerName?: string;
        }
        export interface Types extends ServiceTypes.CoreRequestBase {
            EntityGroup: string;
            EntityGroups: string[];
        }
        export interface VisibleFields extends ServiceTypes.CoreRequestBase {
            EntityType?: string;
        }
        export interface WorkActivityHistory extends ServiceTypes.CoreRequestBase {
            EntityType?: string;
            EntityUid?: string;
            OnlyOpenWAs?: boolean;
        }
    }
    export namespace Responses {
        export interface AssetAlias extends ServiceTypes.CoreResponseBase_<CoreTypes.AssetAlias> {}
        export interface AliasAssets extends ServiceTypes.CoreResponseBase_<CoreTypes.AliasAsset[]> {}
        export interface AllAliases extends ServiceTypes.CoreResponseBase_<CoreTypes.AssetAlias[]> {}
        export interface Attributes extends ServiceTypes.CoreResponseBase_<CoreTypes.EsriServiceLayerAttribute[]> {}
        export interface Configuration extends ServiceTypes.CoreResponseBase_<CoreTypes.EntityConfiguration[]> {}
        export interface CostHistory extends ServiceTypes.CoreResponseBase_<CoreTypes.WorkOrderCostSummary[]> {}
        export interface CostTotal extends ServiceTypes.CoreResponseBase_<CoreTypes.EntityCostTotal[]> {}
        export interface EntityUidField extends ServiceTypes.CoreResponseBase_<string> {}
        export interface Groups extends ServiceTypes.CoreResponseBase_<CoreTypes.PWModule[]> {}
        export interface Types extends ServiceTypes.CoreResponseBase_<CoreTypes.PWEntity[]> {}
        export interface VisibleFields extends ServiceTypes.CoreResponseBase_<string[]> {}
        export interface WorkActivityHistory extends ServiceTypes.CoreResponseBase_<CoreTypes.EntityHistory[]> {}
    }
    export interface IEntityService {
        AddAlias?: (request: Requests.AddAlias) => AbortablePromise<Responses.AssetAlias>;
        AliasAssets?: (request: Requests.AliasAssets) => AbortablePromise<Responses.AliasAssets>;
        Aliases?: (request: Requests.Aliases) => AbortablePromise<Responses.AssetAlias>;
        AllAliases?: (request: Requests.AllAliases) => AbortablePromise<Responses.AllAliases>;
        Attributes?: (request: Requests.Attributes) => AbortablePromise<Responses.Attributes>;
        Configuration?: (request: Requests.Configuration) => AbortablePromise<Responses.Configuration>;
        CostHistory?: (request: Requests.CostHistory) => AbortablePromise<Responses.CostHistory>;
        CostTotal?: (request: Requests.CostTotal) => AbortablePromise<Responses.CostTotal>;
        EntityUidField?: (request: Requests.EntityUidField) => AbortablePromise<Responses.EntityUidField>;
        Groups?: (request: Requests.Groups) => AbortablePromise<Responses.Groups>;
        RemoveAlias?: (request: Requests.RemoveAlias) => AbortablePromise<Responses.AssetAlias>;
        Types?: (request: Requests.Types) => AbortablePromise<Responses.Types>;
        VisibleFields?: (request: Requests.VisibleFields) => AbortablePromise<Responses.VisibleFields>;
        WorkActivityHistory?: (request: Requests.WorkActivityHistory) => AbortablePromise<Responses.WorkActivityHistory>;
    }
}
