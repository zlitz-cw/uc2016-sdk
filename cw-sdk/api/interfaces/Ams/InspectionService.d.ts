import { Enums, ServiceEnums, PromiseTypes, ServiceTypes, Types as CoreTypes } from '../../../bundles/core';
import AbortablePromise = PromiseTypes.AbortablePromise;

export namespace InspectionServiceTypes { 
    export namespace Requests {
        export interface AddEntity extends ServiceTypes.CoreRequestBase {
            Entity?: CoreTypes.WorkOrderEntityBase;
            EntityType: string;
            EntityUid: string;
            InspectionId: number;
            UpdateXY?: boolean;
        }
        export interface Answers extends ServiceTypes.CoreRequestBase {
            InspectionId: number;
            InspectionIds: number[];
        }
        export interface ById extends ServiceTypes.CoreRequestBase {
            InspectionId: number;
        }
        export interface ByIds extends ServiceTypes.CoreRequestBase {
            InspectionIds: number[];
        }
        export interface ByWorkOrderIds extends ServiceTypes.CoreRequestBase {
            WorkOrderIds: string[];
        }
        export interface Cancel extends ServiceTypes.CoreRequestBase {
            CancelReason?: string;
            DateCancelled?: Date;
            InspectionIds: number[];
        }
        export interface Close extends ServiceTypes.CoreRequestBase {
            InspectionIds: number[];
        }
        export interface Create extends ServiceTypes.CoreRequestBase {
            Address?: string;
            Date1?: Date;
            Date2?: Date;
            Date3?: Date;
            Date4?: Date;
            Date5?: Date;
            District?: string;
            Entity?: CoreTypes.WorkOrderEntity;
            EntityType: string;
            GetGisData?: boolean;
            InitiatedByApp?: string;
            InspTemplateId: number;
            Location?: string;
            MapPage?: string;
            Num1?: number;
            Num2?: number;
            Num3?: number;
            Num4?: number;
            Num5?: number;
            Shop?: string;
            StreetName?: string;
            Text1?: string;
            Text10?: string;
            Text2?: string;
            Text3?: string;
            Text4?: string;
            Text5?: string;
            Text6?: string;
            Text7?: string;
            Text8?: string;
            Text9?: string;
            TileNo?: string;
            X?: number;
            Y?: number;
        }
        export interface CreateFromServiceRequest extends ServiceTypes.CoreRequestBase {
            Date1?: Date;
            Date2?: Date;
            Date3?: Date;
            Date4?: Date;
            Date5?: Date;
            Entity?: CoreTypes.WorkOrderEntity;
            EntityType: string;
            GetGisData?: boolean;
            InitiatedByApp?: string;
            InspTemplateId: number;
            Num1?: number;
            Num2?: number;
            Num3?: number;
            Num4?: number;
            Num5?: number;
            RequestId: number;
            Text1?: string;
            Text10?: string;
            Text2?: string;
            Text3?: string;
            Text4?: string;
            Text5?: string;
            Text6?: string;
            Text7?: string;
            Text8?: string;
            Text9?: string;
        }
        export interface CreateFromWorkOrder extends ServiceTypes.CoreRequestBase {
            Date1?: Date;
            Date2?: Date;
            Date3?: Date;
            Date4?: Date;
            Date5?: Date;
            Entity?: CoreTypes.WorkOrderEntity;
            EntityType: string;
            GetGisData?: boolean;
            InitiatedByApp?: string;
            InspTemplateId: number;
            Num1?: number;
            Num2?: number;
            Num3?: number;
            Num4?: number;
            Num5?: number;
            ProjectedStartDate?: Date;
            Text1?: string;
            Text10?: string;
            Text2?: string;
            Text3?: string;
            Text4?: string;
            Text5?: string;
            Text6?: string;
            Text7?: string;
            Text8?: string;
            Text9?: string;
            WorkOrderId: string;
        }
        export interface CycleFrom extends ServiceTypes.CoreRequestBase {
        }
        export interface CycleIntervals extends ServiceTypes.CoreRequestBase {
        }
        export interface CycleTypes extends ServiceTypes.CoreRequestBase {
        }
        export interface Districts extends ServiceTypes.CoreRequestBase {
        }
        export interface InspectionInspections extends ServiceTypes.CoreRequestBase {
            CreatedByCycle?: boolean;
            InspectionIds: number[];
        }
        export interface InspectionServiceRequests extends ServiceTypes.CoreRequestBase {
            InspectionIds: number[];
        }
        export interface InspectionWorkOrders extends ServiceTypes.CoreRequestBase {
            InspectionIds: number[];
        }
        export interface Move extends ServiceTypes.CoreRequestBase {
            InspectionId: number;
            WKID?: number;
            WKT?: string;
            X: number;
            Y: number;
        }
        export interface Priorities extends ServiceTypes.CoreRequestBase {
        }
        export interface QA extends ServiceTypes.CoreRequestBase {
            InspTemplateId: number;
        }
        export interface RemoveEntity extends ServiceTypes.CoreRequestBase {
            InspectionIds: number[];
        }
        export interface Resolutions extends ServiceTypes.CoreRequestBase {
        }
        export interface Search extends ServiceTypes.CoreRequestBase {
            Canceled?: boolean;
            Closed?: boolean;
            CreatedByCycle?: boolean;
            DateSubmitToBegin?: Date;
            DateSubmitToEnd?: Date;
            DateSubmitToIsNull?: boolean;
            DateSubmitToNotInRange?: boolean;
            DateSubmitToRangeIncludeCurrent?: boolean;
            DateSubmitToRangeLast?: number;
            DateSubmitToRangeNext?: number;
            DateSubmitToRangeUnits?: number;
            EntityType?: string;
            EntityUids?: string[];
            Extent?: CoreTypes.GISExtent;
            FeatureType?: string;
            FeatureUids?: string[];
            HasAttachment?: boolean;
            HasRequest?: boolean;
            HasWorkOrder?: boolean;
            InitiatedByApp?: string[];
            InspectedBy?: number[];
            InspectionDateBegin?: Date;
            InspectionDateEnd?: Date;
            InspectionDateIsNull?: boolean;
            InspectionDateNotInRange?: boolean;
            InspectionDateRangeIncludeCurrent?: boolean;
            InspectionDateRangeLast?: number;
            InspectionDateRangeNext?: number;
            InspectionDateRangeUnits?: number;
            InspectionIds?: number[];
            MaxResults?: number;
            ParentInspId?: number[];
            PastDue?: boolean;
            Status?: string[];
            SubmitTo?: number[];
            Text1?: string[];
            Text10?: string[];
            Text2?: string[];
            Text3?: string[];
            Text4?: string[];
            Text5?: string[];
            Text6?: string[];
            Text7?: string[];
            Text8?: string[];
            Text9?: string[];
            Unattached?: boolean;
        }
        export interface SearchObject extends ServiceTypes.CoreRequestBase {
            InspectionId: string;
        }
        export interface Shops extends ServiceTypes.CoreRequestBase {
        }
        export interface Statuses extends ServiceTypes.CoreRequestBase {
        }
        export interface SubmitTos extends ServiceTypes.CoreRequestBase {
            DomainIds?: number[];
            IncludeInactiveEmployees?: boolean;
        }
        export interface Templates extends ServiceTypes.CoreRequestBase {
            CanCreate?: boolean;
            EntityType?: string;
            IncludeInactive?: boolean;
            MaximumDateModified?: Date;
            MinimumDateModified?: Date;
            TemplateIds?: number[];
        }
        export interface Update extends ServiceTypes.CoreRequestBase {
            ActualFinishDate?: Date;
            Answers?: CoreTypes.InspectionAnswer[];
            CondRating?: number;
            CondScore?: number;
            CycleFrom?: string;
            CycleIntervalNum?: number;
            CycleIntervalUnit?: string;
            CycleType?: string;
            Date1?: Date;
            Date2?: Date;
            Date3?: Date;
            Date4?: Date;
            Date5?: Date;
            District?: string;
            Effort?: number;
            ForemanRecomnd?: string;
            FromDate?: Date;
            InitiatedByApp?: string;
            InspectedBy?: number;
            InspectedBySid?: number;
            InspectionDate?: Date;
            InspectionId: number;
            Location?: string;
            MapPage?: string;
            Num1?: number;
            Num2?: number;
            Num3?: number;
            Num4?: number;
            Num5?: number;
            ObservationSum?: string;
            Priority?: string;
            ProjectedFinishDate?: Date;
            ProjectedStartDate?: Date;
            Questions?: CoreTypes.InspectionQuestionDetails[];
            RepairsMade?: string;
            RequestId?: number;
            Resolution?: string;
            Shop?: string;
            Status?: string;
            StreetName?: string;
            SubmitTo?: number;
            SubmitToEmployeeSid?: number;
            SuppressCycle?: boolean;
            Text1?: string;
            Text10?: string;
            Text2?: string;
            Text3?: string;
            Text4?: string;
            Text5?: string;
            Text6?: string;
            Text7?: string;
            Text8?: string;
            Text9?: string;
            TileNo?: string;
            UpdateMap?: boolean;
            WorkOrderId?: string;
        }
    }
    export namespace Responses {
        export interface AddEntity extends ServiceTypes.CoreResponseBase_<CoreTypes.WorkOrderEntity> {}
        export interface Answers extends ServiceTypes.CoreResponseBase_<CoreTypes.InspectionAnswer[]> {}
        export interface ById extends ServiceTypes.CoreResponseBase_<CoreTypes.InspectionBase> {}
        export interface ByIds extends ServiceTypes.CoreResponseBase_<CoreTypes.InspectionBase[]> {}
        export interface ByWorkOrderIds extends ServiceTypes.CoreResponseBase_<CoreTypes.InspectionBase[]> {}
        export interface Cancel extends ServiceTypes.CoreResponseBase_<CoreTypes.InspectionBase[]> {}
        export interface Close extends ServiceTypes.CoreResponseBase_<CoreTypes.InspectionBase[]> {}
        export interface Create extends ServiceTypes.CoreResponseBase_<CoreTypes.InspectionBase> {}
        export interface CreateFromServiceRequest extends ServiceTypes.CoreResponseBase_<CoreTypes.InspectionBase> {}
        export interface CreateFromWorkOrder extends ServiceTypes.CoreResponseBase_<CoreTypes.InspectionBase> {}
        export interface CycleFrom extends ServiceTypes.CoreResponseBase_<{[key: string]: string}> {}
        export interface CycleIntervals extends ServiceTypes.CoreResponseBase_<{[key: string]: string}> {}
        export interface CycleTypes extends ServiceTypes.CoreResponseBase_<{[key: string]: string}> {}
        export interface Districts extends ServiceTypes.CoreResponseBase_<CoreTypes.CodeDesc[]> {}
        export interface InspectionInspections extends ServiceTypes.CoreResponseBase_<CoreTypes.InspectionRelatedInspection[]> {}
        export interface InspectionServiceRequests extends ServiceTypes.CoreResponseBase_<CoreTypes.RequestInspection[]> {}
        export interface InspectionWorkOrders extends ServiceTypes.CoreResponseBase_<CoreTypes.WorkOrderInspection[]> {}
        export interface Move extends ServiceTypes.CoreResponseBase_<CoreTypes.GISPoint> {}
        export interface Priorities extends ServiceTypes.CoreResponseBase_<CoreTypes.CodeDesc[]> {}
        export interface QA extends ServiceTypes.CoreResponseBase_<CoreTypes.InspTemplateQA> {}
        export interface RemoveEntity extends ServiceTypes.CoreResponseBase_<{[key: number]: boolean}> {}
        export interface Resolutions extends ServiceTypes.CoreResponseBase_<CoreTypes.CodeDesc[]> {}
        export interface Search extends ServiceTypes.CoreResponseBase_<number[]> {}
        export interface SearchObject extends ServiceTypes.CoreResponseBase_<CoreTypes.InspectionBase[]> {}
        export interface Shops extends ServiceTypes.CoreResponseBase_<CoreTypes.CodeDesc[]> {}
        export interface Statuses extends ServiceTypes.CoreResponseBase_<CoreTypes.CodeDesc[]> {}
        export interface SubmitTos extends ServiceTypes.CoreResponseBase_<CoreTypes.EmployeeNameId[]> {}
        export interface Templates extends ServiceTypes.CoreResponseBase_<CoreTypes.InspTemplateBase[]> {}
        export interface Update extends ServiceTypes.CoreResponseBase_<CoreTypes.InspectionBase> {}
    }
    export interface IInspectionService {
        AddEntity?: (request: Requests.AddEntity) => AbortablePromise<Responses.AddEntity>;
        Answers?: (request: Requests.Answers) => AbortablePromise<Responses.Answers>;
        ById?: (request: Requests.ById) => AbortablePromise<Responses.ById>;
        ByIds?: (request: Requests.ByIds) => AbortablePromise<Responses.ByIds>;
        ByWorkOrderIds?: (request: Requests.ByWorkOrderIds) => AbortablePromise<Responses.ByWorkOrderIds>;
        Cancel?: (request: Requests.Cancel) => AbortablePromise<Responses.Cancel>;
        Close?: (request: Requests.Close) => AbortablePromise<Responses.Close>;
        Create?: (request: Requests.Create) => AbortablePromise<Responses.Create>;
        CreateFromServiceRequest?: (request: Requests.CreateFromServiceRequest) => AbortablePromise<Responses.CreateFromServiceRequest>;
        CreateFromWorkOrder?: (request: Requests.CreateFromWorkOrder) => AbortablePromise<Responses.CreateFromWorkOrder>;
        CycleFrom?: (request: Requests.CycleFrom) => AbortablePromise<Responses.CycleFrom>;
        CycleIntervals?: (request: Requests.CycleIntervals) => AbortablePromise<Responses.CycleIntervals>;
        CycleTypes?: (request: Requests.CycleTypes) => AbortablePromise<Responses.CycleTypes>;
        Districts?: (request: Requests.Districts) => AbortablePromise<Responses.Districts>;
        InspectionInspections?: (request: Requests.InspectionInspections) => AbortablePromise<Responses.InspectionInspections>;
        InspectionServiceRequests?: (request: Requests.InspectionServiceRequests) => AbortablePromise<Responses.InspectionServiceRequests>;
        InspectionWorkOrders?: (request: Requests.InspectionWorkOrders) => AbortablePromise<Responses.InspectionWorkOrders>;
        Move?: (request: Requests.Move) => AbortablePromise<Responses.Move>;
        Priorities?: (request: Requests.Priorities) => AbortablePromise<Responses.Priorities>;
        QA?: (request: Requests.QA) => AbortablePromise<Responses.QA>;
        RemoveEntity?: (request: Requests.RemoveEntity) => AbortablePromise<Responses.RemoveEntity>;
        Resolutions?: (request: Requests.Resolutions) => AbortablePromise<Responses.Resolutions>;
        Search?: (request: Requests.Search) => AbortablePromise<Responses.Search>;
        SearchObject?: (request: Requests.SearchObject) => AbortablePromise<Responses.SearchObject>;
        Shops?: (request: Requests.Shops) => AbortablePromise<Responses.Shops>;
        Statuses?: (request: Requests.Statuses) => AbortablePromise<Responses.Statuses>;
        SubmitTos?: (request: Requests.SubmitTos) => AbortablePromise<Responses.SubmitTos>;
        Templates?: (request: Requests.Templates) => AbortablePromise<Responses.Templates>;
        Update?: (request: Requests.Update) => AbortablePromise<Responses.Update>;
    }
}
