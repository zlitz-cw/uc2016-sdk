import { Enums, ServiceEnums, PromiseTypes, ServiceTypes, Types as CoreTypes } from '../../../bundles/core';
import AbortablePromise = PromiseTypes.AbortablePromise;

export namespace CaseServiceTypes { 
    export namespace Requests {
        export interface Create extends ServiceTypes.CoreRequestBase {
            AcceptedBy?: number;
            CaseName?: string;
            CaseNumber?: string;
            CaseTypeId: number;
            DateAccepted?: Date;
            Location?: string;
            ProjectCode?: string;
            SubTypeId?: number;
            X?: number;
            Y?: number;
        }
    }
    export namespace Responses {
        export interface Create extends ServiceTypes.CoreResponseBase_obsolete_<CoreTypes.CaObjectItem> {}
    }
    export interface ICaseService {
        Create?: (request: Requests.Create) => AbortablePromise<Responses.Create>;
    }
}
