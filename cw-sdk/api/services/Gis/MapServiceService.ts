import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { MapServiceServiceTypes as SvcDef } from '../../interfaces/Gis/MapServiceService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import IMapServiceService = SvcDef.IMapServiceService;

export * from '../../../http/api-service';
export * from '../../interfaces/Gis/MapServiceService';

export class MapServiceService implements IMapServiceService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Configuration(request: Requests.Configuration): AbortablePromise<Responses.Configuration> {
        return this._service.call(request, 'Gis/MapService/Configuration');
    }
    public Domain(request: Requests.Domain): AbortablePromise<Responses.Domain> {
        return this._service.call(request, 'Gis/MapService/Domain');
    }
    public DownloadMobileMapCache(request: Requests.DownloadMobileMapCache): AbortablePromise<Responses.DownloadMobileMapCache> {
        return this._service.call(request, 'Gis/MapService/DownloadMobileMapCache');
    }
    public User(request: Requests.User): AbortablePromise<Responses.User> {
        return this._service.call(request, 'Gis/MapService/User');
    }
}