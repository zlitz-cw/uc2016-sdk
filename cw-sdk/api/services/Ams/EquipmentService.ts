import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { EquipmentServiceTypes as SvcDef } from '../../interfaces/Ams/EquipmentService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import IEquipmentService = SvcDef.IEquipmentService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/EquipmentService';

export class EquipmentService implements IEquipmentService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public All(request: Requests.All): AbortablePromise<Responses.All> {
        return this._service.call(request, 'Ams/Equipment/All');
    }
    public ById(request: Requests.ById): AbortablePromise<Responses.ById> {
        return this._service.call(request, 'Ams/Equipment/ById');
    }
    public ByIds(request: Requests.ByIds): AbortablePromise<Responses.ByIds> {
        return this._service.call(request, 'Ams/Equipment/ByIds');
    }
}