import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { EquipmentCostServiceTypes as SvcDef } from '../../interfaces/Ams/EquipmentCostService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import IEquipmentCostService = SvcDef.IEquipmentCostService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/EquipmentCostService';

export class EquipmentCostService implements IEquipmentCostService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public AddWorkOrderCosts(request: Requests.AddWorkOrderCosts): AbortablePromise<Responses.AddWorkOrderCosts> {
        return this._service.call(request, 'Ams/EquipmentCost/AddWorkOrderCosts');
    }
    public DeleteWorkOrderCosts(request: Requests.DeleteWorkOrderCosts): AbortablePromise<Responses.DeleteWorkOrderCosts> {
        return this._service.call(request, 'Ams/EquipmentCost/DeleteWorkOrderCosts');
    }
    public WorkOrderCostsByWorkOrder(request: Requests.WorkOrderCostsByWorkOrder): AbortablePromise<Responses.WorkOrderCostsByWorkOrder> {
        return this._service.call(request, 'Ams/EquipmentCost/WorkOrderCostsByWorkOrder');
    }
}