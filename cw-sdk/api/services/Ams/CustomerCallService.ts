import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { CustomerCallServiceTypes as SvcDef } from '../../interfaces/Ams/CustomerCallService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ICustomerCallService = SvcDef.ICustomerCallService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/CustomerCallService';

export class CustomerCallService implements ICustomerCallService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public AddToRequest(request: Requests.AddToRequest): AbortablePromise<Responses.AddToRequest> {
        return this._service.call(request, 'Ams/CustomerCall/AddToRequest');
    }
    public ByIncidentNum(request: Requests.ByIncidentNum): AbortablePromise<Responses.ByIncidentNum> {
        return this._service.call(request, 'Ams/CustomerCall/ByIncidentNum');
    }
    public ByRequestId(request: Requests.ByRequestId): AbortablePromise<Responses.ByRequestId> {
        return this._service.call(request, 'Ams/CustomerCall/ByRequestId');
    }
    public CallerQuestions(request: Requests.CallerQuestions): AbortablePromise<Responses.CallerQuestions> {
        return this._service.call(request, 'Ams/CustomerCall/CallerQuestions');
    }
    public CallerQuestionsByRequestIds(request: Requests.CallerQuestionsByRequestIds): AbortablePromise<Responses.CallerQuestionsByRequestIds> {
        return this._service.call(request, 'Ams/CustomerCall/CallerQuestionsByRequestIds');
    }
    public Delete(request: Requests.Delete): AbortablePromise<Responses.Delete> {
        return this._service.call(request, 'Ams/CustomerCall/Delete');
    }
    public Move(request: Requests.Move): AbortablePromise<Responses.Move> {
        return this._service.call(request, 'Ams/CustomerCall/Move');
    }
    public MoveToRequest(request: Requests.MoveToRequest): AbortablePromise<Responses.MoveToRequest> {
        return this._service.call(request, 'Ams/CustomerCall/MoveToRequest');
    }
    public Update(request: Requests.Update): AbortablePromise<Responses.Update> {
        return this._service.call(request, 'Ams/CustomerCall/Update');
    }
}