import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { InspectionServiceTypes as SvcDef } from '../../interfaces/Ams/InspectionService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import IInspectionService = SvcDef.IInspectionService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/InspectionService';

export class InspectionService implements IInspectionService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public AddEntity(request: Requests.AddEntity): AbortablePromise<Responses.AddEntity> {
        return this._service.call(request, 'Ams/Inspection/AddEntity');
    }
    public Answers(request: Requests.Answers): AbortablePromise<Responses.Answers> {
        return this._service.call(request, 'Ams/Inspection/Answers');
    }
    public ById(request: Requests.ById): AbortablePromise<Responses.ById> {
        return this._service.call(request, 'Ams/Inspection/ById');
    }
    public ByIds(request: Requests.ByIds): AbortablePromise<Responses.ByIds> {
        return this._service.call(request, 'Ams/Inspection/ByIds');
    }
    public ByWorkOrderIds(request: Requests.ByWorkOrderIds): AbortablePromise<Responses.ByWorkOrderIds> {
        return this._service.call(request, 'Ams/Inspection/ByWorkOrderIds');
    }
    public Cancel(request: Requests.Cancel): AbortablePromise<Responses.Cancel> {
        return this._service.call(request, 'Ams/Inspection/Cancel');
    }
    public Close(request: Requests.Close): AbortablePromise<Responses.Close> {
        return this._service.call(request, 'Ams/Inspection/Close');
    }
    public Create(request: Requests.Create): AbortablePromise<Responses.Create> {
        return this._service.call(request, 'Ams/Inspection/Create');
    }
    public CreateFromServiceRequest(request: Requests.CreateFromServiceRequest): AbortablePromise<Responses.CreateFromServiceRequest> {
        return this._service.call(request, 'Ams/Inspection/CreateFromServiceRequest');
    }
    public CreateFromWorkOrder(request: Requests.CreateFromWorkOrder): AbortablePromise<Responses.CreateFromWorkOrder> {
        return this._service.call(request, 'Ams/Inspection/CreateFromWorkOrder');
    }
    public CycleFrom(request: Requests.CycleFrom): AbortablePromise<Responses.CycleFrom> {
        return this._service.call(request, 'Ams/Inspection/CycleFrom');
    }
    public CycleIntervals(request: Requests.CycleIntervals): AbortablePromise<Responses.CycleIntervals> {
        return this._service.call(request, 'Ams/Inspection/CycleIntervals');
    }
    public CycleTypes(request: Requests.CycleTypes): AbortablePromise<Responses.CycleTypes> {
        return this._service.call(request, 'Ams/Inspection/CycleTypes');
    }
    public Districts(request: Requests.Districts): AbortablePromise<Responses.Districts> {
        return this._service.call(request, 'Ams/Inspection/Districts');
    }
    public InspectionInspections(request: Requests.InspectionInspections): AbortablePromise<Responses.InspectionInspections> {
        return this._service.call(request, 'Ams/Inspection/InspectionInspections');
    }
    public InspectionServiceRequests(request: Requests.InspectionServiceRequests): AbortablePromise<Responses.InspectionServiceRequests> {
        return this._service.call(request, 'Ams/Inspection/InspectionServiceRequests');
    }
    public InspectionWorkOrders(request: Requests.InspectionWorkOrders): AbortablePromise<Responses.InspectionWorkOrders> {
        return this._service.call(request, 'Ams/Inspection/InspectionWorkOrders');
    }
    public Move(request: Requests.Move): AbortablePromise<Responses.Move> {
        return this._service.call(request, 'Ams/Inspection/Move');
    }
    public Priorities(request: Requests.Priorities): AbortablePromise<Responses.Priorities> {
        return this._service.call(request, 'Ams/Inspection/Priorities');
    }
    public QA(request: Requests.QA): AbortablePromise<Responses.QA> {
        return this._service.call(request, 'Ams/Inspection/QA');
    }
    public RemoveEntity(request: Requests.RemoveEntity): AbortablePromise<Responses.RemoveEntity> {
        return this._service.call(request, 'Ams/Inspection/RemoveEntity');
    }
    public Resolutions(request: Requests.Resolutions): AbortablePromise<Responses.Resolutions> {
        return this._service.call(request, 'Ams/Inspection/Resolutions');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'Ams/Inspection/Search');
    }
    public SearchObject(request: Requests.SearchObject): AbortablePromise<Responses.SearchObject> {
        return this._service.call(request, 'Ams/Inspection/SearchObject');
    }
    public Shops(request: Requests.Shops): AbortablePromise<Responses.Shops> {
        return this._service.call(request, 'Ams/Inspection/Shops');
    }
    public Statuses(request: Requests.Statuses): AbortablePromise<Responses.Statuses> {
        return this._service.call(request, 'Ams/Inspection/Statuses');
    }
    public SubmitTos(request: Requests.SubmitTos): AbortablePromise<Responses.SubmitTos> {
        return this._service.call(request, 'Ams/Inspection/SubmitTos');
    }
    public Templates(request: Requests.Templates): AbortablePromise<Responses.Templates> {
        return this._service.call(request, 'Ams/Inspection/Templates');
    }
    public Update(request: Requests.Update): AbortablePromise<Responses.Update> {
        return this._service.call(request, 'Ams/Inspection/Update');
    }
}