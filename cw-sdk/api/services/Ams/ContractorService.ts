import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { ContractorServiceTypes as SvcDef } from '../../interfaces/Ams/ContractorService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import IContractorService = SvcDef.IContractorService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/ContractorService';

export class ContractorService implements IContractorService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Add(request: Requests.Add): AbortablePromise<Responses.Add> {
        return this._service.call(request, 'Ams/Contractor/Add');
    }
    public AddKeywords(request: Requests.AddKeywords): AbortablePromise<Responses.AddKeywords> {
        return this._service.call(request, 'Ams/Contractor/AddKeywords');
    }
    public All(request: Requests.All): AbortablePromise<Responses.All> {
        return this._service.call(request, 'Ams/Contractor/All');
    }
    public ById(request: Requests.ById): AbortablePromise<Responses.ById> {
        return this._service.call(request, 'Ams/Contractor/ById');
    }
    public Delete(request: Requests.Delete): AbortablePromise<Responses.Delete> {
        return this._service.call(request, 'Ams/Contractor/Delete');
    }
    public DeleteKeywords(request: Requests.DeleteKeywords): AbortablePromise<Responses.DeleteKeywords> {
        return this._service.call(request, 'Ams/Contractor/DeleteKeywords');
    }
    public Keywords(request: Requests.Keywords): AbortablePromise<Responses.Keywords> {
        return this._service.call(request, 'Ams/Contractor/Keywords');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'Ams/Contractor/Search');
    }
    public Update(request: Requests.Update): AbortablePromise<Responses.Update> {
        return this._service.call(request, 'Ams/Contractor/Update');
    }
}