import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { LaborCostServiceTypes as SvcDef } from '../../interfaces/Ams/LaborCostService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ILaborCostService = SvcDef.ILaborCostService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/LaborCostService';

export class LaborCostService implements ILaborCostService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public AddRequestCosts(request: Requests.AddRequestCosts): AbortablePromise<Responses.AddRequestCosts> {
        return this._service.call(request, 'Ams/LaborCost/AddRequestCosts');
    }
    public AddWorkOrderCosts(request: Requests.AddWorkOrderCosts): AbortablePromise<Responses.AddWorkOrderCosts> {
        return this._service.call(request, 'Ams/LaborCost/AddWorkOrderCosts');
    }
    public CostCodes(request: Requests.CostCodes): AbortablePromise<Responses.CostCodes> {
        return this._service.call(request, 'Ams/LaborCost/CostCodes');
    }
    public DeleteRequestCosts(request: Requests.DeleteRequestCosts): AbortablePromise<Responses.DeleteRequestCosts> {
        return this._service.call(request, 'Ams/LaborCost/DeleteRequestCosts');
    }
    public DeleteWorkOrderCosts(request: Requests.DeleteWorkOrderCosts): AbortablePromise<Responses.DeleteWorkOrderCosts> {
        return this._service.call(request, 'Ams/LaborCost/DeleteWorkOrderCosts');
    }
    public JobCodes(request: Requests.JobCodes): AbortablePromise<Responses.JobCodes> {
        return this._service.call(request, 'Ams/LaborCost/JobCodes');
    }
    public RequestCostsByRequest(request: Requests.RequestCostsByRequest): AbortablePromise<Responses.RequestCostsByRequest> {
        return this._service.call(request, 'Ams/LaborCost/RequestCostsByRequest');
    }
    public WorkOrderCostsByWorkOrder(request: Requests.WorkOrderCostsByWorkOrder): AbortablePromise<Responses.WorkOrderCostsByWorkOrder> {
        return this._service.call(request, 'Ams/LaborCost/WorkOrderCostsByWorkOrder');
    }
}