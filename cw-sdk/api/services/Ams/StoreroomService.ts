import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { StoreroomServiceTypes as SvcDef } from '../../interfaces/Ams/StoreroomService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import IStoreroomService = SvcDef.IStoreroomService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/StoreroomService';

export class StoreroomService implements IStoreroomService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Accounts(request: Requests.Accounts): AbortablePromise<Responses.Accounts> {
        return this._service.call(request, 'Ams/Storeroom/Accounts');
    }
}