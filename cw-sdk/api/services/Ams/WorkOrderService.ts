import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { WorkOrderServiceTypes as SvcDef } from '../../interfaces/Ams/WorkOrderService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import IWorkOrderService = SvcDef.IWorkOrderService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/WorkOrderService';

export class WorkOrderService implements IWorkOrderService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public AddComments(request: Requests.AddComments): AbortablePromise<Responses.AddComments> {
        return this._service.call(request, 'Ams/WorkOrder/AddComments');
    }
    public AddEntities(request: Requests.AddEntities): AbortablePromise<Responses.AddEntities> {
        return this._service.call(request, 'Ams/WorkOrder/AddEntities');
    }
    public ById(request: Requests.ById): AbortablePromise<Responses.ById> {
        return this._service.call(request, 'Ams/WorkOrder/ById');
    }
    public ByIds(request: Requests.ByIds): AbortablePromise<Responses.ByIds> {
        return this._service.call(request, 'Ams/WorkOrder/ByIds');
    }
    public Cancel(request: Requests.Cancel): AbortablePromise<Responses.Cancel> {
        return this._service.call(request, 'Ams/WorkOrder/Cancel');
    }
    public Categories(request: Requests.Categories): AbortablePromise<Responses.Categories> {
        return this._service.call(request, 'Ams/WorkOrder/Categories');
    }
    public Close(request: Requests.Close): AbortablePromise<Responses.Close> {
        return this._service.call(request, 'Ams/WorkOrder/Close');
    }
    public Comments(request: Requests.Comments): AbortablePromise<Responses.Comments> {
        return this._service.call(request, 'Ams/WorkOrder/Comments');
    }
    public CommentsByWorkOrderIds(request: Requests.CommentsByWorkOrderIds): AbortablePromise<Responses.CommentsByWorkOrderIds> {
        return this._service.call(request, 'Ams/WorkOrder/CommentsByWorkOrderIds');
    }
    public Create(request: Requests.Create): AbortablePromise<Responses.Create> {
        return this._service.call(request, 'Ams/WorkOrder/Create');
    }
    public CreateFromInspection(request: Requests.CreateFromInspection): AbortablePromise<Responses.CreateFromInspection> {
        return this._service.call(request, 'Ams/WorkOrder/CreateFromInspection');
    }
    public CreateFromParent(request: Requests.CreateFromParent): AbortablePromise<Responses.CreateFromParent> {
        return this._service.call(request, 'Ams/WorkOrder/CreateFromParent');
    }
    public CreateFromServiceRequest(request: Requests.CreateFromServiceRequest): AbortablePromise<Responses.CreateFromServiceRequest> {
        return this._service.call(request, 'Ams/WorkOrder/CreateFromServiceRequest');
    }
    public CustomFields(request: Requests.CustomFields): AbortablePromise<Responses.CustomFields> {
        return this._service.call(request, 'Ams/WorkOrder/CustomFields');
    }
    public CycleFrom(request: Requests.CycleFrom): AbortablePromise<Responses.CycleFrom> {
        return this._service.call(request, 'Ams/WorkOrder/CycleFrom');
    }
    public CycleIntervals(request: Requests.CycleIntervals): AbortablePromise<Responses.CycleIntervals> {
        return this._service.call(request, 'Ams/WorkOrder/CycleIntervals');
    }
    public CycleTypes(request: Requests.CycleTypes): AbortablePromise<Responses.CycleTypes> {
        return this._service.call(request, 'Ams/WorkOrder/CycleTypes');
    }
    public Entities(request: Requests.Entities): AbortablePromise<Responses.Entities> {
        return this._service.call(request, 'Ams/WorkOrder/Entities');
    }
    public ExpenseTypes(request: Requests.ExpenseTypes): AbortablePromise<Responses.ExpenseTypes> {
        return this._service.call(request, 'Ams/WorkOrder/ExpenseTypes');
    }
    public InstructionsByWorkOrderIds(request: Requests.InstructionsByWorkOrderIds): AbortablePromise<Responses.InstructionsByWorkOrderIds> {
        return this._service.call(request, 'Ams/WorkOrder/InstructionsByWorkOrderIds');
    }
    public LinkInspections(request: Requests.LinkInspections): AbortablePromise<Responses.LinkInspections> {
        return this._service.call(request, 'Ams/WorkOrder/LinkInspections');
    }
    public LinkServiceRequests(request: Requests.LinkServiceRequests): AbortablePromise<Responses.LinkServiceRequests> {
        return this._service.call(request, 'Ams/WorkOrder/LinkServiceRequests');
    }
    public Move(request: Requests.Move): AbortablePromise<Responses.Move> {
        return this._service.call(request, 'Ams/WorkOrder/Move');
    }
    public Priorities(request: Requests.Priorities): AbortablePromise<Responses.Priorities> {
        return this._service.call(request, 'Ams/WorkOrder/Priorities');
    }
    public RemoveEntities(request: Requests.RemoveEntities): AbortablePromise<Responses.RemoveEntities> {
        return this._service.call(request, 'Ams/WorkOrder/RemoveEntities');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'Ams/WorkOrder/Search');
    }
    public SearchObject(request: Requests.SearchObject): AbortablePromise<Responses.SearchObject> {
        return this._service.call(request, 'Ams/WorkOrder/SearchObject');
    }
    public Stages(request: Requests.Stages): AbortablePromise<Responses.Stages> {
        return this._service.call(request, 'Ams/WorkOrder/Stages');
    }
    public Statuses(request: Requests.Statuses): AbortablePromise<Responses.Statuses> {
        return this._service.call(request, 'Ams/WorkOrder/Statuses');
    }
    public SubmitTos(request: Requests.SubmitTos): AbortablePromise<Responses.SubmitTos> {
        return this._service.call(request, 'Ams/WorkOrder/SubmitTos');
    }
    public Template(request: Requests.Template): AbortablePromise<Responses.Template> {
        return this._service.call(request, 'Ams/WorkOrder/Template');
    }
    public TemplateCustomFields(request: Requests.TemplateCustomFields): AbortablePromise<Responses.TemplateCustomFields> {
        return this._service.call(request, 'Ams/WorkOrder/TemplateCustomFields');
    }
    public Templates(request: Requests.Templates): AbortablePromise<Responses.Templates> {
        return this._service.call(request, 'Ams/WorkOrder/Templates');
    }
    public UnlinkInspections(request: Requests.UnlinkInspections): AbortablePromise<Responses.UnlinkInspections> {
        return this._service.call(request, 'Ams/WorkOrder/UnlinkInspections');
    }
    public UnlinkServiceRequests(request: Requests.UnlinkServiceRequests): AbortablePromise<Responses.UnlinkServiceRequests> {
        return this._service.call(request, 'Ams/WorkOrder/UnlinkServiceRequests');
    }
    public Update(request: Requests.Update): AbortablePromise<Responses.Update> {
        return this._service.call(request, 'Ams/WorkOrder/Update');
    }
    public UpdateEntity(request: Requests.UpdateEntity): AbortablePromise<Responses.UpdateEntity> {
        return this._service.call(request, 'Ams/WorkOrder/UpdateEntity');
    }
    public WorkOrderInspections(request: Requests.WorkOrderInspections): AbortablePromise<Responses.WorkOrderInspections> {
        return this._service.call(request, 'Ams/WorkOrder/WorkOrderInspections');
    }
    public WorkOrderServiceRequests(request: Requests.WorkOrderServiceRequests): AbortablePromise<Responses.WorkOrderServiceRequests> {
        return this._service.call(request, 'Ams/WorkOrder/WorkOrderServiceRequests');
    }
    public WorkOrderWorkOrders(request: Requests.WorkOrderWorkOrders): AbortablePromise<Responses.WorkOrderWorkOrders> {
        return this._service.call(request, 'Ams/WorkOrder/WorkOrderWorkOrders');
    }
}