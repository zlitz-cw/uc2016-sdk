import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { ConditionServiceTypes as SvcDef } from '../../interfaces/Ams/ConditionService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import IConditionService = SvcDef.IConditionService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/ConditionService';

export class ConditionService implements IConditionService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public History(request: Requests.History): AbortablePromise<Responses.History> {
        return this._service.call(request, 'Ams/Condition/History');
    }
}