import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { CommentServiceTypes as SvcDef } from '../../interfaces/Ams/CommentService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ICommentService = SvcDef.ICommentService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/CommentService';

export class CommentService implements ICommentService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Add(request: Requests.Add): AbortablePromise<Responses.Add> {
        return this._service.call(request, 'Ams/Comment/Add');
    }
    public ByActivityIds(request: Requests.ByActivityIds): AbortablePromise<Responses.ByActivityIds> {
        return this._service.call(request, 'Ams/Comment/ByActivityIds');
    }
    public Update(request: Requests.Update): AbortablePromise<Responses.Update> {
        return this._service.call(request, 'Ams/Comment/Update');
    }
}