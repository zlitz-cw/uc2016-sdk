import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { ReadingServiceTypes as SvcDef } from '../../interfaces/Ams/ReadingService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import IReadingService = SvcDef.IReadingService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/ReadingService';

export class ReadingService implements IReadingService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Add(request: Requests.Add): AbortablePromise<Responses.Add> {
        return this._service.call(request, 'Ams/Reading/Add');
    }
    public ByEntity(request: Requests.ByEntity): AbortablePromise<Responses.ByEntity> {
        return this._service.call(request, 'Ams/Reading/ByEntity');
    }
    public Configuration(request: Requests.Configuration): AbortablePromise<Responses.Configuration> {
        return this._service.call(request, 'Ams/Reading/Configuration');
    }
    public Update(request: Requests.Update): AbortablePromise<Responses.Update> {
        return this._service.call(request, 'Ams/Reading/Update');
    }
}