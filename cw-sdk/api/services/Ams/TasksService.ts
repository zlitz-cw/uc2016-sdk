import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { TasksServiceTypes as SvcDef } from '../../interfaces/Ams/TasksService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ITasksService = SvcDef.ITasksService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/TasksService';

export class TasksService implements ITasksService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Add(request: Requests.Add): AbortablePromise<Responses.Add> {
        return this._service.call(request, 'Ams/Tasks/Add');
    }
    public ById(request: Requests.ById): AbortablePromise<Responses.ById> {
        return this._service.call(request, 'Ams/Tasks/ById');
    }
    public ByWorkOrder(request: Requests.ByWorkOrder): AbortablePromise<Responses.ByWorkOrder> {
        return this._service.call(request, 'Ams/Tasks/ByWorkOrder');
    }
    public ByWorkOrderTemplate(request: Requests.ByWorkOrderTemplate): AbortablePromise<Responses.ByWorkOrderTemplate> {
        return this._service.call(request, 'Ams/Tasks/ByWorkOrderTemplate');
    }
    public Delete(request: Requests.Delete): AbortablePromise<Responses.Delete> {
        return this._service.call(request, 'Ams/Tasks/Delete');
    }
    public Keywords(request: Requests.Keywords): AbortablePromise<Responses.Keywords> {
        return this._service.call(request, 'Ams/Tasks/Keywords');
    }
    public Statuses(request: Requests.Statuses): AbortablePromise<Responses.Statuses> {
        return this._service.call(request, 'Ams/Tasks/Statuses');
    }
    public TaskCodes(request: Requests.TaskCodes): AbortablePromise<Responses.TaskCodes> {
        return this._service.call(request, 'Ams/Tasks/TaskCodes');
    }
    public TaskNodes(request: Requests.TaskNodes): AbortablePromise<Responses.TaskNodes> {
        return this._service.call(request, 'Ams/Tasks/TaskNodes');
    }
    public Template(request: Requests.Template): AbortablePromise<Responses.Template> {
        return this._service.call(request, 'Ams/Tasks/Template');
    }
    public Templates(request: Requests.Templates): AbortablePromise<Responses.Templates> {
        return this._service.call(request, 'Ams/Tasks/Templates');
    }
    public Update(request: Requests.Update): AbortablePromise<Responses.Update> {
        return this._service.call(request, 'Ams/Tasks/Update');
    }
}