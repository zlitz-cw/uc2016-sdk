import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { CodesServiceTypes as SvcDef } from '../../interfaces/Ams/CodesService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ICodesService = SvcDef.ICodesService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/CodesService';

export class CodesService implements ICodesService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public All(request: Requests.All): AbortablePromise<Responses.All> {
        return this._service.call(request, 'Ams/Codes/All');
    }
    public AllCCTVCodeDescScore(request: Requests.AllCCTVCodeDescScore): AbortablePromise<Responses.AllCCTVCodeDescScore> {
        return this._service.call(request, 'Ams/Codes/AllCCTVCodeDescScore');
    }
    public AllDescScore(request: Requests.AllDescScore): AbortablePromise<Responses.AllDescScore> {
        return this._service.call(request, 'Ams/Codes/AllDescScore');
    }
    public ByCodeType(request: Requests.ByCodeType): AbortablePromise<Responses.ByCodeType> {
        return this._service.call(request, 'Ams/Codes/ByCodeType');
    }
    public Import(request: Requests.Import): AbortablePromise<Responses.Import> {
        return this._service.call(request, 'Ams/Codes/Import');
    }
    public ImportCCTVCodeDescScore(request: Requests.ImportCCTVCodeDescScore): AbortablePromise<Responses.ImportCCTVCodeDescScore> {
        return this._service.call(request, 'Ams/Codes/ImportCCTVCodeDescScore');
    }
    public ImportDescScore(request: Requests.ImportDescScore): AbortablePromise<Responses.ImportDescScore> {
        return this._service.call(request, 'Ams/Codes/ImportDescScore');
    }
}