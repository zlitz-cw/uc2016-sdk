import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { MaterialServiceTypes as SvcDef } from '../../interfaces/Ams/MaterialService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import IMaterialService = SvcDef.IMaterialService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/MaterialService';

export class MaterialService implements IMaterialService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public All(request: Requests.All): AbortablePromise<Responses.All> {
        return this._service.call(request, 'Ams/Material/All');
    }
    public ById(request: Requests.ById): AbortablePromise<Responses.ById> {
        return this._service.call(request, 'Ams/Material/ById');
    }
    public ByIds(request: Requests.ByIds): AbortablePromise<Responses.ByIds> {
        return this._service.call(request, 'Ams/Material/ByIds');
    }
    public ByParent(request: Requests.ByParent): AbortablePromise<Responses.ByParent> {
        return this._service.call(request, 'Ams/Material/ByParent');
    }
    public ByStoreroom(request: Requests.ByStoreroom): AbortablePromise<Responses.ByStoreroom> {
        return this._service.call(request, 'Ams/Material/ByStoreroom');
    }
    public ByStoreroomAndId(request: Requests.ByStoreroomAndId): AbortablePromise<Responses.ByStoreroomAndId> {
        return this._service.call(request, 'Ams/Material/ByStoreroomAndId');
    }
    public Keywords(request: Requests.Keywords): AbortablePromise<Responses.Keywords> {
        return this._service.call(request, 'Ams/Material/Keywords');
    }
    public MaterialBom(request: Requests.MaterialBom): AbortablePromise<Responses.MaterialBom> {
        return this._service.call(request, 'Ams/Material/MaterialBom');
    }
    public MaterialCategories(request: Requests.MaterialCategories): AbortablePromise<Responses.MaterialCategories> {
        return this._service.call(request, 'Ams/Material/MaterialCategories');
    }
    public MaterialNodes(request: Requests.MaterialNodes): AbortablePromise<Responses.MaterialNodes> {
        return this._service.call(request, 'Ams/Material/MaterialNodes');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'Ams/Material/Search');
    }
}