import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { SecurityServiceTypes as SvcDef } from '../../interfaces/Ams/SecurityService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ISecurityService = SvcDef.ISecurityService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/SecurityService';

export class SecurityService implements ISecurityService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Inspection(request: Requests.Inspection): AbortablePromise<Responses.Inspection> {
        return this._service.call(request, 'Ams/Security/Inspection');
    }
    public Inspections(request: Requests.Inspections): AbortablePromise<Responses.Inspections> {
        return this._service.call(request, 'Ams/Security/Inspections');
    }
    public InspectionTemplate(request: Requests.InspectionTemplate): AbortablePromise<Responses.InspectionTemplate> {
        return this._service.call(request, 'Ams/Security/InspectionTemplate');
    }
    public InspectionTemplates(request: Requests.InspectionTemplates): AbortablePromise<Responses.InspectionTemplates> {
        return this._service.call(request, 'Ams/Security/InspectionTemplates');
    }
    public Problem(request: Requests.Problem): AbortablePromise<Responses.Problem> {
        return this._service.call(request, 'Ams/Security/Problem');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'Ams/Security/Search');
    }
    public ServiceRequest(request: Requests.ServiceRequest): AbortablePromise<Responses.ServiceRequest> {
        return this._service.call(request, 'Ams/Security/ServiceRequest');
    }
    public ServiceRequests(request: Requests.ServiceRequests): AbortablePromise<Responses.ServiceRequests> {
        return this._service.call(request, 'Ams/Security/ServiceRequests');
    }
    public WorkOrder(request: Requests.WorkOrder): AbortablePromise<Responses.WorkOrder> {
        return this._service.call(request, 'Ams/Security/WorkOrder');
    }
    public WorkOrders(request: Requests.WorkOrders): AbortablePromise<Responses.WorkOrders> {
        return this._service.call(request, 'Ams/Security/WorkOrders');
    }
    public WorkOrderTemplate(request: Requests.WorkOrderTemplate): AbortablePromise<Responses.WorkOrderTemplate> {
        return this._service.call(request, 'Ams/Security/WorkOrderTemplate');
    }
}