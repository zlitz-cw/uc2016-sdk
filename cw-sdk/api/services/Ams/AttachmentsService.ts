import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { AttachmentsServiceTypes as SvcDef } from '../../interfaces/Ams/AttachmentsService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import IAttachmentsService = SvcDef.IAttachmentsService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/AttachmentsService';

export class AttachmentsService implements IAttachmentsService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public AddInspectionAttachment(request: Requests.AddInspectionAttachment): AbortablePromise<Responses.AddInspectionAttachment> {
        return this._service.call(request, 'Ams/Attachments/AddInspectionAttachment');
    }
    public AddRequestAttachment(request: Requests.AddRequestAttachment): AbortablePromise<Responses.AddRequestAttachment> {
        return this._service.call(request, 'Ams/Attachments/AddRequestAttachment');
    }
    public AddWorkOrderAttachment(request: Requests.AddWorkOrderAttachment): AbortablePromise<Responses.AddWorkOrderAttachment> {
        return this._service.call(request, 'Ams/Attachments/AddWorkOrderAttachment');
    }
    public DeleteInspectionAttachments(request: Requests.DeleteInspectionAttachments): AbortablePromise<Responses.DeleteInspectionAttachments> {
        return this._service.call(request, 'Ams/Attachments/DeleteInspectionAttachments');
    }
    public DeleteRequestAttachments(request: Requests.DeleteRequestAttachments): AbortablePromise<Responses.DeleteRequestAttachments> {
        return this._service.call(request, 'Ams/Attachments/DeleteRequestAttachments');
    }
    public DeleteWorkOrderAttachments(request: Requests.DeleteWorkOrderAttachments): AbortablePromise<Responses.DeleteWorkOrderAttachments> {
        return this._service.call(request, 'Ams/Attachments/DeleteWorkOrderAttachments');
    }
    public DownloadInspectionAttachment(request: Requests.DownloadInspectionAttachment): AbortablePromise<Responses.DownloadInspectionAttachment> {
        return this._service.call(request, 'Ams/Attachments/DownloadInspectionAttachment');
    }
    public DownloadRequestAttachment(request: Requests.DownloadRequestAttachment): AbortablePromise<Responses.DownloadRequestAttachment> {
        return this._service.call(request, 'Ams/Attachments/DownloadRequestAttachment');
    }
    public DownloadWorkOrderAttachment(request: Requests.DownloadWorkOrderAttachment): AbortablePromise<Responses.DownloadWorkOrderAttachment> {
        return this._service.call(request, 'Ams/Attachments/DownloadWorkOrderAttachment');
    }
    public InspectionAttachmentById(request: Requests.InspectionAttachmentById): AbortablePromise<Responses.InspectionAttachmentById> {
        return this._service.call(request, 'Ams/Attachments/InspectionAttachmentById');
    }
    public InspectionAttachments(request: Requests.InspectionAttachments): AbortablePromise<Responses.InspectionAttachments> {
        return this._service.call(request, 'Ams/Attachments/InspectionAttachments');
    }
    public RequestAttachmentById(request: Requests.RequestAttachmentById): AbortablePromise<Responses.RequestAttachmentById> {
        return this._service.call(request, 'Ams/Attachments/RequestAttachmentById');
    }
    public RequestAttachments(request: Requests.RequestAttachments): AbortablePromise<Responses.RequestAttachments> {
        return this._service.call(request, 'Ams/Attachments/RequestAttachments');
    }
    public WorkOrderAttachments(request: Requests.WorkOrderAttachments): AbortablePromise<Responses.WorkOrderAttachments> {
        return this._service.call(request, 'Ams/Attachments/WorkOrderAttachments');
    }
}