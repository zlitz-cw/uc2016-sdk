import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { InboxServiceTypes as SvcDef } from '../../interfaces/Ams/InboxService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import IInboxService = SvcDef.IInboxService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/InboxService';

export class InboxService implements IInboxService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public CopyWidgetContainerTab(request: Requests.CopyWidgetContainerTab): AbortablePromise<Responses.CopyWidgetContainerTab> {
        return this._service.call(request, 'Ams/Inbox/CopyWidgetContainerTab');
    }
    public CreateWidget(request: Requests.CreateWidget): AbortablePromise<Responses.CreateWidget> {
        return this._service.call(request, 'Ams/Inbox/CreateWidget');
    }
    public CreateWidgetContainer(request: Requests.CreateWidgetContainer): AbortablePromise<Responses.CreateWidgetContainer> {
        return this._service.call(request, 'Ams/Inbox/CreateWidgetContainer');
    }
    public CreateWidgetContainerTab(request: Requests.CreateWidgetContainerTab): AbortablePromise<Responses.CreateWidgetContainerTab> {
        return this._service.call(request, 'Ams/Inbox/CreateWidgetContainerTab');
    }
    public CreateWidgetZone(request: Requests.CreateWidgetZone): AbortablePromise<Responses.CreateWidgetZone> {
        return this._service.call(request, 'Ams/Inbox/CreateWidgetZone');
    }
    public DeleteWidget(request: Requests.DeleteWidget): AbortablePromise<Responses.DeleteWidget> {
        return this._service.call(request, 'Ams/Inbox/DeleteWidget');
    }
    public DeleteWidgetContainer(request: Requests.DeleteWidgetContainer): AbortablePromise<Responses.DeleteWidgetContainer> {
        return this._service.call(request, 'Ams/Inbox/DeleteWidgetContainer');
    }
    public DeleteWidgetContainerTab(request: Requests.DeleteWidgetContainerTab): AbortablePromise<Responses.DeleteWidgetContainerTab> {
        return this._service.call(request, 'Ams/Inbox/DeleteWidgetContainerTab');
    }
    public DeleteWidgetZone(request: Requests.DeleteWidgetZone): AbortablePromise<Responses.DeleteWidgetZone> {
        return this._service.call(request, 'Ams/Inbox/DeleteWidgetZone');
    }
    public MoveWidget(request: Requests.MoveWidget): AbortablePromise<Responses.MoveWidget> {
        return this._service.call(request, 'Ams/Inbox/MoveWidget');
    }
    public MoveWidgetContainerTab(request: Requests.MoveWidgetContainerTab): AbortablePromise<Responses.MoveWidgetContainerTab> {
        return this._service.call(request, 'Ams/Inbox/MoveWidgetContainerTab');
    }
    public MoveWidgetZone(request: Requests.MoveWidgetZone): AbortablePromise<Responses.MoveWidgetZone> {
        return this._service.call(request, 'Ams/Inbox/MoveWidgetZone');
    }
    public UpdateChartWidget(request: Requests.UpdateChartWidget): AbortablePromise<Responses.UpdateChartWidget> {
        return this._service.call(request, 'Ams/Inbox/UpdateChartWidget');
    }
    public UpdateHtmlWidget(request: Requests.UpdateHtmlWidget): AbortablePromise<Responses.UpdateHtmlWidget> {
        return this._service.call(request, 'Ams/Inbox/UpdateHtmlWidget');
    }
    public UpdateNotesWidget(request: Requests.UpdateNotesWidget): AbortablePromise<Responses.UpdateNotesWidget> {
        return this._service.call(request, 'Ams/Inbox/UpdateNotesWidget');
    }
    public UpdateReportLinksWidget(request: Requests.UpdateReportLinksWidget): AbortablePromise<Responses.UpdateReportLinksWidget> {
        return this._service.call(request, 'Ams/Inbox/UpdateReportLinksWidget');
    }
    public UpdateSearchResultsWidget(request: Requests.UpdateSearchResultsWidget): AbortablePromise<Responses.UpdateSearchResultsWidget> {
        return this._service.call(request, 'Ams/Inbox/UpdateSearchResultsWidget');
    }
    public UpdateWeatherWidget(request: Requests.UpdateWeatherWidget): AbortablePromise<Responses.UpdateWeatherWidget> {
        return this._service.call(request, 'Ams/Inbox/UpdateWeatherWidget');
    }
    public UpdateWidgetContainer(request: Requests.UpdateWidgetContainer): AbortablePromise<Responses.UpdateWidgetContainer> {
        return this._service.call(request, 'Ams/Inbox/UpdateWidgetContainer');
    }
    public UpdateWidgetContainerTab(request: Requests.UpdateWidgetContainerTab): AbortablePromise<Responses.UpdateWidgetContainerTab> {
        return this._service.call(request, 'Ams/Inbox/UpdateWidgetContainerTab');
    }
    public UpdateWidgetZone(request: Requests.UpdateWidgetZone): AbortablePromise<Responses.UpdateWidgetZone> {
        return this._service.call(request, 'Ams/Inbox/UpdateWidgetZone');
    }
}