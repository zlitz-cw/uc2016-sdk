import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { SearchServiceTypes as SvcDef } from '../../interfaces/Ams/SearchService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ISearchService = SvcDef.ISearchService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/SearchService';

export class SearchService implements ISearchService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Definition(request: Requests.Definition): AbortablePromise<Responses.Definition> {
        return this._service.call(request, 'Ams/Search/Definition');
    }
    public Definitions(request: Requests.Definitions): AbortablePromise<Responses.Definitions> {
        return this._service.call(request, 'Ams/Search/Definitions');
    }
    public Execute(request: Requests.Execute): AbortablePromise<Responses.Execute> {
        return this._service.call(request, 'Ams/Search/Execute');
    }
    public GISSaved(request: Requests.GISSaved): AbortablePromise<Responses.Saved> {
        return this._service.call(request, 'Ams/Search/GISSaved');
    }
    public InspectionSaved(request: Requests.InspectionSaved): AbortablePromise<Responses.Saved> {
        return this._service.call(request, 'Ams/Search/InspectionSaved');
    }
    public MaterialSaved(request: Requests.MaterialSaved): AbortablePromise<Responses.Saved> {
        return this._service.call(request, 'Ams/Search/MaterialSaved');
    }
    public PllSaved(request: Requests.PllSaved): AbortablePromise<Responses.Saved> {
        return this._service.call(request, 'Ams/Search/PllSaved');
    }
    public RecentActivity(request: Requests.RecentActivity): AbortablePromise<Responses.RecentActivity> {
        return this._service.call(request, 'Ams/Search/RecentActivity');
    }
    public RequestSaved(request: Requests.RequestSaved): AbortablePromise<Responses.Saved> {
        return this._service.call(request, 'Ams/Search/RequestSaved');
    }
    public RequisitionSaved(request: Requests.RequisitionSaved): AbortablePromise<Responses.Saved> {
        return this._service.call(request, 'Ams/Search/RequisitionSaved');
    }
    public SavedByType(request: Requests.SavedByType): AbortablePromise<Responses.Saved> {
        return this._service.call(request, 'Ams/Search/SavedByType');
    }
    public StoreTransactionSaved(request: Requests.StoreTransactionSaved): AbortablePromise<Responses.Saved> {
        return this._service.call(request, 'Ams/Search/StoreTransactionSaved');
    }
    public Types(request: Requests.Types): AbortablePromise<Responses.Types> {
        return this._service.call(request, 'Ams/Search/Types');
    }
    public WorkActivitySaved(request: Requests.WorkActivitySaved): AbortablePromise<Responses.Saved> {
        return this._service.call(request, 'Ams/Search/WorkActivitySaved');
    }
    public WorkOrderEntitySaved(request: Requests.WorkOrderEntitySaved): AbortablePromise<Responses.Saved> {
        return this._service.call(request, 'Ams/Search/WorkOrderEntitySaved');
    }
    public WorkOrderSaved(request: Requests.WorkOrderSaved): AbortablePromise<Responses.Saved> {
        return this._service.call(request, 'Ams/Search/WorkOrderSaved');
    }
}