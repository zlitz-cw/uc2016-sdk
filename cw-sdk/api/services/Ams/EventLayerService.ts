import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { EventLayerServiceTypes as SvcDef } from '../../interfaces/Ams/EventLayerService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import IEventLayerService = SvcDef.IEventLayerService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/EventLayerService';

export class EventLayerService implements IEventLayerService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public All(request: Requests.All): AbortablePromise<Responses.All> {
        return this._service.call(request, 'Ams/EventLayer/All');
    }
    public ById(request: Requests.ById): AbortablePromise<Responses.ById> {
        return this._service.call(request, 'Ams/EventLayer/ById');
    }
}