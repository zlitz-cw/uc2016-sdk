import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { CrewServiceTypes as SvcDef } from '../../interfaces/Ams/CrewService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ICrewService = SvcDef.ICrewService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/CrewService';

export class CrewService implements ICrewService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public ByEmployee(request: Requests.ByEmployee): AbortablePromise<Responses.ByEmployee> {
        return this._service.call(request, 'Ams/Crew/ByEmployee');
    }
    public ByIds(request: Requests.ByIds): AbortablePromise<Responses.ByIds> {
        return this._service.call(request, 'Ams/Crew/ByIds');
    }
    public Employees(request: Requests.Employees): AbortablePromise<Responses.Employees> {
        return this._service.call(request, 'Ams/Crew/Employees');
    }
    public Equipment(request: Requests.Equipment): AbortablePromise<Responses.Equipment> {
        return this._service.call(request, 'Ams/Crew/Equipment');
    }
    public Material(request: Requests.Material): AbortablePromise<Responses.Material> {
        return this._service.call(request, 'Ams/Crew/Material');
    }
}