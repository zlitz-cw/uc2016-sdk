import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { RecentActivityServiceTypes as SvcDef } from '../../interfaces/Ams/RecentActivityService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import IRecentActivityService = SvcDef.IRecentActivityService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/RecentActivityService';

export class RecentActivityService implements IRecentActivityService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public CaseViewed(request: Requests.CaseViewed): AbortablePromise<Responses.CaseViewed> {
        return this._service.call(request, 'Ams/RecentActivity/CaseViewed');
    }
    public InspectionViewed(request: Requests.InspectionViewed): AbortablePromise<Responses.InspectionViewed> {
        return this._service.call(request, 'Ams/RecentActivity/InspectionViewed');
    }
    public RequestViewed(request: Requests.RequestViewed): AbortablePromise<Responses.RequestViewed> {
        return this._service.call(request, 'Ams/RecentActivity/RequestViewed');
    }
    public User(request: Requests.User): AbortablePromise<Responses.User> {
        return this._service.call(request, 'Ams/RecentActivity/User');
    }
    public WorkOrderViewed(request: Requests.WorkOrderViewed): AbortablePromise<Responses.WorkOrderViewed> {
        return this._service.call(request, 'Ams/RecentActivity/WorkOrderViewed');
    }
}