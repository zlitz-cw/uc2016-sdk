import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { MaterialCostServiceTypes as SvcDef } from '../../interfaces/Ams/MaterialCostService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import IMaterialCostService = SvcDef.IMaterialCostService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/MaterialCostService';

export class MaterialCostService implements IMaterialCostService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public AddWorkOrderCosts(request: Requests.AddWorkOrderCosts): AbortablePromise<Responses.AddWorkOrderCosts> {
        return this._service.call(request, 'Ams/MaterialCost/AddWorkOrderCosts');
    }
    public DeleteWorkOrderCosts(request: Requests.DeleteWorkOrderCosts): AbortablePromise<Responses.DeleteWorkOrderCosts> {
        return this._service.call(request, 'Ams/MaterialCost/DeleteWorkOrderCosts');
    }
    public WorkOrderCostsByWorkOrder(request: Requests.WorkOrderCostsByWorkOrder): AbortablePromise<Responses.WorkOrderCostsByWorkOrder> {
        return this._service.call(request, 'Ams/MaterialCost/WorkOrderCostsByWorkOrder');
    }
}