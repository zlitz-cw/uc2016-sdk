import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { ServiceRequestServiceTypes as SvcDef } from '../../interfaces/Ams/ServiceRequestService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import IServiceRequestService = SvcDef.IServiceRequestService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/ServiceRequestService';

export class ServiceRequestService implements IServiceRequestService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public AddComments(request: Requests.AddComments): AbortablePromise<Responses.AddComments> {
        return this._service.call(request, 'Ams/ServiceRequest/AddComments');
    }
    public AllStreetCode(request: Requests.AllStreetCode): AbortablePromise<Responses.AllStreetCode> {
        return this._service.call(request, 'Ams/ServiceRequest/AllStreetCode');
    }
    public ById(request: Requests.ById): AbortablePromise<Responses.ById> {
        return this._service.call(request, 'Ams/ServiceRequest/ById');
    }
    public ByIds(request: Requests.ByIds): AbortablePromise<Responses.ByIds> {
        return this._service.call(request, 'Ams/ServiceRequest/ByIds');
    }
    public ByIncidentAndEmail(request: Requests.ByIncidentAndEmail): AbortablePromise<Responses.ByIncidentAndEmail> {
        return this._service.call(request, 'Ams/ServiceRequest/ByIncidentAndEmail');
    }
    public ByOtherSystemId(request: Requests.ByOtherSystemId): AbortablePromise<Responses.ByOtherSystemId> {
        return this._service.call(request, 'Ams/ServiceRequest/ByOtherSystemId');
    }
    public Cancel(request: Requests.Cancel): AbortablePromise<Responses.Cancel> {
        return this._service.call(request, 'Ams/ServiceRequest/Cancel');
    }
    public Close(request: Requests.Close): AbortablePromise<Responses.Close> {
        return this._service.call(request, 'Ams/ServiceRequest/Close');
    }
    public Comments(request: Requests.Comments): AbortablePromise<Responses.Comments> {
        return this._service.call(request, 'Ams/ServiceRequest/Comments');
    }
    public CommentsByRequestIds(request: Requests.CommentsByRequestIds): AbortablePromise<Responses.CommentsByRequestIds> {
        return this._service.call(request, 'Ams/ServiceRequest/CommentsByRequestIds');
    }
    public Create(request: Requests.Create): AbortablePromise<Responses.Create> {
        return this._service.call(request, 'Ams/ServiceRequest/Create');
    }
    public CustomFieldCategories(request: Requests.CustomFieldCategories): AbortablePromise<Responses.CustomFieldCategories> {
        return this._service.call(request, 'Ams/ServiceRequest/CustomFieldCategories');
    }
    public CustomFields(request: Requests.CustomFields): AbortablePromise<Responses.CustomFields> {
        return this._service.call(request, 'Ams/ServiceRequest/CustomFields');
    }
    public LinkInspections(request: Requests.LinkInspections): AbortablePromise<Responses.LinkInspections> {
        return this._service.call(request, 'Ams/ServiceRequest/LinkInspections');
    }
    public LinkWorkOrders(request: Requests.LinkWorkOrders): AbortablePromise<Responses.LinkWorkOrders> {
        return this._service.call(request, 'Ams/ServiceRequest/LinkWorkOrders');
    }
    public ProblemLeafByOtherSysCodeDescs(request: Requests.ProblemLeafByOtherSysCodeDescs): AbortablePromise<Responses.ProblemLeafByOtherSysCodeDescs> {
        return this._service.call(request, 'Ams/ServiceRequest/ProblemLeafByOtherSysCodeDescs');
    }
    public ProblemLeafBySid(request: Requests.ProblemLeafBySid): AbortablePromise<Responses.ProblemLeafBySid> {
        return this._service.call(request, 'Ams/ServiceRequest/ProblemLeafBySid');
    }
    public ProblemNodes(request: Requests.ProblemNodes): AbortablePromise<Responses.ProblemNodes> {
        return this._service.call(request, 'Ams/ServiceRequest/ProblemNodes');
    }
    public Problems(request: Requests.Problems): AbortablePromise<Responses.Problems> {
        return this._service.call(request, 'Ams/ServiceRequest/Problems');
    }
    public QA(request: Requests.QA): AbortablePromise<Responses.QA> {
        return this._service.call(request, 'Ams/ServiceRequest/QA');
    }
    public RequestInspections(request: Requests.RequestInspections): AbortablePromise<Responses.RequestInspections> {
        return this._service.call(request, 'Ams/ServiceRequest/RequestInspections');
    }
    public RequestWorkOrders(request: Requests.RequestWorkOrders): AbortablePromise<Responses.RequestWorkOrders> {
        return this._service.call(request, 'Ams/ServiceRequest/RequestWorkOrders');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'Ams/ServiceRequest/Search');
    }
    public SearchObject(request: Requests.SearchObject): AbortablePromise<Responses.SearchObject> {
        return this._service.call(request, 'Ams/ServiceRequest/SearchObject');
    }
    public Statuses(request: Requests.Statuses): AbortablePromise<Responses.Statuses> {
        return this._service.call(request, 'Ams/ServiceRequest/Statuses');
    }
    public TemplateCustomFields(request: Requests.TemplateCustomFields): AbortablePromise<Responses.TemplateCustomFields> {
        return this._service.call(request, 'Ams/ServiceRequest/TemplateCustomFields');
    }
    public UnlinkInspections(request: Requests.UnlinkInspections): AbortablePromise<Responses.UnlinkInspections> {
        return this._service.call(request, 'Ams/ServiceRequest/UnlinkInspections');
    }
    public UnlinkWorkOrders(request: Requests.UnlinkWorkOrders): AbortablePromise<Responses.UnlinkWorkOrders> {
        return this._service.call(request, 'Ams/ServiceRequest/UnlinkWorkOrders');
    }
    public Update(request: Requests.Update): AbortablePromise<Responses.Update> {
        return this._service.call(request, 'Ams/ServiceRequest/Update');
    }
}