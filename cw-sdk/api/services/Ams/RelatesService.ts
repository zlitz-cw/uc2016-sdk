import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { RelatesServiceTypes as SvcDef } from '../../interfaces/Ams/RelatesService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import IRelatesService = SvcDef.IRelatesService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/RelatesService';

export class RelatesService implements IRelatesService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public EquipChangeOutChangedBy(request: Requests.EquipChangeOutChangedBy): AbortablePromise<Responses.EmployeeRelates> {
        return this._service.call(request, 'Ams/Relates/EquipChangeOutChangedBy');
    }
    public InspectionCancelledBy(request: Requests.InspectionCancelledBy): AbortablePromise<Responses.EmployeeRelates> {
        return this._service.call(request, 'Ams/Relates/InspectionCancelledBy');
    }
    public InspectionClosedBy(request: Requests.InspectionClosedBy): AbortablePromise<Responses.EmployeeRelates> {
        return this._service.call(request, 'Ams/Relates/InspectionClosedBy');
    }
    public InspectionEditAfterClose(request: Requests.InspectionEditAfterClose): AbortablePromise<Responses.EmployeeRelates> {
        return this._service.call(request, 'Ams/Relates/InspectionEditAfterClose');
    }
    public InspectionInitiatedBy(request: Requests.InspectionInitiatedBy): AbortablePromise<Responses.EmployeeRelates> {
        return this._service.call(request, 'Ams/Relates/InspectionInitiatedBy');
    }
    public InspectionInspectedBy(request: Requests.InspectionInspectedBy): AbortablePromise<Responses.EmployeeRelates> {
        return this._service.call(request, 'Ams/Relates/InspectionInspectedBy');
    }
    public InspectionSubmitTo(request: Requests.InspectionSubmitTo): AbortablePromise<Responses.EmployeeRelates> {
        return this._service.call(request, 'Ams/Relates/InspectionSubmitTo');
    }
    public ProjectApprovedBy(request: Requests.ProjectApprovedBy): AbortablePromise<Responses.EmployeeRelates> {
        return this._service.call(request, 'Ams/Relates/ProjectApprovedBy');
    }
    public ProjectAssignedTo(request: Requests.ProjectAssignedTo): AbortablePromise<Responses.EmployeeRelates> {
        return this._service.call(request, 'Ams/Relates/ProjectAssignedTo');
    }
    public ProjectInitiatedBy(request: Requests.ProjectInitiatedBy): AbortablePromise<Responses.EmployeeRelates> {
        return this._service.call(request, 'Ams/Relates/ProjectInitiatedBy');
    }
    public RequestCancelledBy(request: Requests.RequestCancelledBy): AbortablePromise<Responses.EmployeeRelates> {
        return this._service.call(request, 'Ams/Relates/RequestCancelledBy');
    }
    public RequestClosedBy(request: Requests.RequestClosedBy): AbortablePromise<Responses.EmployeeRelates> {
        return this._service.call(request, 'Ams/Relates/RequestClosedBy');
    }
    public RequestDispatchTo(request: Requests.RequestDispatchTo): AbortablePromise<Responses.EmployeeRelates> {
        return this._service.call(request, 'Ams/Relates/RequestDispatchTo');
    }
    public RequestEditAfterClose(request: Requests.RequestEditAfterClose): AbortablePromise<Responses.EmployeeRelates> {
        return this._service.call(request, 'Ams/Relates/RequestEditAfterClose');
    }
    public RequestInitiatedBy(request: Requests.RequestInitiatedBy): AbortablePromise<Responses.EmployeeRelates> {
        return this._service.call(request, 'Ams/Relates/RequestInitiatedBy');
    }
    public RequestSubmitTo(request: Requests.RequestSubmitTo): AbortablePromise<Responses.EmployeeRelates> {
        return this._service.call(request, 'Ams/Relates/RequestSubmitTo');
    }
    public SearchViewCost(request: Requests.SearchViewCost): AbortablePromise<Responses.EmployeeRelates> {
        return this._service.call(request, 'Ams/Relates/SearchViewCost');
    }
    public TaskAssignTo(request: Requests.TaskAssignTo): AbortablePromise<Responses.EmployeeRelates> {
        return this._service.call(request, 'Ams/Relates/TaskAssignTo');
    }
    public WorkOrderCancelledBy(request: Requests.WorkOrderCancelledBy): AbortablePromise<Responses.EmployeeRelates> {
        return this._service.call(request, 'Ams/Relates/WorkOrderCancelledBy');
    }
    public WorkOrderClosedBy(request: Requests.WorkOrderClosedBy): AbortablePromise<Responses.EmployeeRelates> {
        return this._service.call(request, 'Ams/Relates/WorkOrderClosedBy');
    }
    public WorkOrderCompletedBy(request: Requests.WorkOrderCompletedBy): AbortablePromise<Responses.EmployeeRelates> {
        return this._service.call(request, 'Ams/Relates/WorkOrderCompletedBy');
    }
    public WorkOrderEditAfterClose(request: Requests.WorkOrderEditAfterClose): AbortablePromise<Responses.EmployeeRelates> {
        return this._service.call(request, 'Ams/Relates/WorkOrderEditAfterClose');
    }
    public WorkOrderInitiatedBy(request: Requests.WorkOrderInitiatedBy): AbortablePromise<Responses.EmployeeRelates> {
        return this._service.call(request, 'Ams/Relates/WorkOrderInitiatedBy');
    }
    public WorkOrderRequestedBy(request: Requests.WorkOrderRequestedBy): AbortablePromise<Responses.EmployeeRelates> {
        return this._service.call(request, 'Ams/Relates/WorkOrderRequestedBy');
    }
    public WorkOrderSubmitTo(request: Requests.WorkOrderSubmitTo): AbortablePromise<Responses.EmployeeRelates> {
        return this._service.call(request, 'Ams/Relates/WorkOrderSubmitTo');
    }
    public WorkOrderSupervisors(request: Requests.WorkOrderSupervisors): AbortablePromise<Responses.EmployeeRelates> {
        return this._service.call(request, 'Ams/Relates/WorkOrderSupervisors');
    }
}