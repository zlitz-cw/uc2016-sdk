import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { PreferencesServiceTypes as SvcDef } from '../../interfaces/Ams/PreferencesService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import IPreferencesService = SvcDef.IPreferencesService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/PreferencesService';

export class PreferencesService implements IPreferencesService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Global(request: Requests.Global): AbortablePromise<Responses.Global> {
        return this._service.call(request, 'Ams/Preferences/Global');
    }
    public User(request: Requests.User): AbortablePromise<Responses.User> {
        return this._service.call(request, 'Ams/Preferences/User');
    }
    public UserSave(request: Requests.UserSave): AbortablePromise<Responses.UserSave> {
        return this._service.call(request, 'Ams/Preferences/UserSave');
    }
}