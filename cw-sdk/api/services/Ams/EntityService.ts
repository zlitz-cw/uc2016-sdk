import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { EntityServiceTypes as SvcDef } from '../../interfaces/Ams/EntityService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import IEntityService = SvcDef.IEntityService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/EntityService';

export class EntityService implements IEntityService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public AddAlias(request: Requests.AddAlias): AbortablePromise<Responses.AssetAlias> {
        return this._service.call(request, 'Ams/Entity/AddAlias');
    }
    public AliasAssets(request: Requests.AliasAssets): AbortablePromise<Responses.AliasAssets> {
        return this._service.call(request, 'Ams/Entity/AliasAssets');
    }
    public Aliases(request: Requests.Aliases): AbortablePromise<Responses.AssetAlias> {
        return this._service.call(request, 'Ams/Entity/Aliases');
    }
    public AllAliases(request: Requests.AllAliases): AbortablePromise<Responses.AllAliases> {
        return this._service.call(request, 'Ams/Entity/AllAliases');
    }
    public Attributes(request: Requests.Attributes): AbortablePromise<Responses.Attributes> {
        return this._service.call(request, 'Ams/Entity/Attributes');
    }
    public Configuration(request: Requests.Configuration): AbortablePromise<Responses.Configuration> {
        return this._service.call(request, 'Ams/Entity/Configuration');
    }
    public CostHistory(request: Requests.CostHistory): AbortablePromise<Responses.CostHistory> {
        return this._service.call(request, 'Ams/Entity/CostHistory');
    }
    public CostTotal(request: Requests.CostTotal): AbortablePromise<Responses.CostTotal> {
        return this._service.call(request, 'Ams/Entity/CostTotal');
    }
    public EntityUidField(request: Requests.EntityUidField): AbortablePromise<Responses.EntityUidField> {
        return this._service.call(request, 'Ams/Entity/EntityUidField');
    }
    public Groups(request: Requests.Groups): AbortablePromise<Responses.Groups> {
        return this._service.call(request, 'Ams/Entity/Groups');
    }
    public RemoveAlias(request: Requests.RemoveAlias): AbortablePromise<Responses.AssetAlias> {
        return this._service.call(request, 'Ams/Entity/RemoveAlias');
    }
    public Types(request: Requests.Types): AbortablePromise<Responses.Types> {
        return this._service.call(request, 'Ams/Entity/Types');
    }
    public VisibleFields(request: Requests.VisibleFields): AbortablePromise<Responses.VisibleFields> {
        return this._service.call(request, 'Ams/Entity/VisibleFields');
    }
    public WorkActivityHistory(request: Requests.WorkActivityHistory): AbortablePromise<Responses.WorkActivityHistory> {
        return this._service.call(request, 'Ams/Entity/WorkActivityHistory');
    }
}