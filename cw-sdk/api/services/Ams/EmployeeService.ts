import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { EmployeeServiceTypes as SvcDef } from '../../interfaces/Ams/EmployeeService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import IEmployeeService = SvcDef.IEmployeeService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/EmployeeService';

export class EmployeeService implements IEmployeeService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public All(request: Requests.All): AbortablePromise<Responses.All> {
        return this._service.call(request, 'Ams/Employee/All');
    }
    public ById(request: Requests.ById): AbortablePromise<Responses.ById> {
        return this._service.call(request, 'Ams/Employee/ById');
    }
    public ByIds(request: Requests.ByIds): AbortablePromise<Responses.ByIds> {
        return this._service.call(request, 'Ams/Employee/ByIds');
    }
}