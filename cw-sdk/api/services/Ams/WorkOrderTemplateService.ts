import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { WorkOrderTemplateServiceTypes as SvcDef } from '../../interfaces/Ams/WorkOrderTemplateService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import IWorkOrderTemplateService = SvcDef.IWorkOrderTemplateService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/WorkOrderTemplateService';

export class WorkOrderTemplateService implements IWorkOrderTemplateService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public ByIds(request: Requests.ByIds): AbortablePromise<Responses.ByIds> {
        return this._service.call(request, 'Ams/WorkOrderTemplate/ByIds');
    }
    public CustomFields(request: Requests.CustomFields): AbortablePromise<Responses.CustomFields> {
        return this._service.call(request, 'Ams/WorkOrderTemplate/CustomFields');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'Ams/WorkOrderTemplate/Search');
    }
    public TemplateNames(request: Requests.TemplateNames): AbortablePromise<Responses.TemplateNames> {
        return this._service.call(request, 'Ams/WorkOrderTemplate/TemplateNames');
    }
}