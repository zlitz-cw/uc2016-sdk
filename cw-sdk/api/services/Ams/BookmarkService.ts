import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { BookmarkServiceTypes as SvcDef } from '../../interfaces/Ams/BookmarkService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import IBookmarkService = SvcDef.IBookmarkService;

export * from '../../../http/api-service';
export * from '../../interfaces/Ams/BookmarkService';

export class BookmarkService implements IBookmarkService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public AddBookmark(request: Requests.AddBookmark): AbortablePromise<Responses.AddBookmark> {
        return this._service.call(request, 'Ams/Bookmark/AddBookmark');
    }
    public AddScale(request: Requests.AddScale): AbortablePromise<Responses.AddScale> {
        return this._service.call(request, 'Ams/Bookmark/AddScale');
    }
    public DeleteBookmarks(request: Requests.DeleteBookmarks): AbortablePromise<Responses.DeleteBookmarksReponse> {
        return this._service.call(request, 'Ams/Bookmark/DeleteBookmarks');
    }
    public DeleteScales(request: Requests.DeleteScales): AbortablePromise<Responses.DeleteScales> {
        return this._service.call(request, 'Ams/Bookmark/DeleteScales');
    }
    public GetBookmarks(request: Requests.GetBookmarks): AbortablePromise<Responses.GetBookmarks> {
        return this._service.call(request, 'Ams/Bookmark/GetBookmarks');
    }
    public GetScales(request: Requests.GetScales): AbortablePromise<Responses.GetScales> {
        return this._service.call(request, 'Ams/Bookmark/GetScales');
    }
    public SetInitialExtent(request: Requests.SetInitialExtent): AbortablePromise<Responses.SetInitialExtent> {
        return this._service.call(request, 'Ams/Bookmark/SetInitialExtent');
    }
    public UpdateBookmark(request: Requests.UpdateBookmark): AbortablePromise<Responses.UpdateBookmark> {
        return this._service.call(request, 'Ams/Bookmark/UpdateBookmark');
    }
    public UpdateScale(request: Requests.UpdateScale): AbortablePromise<Responses.UpdateScale> {
        return this._service.call(request, 'Ams/Bookmark/UpdateScale');
    }
}