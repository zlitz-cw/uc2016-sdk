import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { CaseDataDetailServiceTypes as SvcDef } from '../../interfaces/PLL/CaseDataDetailService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ICaseDataDetailService = SvcDef.ICaseDataDetailService;

export * from '../../../http/api-service';
export * from '../../interfaces/PLL/CaseDataDetailService';

export class CaseDataDetailService implements ICaseDataDetailService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Add(request: Requests.Add): AbortablePromise<Responses.Add> {
        return this._service.call(request, 'PLL/CaseDataDetail/Add');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'PLL/CaseDataDetail/Search');
    }
    public SearchObject(request: Requests.SearchObject): AbortablePromise<Responses.SearchObject> {
        return this._service.call(request, 'PLL/CaseDataDetail/SearchObject');
    }
    public WIPAdd(request: Requests.WIPAdd): AbortablePromise<Responses.Add> {
        return this._service.call(request, 'PLL/CaseDataDetail/WIPAdd');
    }
}