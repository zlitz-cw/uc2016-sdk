import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { CaseCorrStatusServiceTypes as SvcDef } from '../../interfaces/PLL/CaseCorrStatusService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ICaseCorrStatusService = SvcDef.ICaseCorrStatusService;

export * from '../../../http/api-service';
export * from '../../interfaces/PLL/CaseCorrStatusService';

export class CaseCorrStatusService implements ICaseCorrStatusService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Add(request: Requests.Add): AbortablePromise<Responses.Add> {
        return this._service.call(request, 'PLL/CaseCorrStatus/Add');
    }
    public ByCaCorrectionsIds(request: Requests.ByCaCorrectionsIds): AbortablePromise<Responses.ByCaCorrectionsIds> {
        return this._service.call(request, 'PLL/CaseCorrStatus/ByCaCorrectionsIds');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'PLL/CaseCorrStatus/Search');
    }
}