import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { CaseRelDocsServiceTypes as SvcDef } from '../../interfaces/PLL/CaseRelDocsService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ICaseRelDocsService = SvcDef.ICaseRelDocsService;

export * from '../../../http/api-service';
export * from '../../interfaces/PLL/CaseRelDocsService';

export class CaseRelDocsService implements ICaseRelDocsService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Add(request: Requests.Add): AbortablePromise<Responses.Add> {
        return this._service.call(request, 'PLL/CaseRelDocs/Add');
    }
    public AddCaseAttachmentsToDataBase(request: Requests.AddCaseAttachmentsToDataBase): AbortablePromise<Responses.Add> {
        return this._service.call(request, 'PLL/CaseRelDocs/AddCaseAttachmentsToDataBase');
    }
    public AddCaseAttachmentToFileSystem(request: Requests.AddCaseAttachmentToFileSystem): AbortablePromise<Responses.Add> {
        return this._service.call(request, 'PLL/CaseRelDocs/AddCaseAttachmentToFileSystem');
    }
    public ByCaObjectId(request: Requests.ByCaObjectId): AbortablePromise<Responses.ByCaObjectId> {
        return this._service.call(request, 'PLL/CaseRelDocs/ByCaObjectId');
    }
    public Delete(request: Requests.Delete): AbortablePromise<Responses.Delete> {
        return this._service.call(request, 'PLL/CaseRelDocs/Delete');
    }
    public DeleteByCaObjectId(request: Requests.DeleteByCaObjectId): AbortablePromise<Responses.DeleteByCaObjectId> {
        return this._service.call(request, 'PLL/CaseRelDocs/DeleteByCaObjectId');
    }
    public GetStoragePreferences(request: Requests.GetStoragePreferences): AbortablePromise<Responses.StoragePreferences> {
        return this._service.call(request, 'PLL/CaseRelDocs/GetStoragePreferences');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'PLL/CaseRelDocs/Search');
    }
}