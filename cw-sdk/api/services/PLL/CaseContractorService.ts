import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { CaseContractorServiceTypes as SvcDef } from '../../interfaces/PLL/CaseContractorService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ICaseContractorService = SvcDef.ICaseContractorService;

export * from '../../../http/api-service';
export * from '../../interfaces/PLL/CaseContractorService';

export class CaseContractorService implements ICaseContractorService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Add(request: Requests.Add): AbortablePromise<Responses.Add> {
        return this._service.call(request, 'PLL/CaseContractor/Add');
    }
    public ByCaObjectId(request: Requests.ByCaObjectId): AbortablePromise<Responses.ByCaObjectId> {
        return this._service.call(request, 'PLL/CaseContractor/ByCaObjectId');
    }
    public DeleteByCaObjectId(request: Requests.DeleteByCaObjectId): AbortablePromise<Responses.DeleteByCaObjectId> {
        return this._service.call(request, 'PLL/CaseContractor/DeleteByCaObjectId');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'PLL/CaseContractor/Search');
    }
    public SearchObject(request: Requests.SearchObject): AbortablePromise<Responses.SearchObject> {
        return this._service.call(request, 'PLL/CaseContractor/SearchObject');
    }
}