import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { CaseInstReleasesServiceTypes as SvcDef } from '../../interfaces/PLL/CaseInstReleasesService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ICaseInstReleasesService = SvcDef.ICaseInstReleasesService;

export * from '../../../http/api-service';
export * from '../../interfaces/PLL/CaseInstReleasesService';

export class CaseInstReleasesService implements ICaseInstReleasesService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Add(request: Requests.Add): AbortablePromise<Responses.Add> {
        return this._service.call(request, 'PLL/CaseInstReleases/Add');
    }
    public Delete(request: Requests.Delete): AbortablePromise<Responses.Delete> {
        return this._service.call(request, 'PLL/CaseInstReleases/Delete');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'PLL/CaseInstReleases/Search');
    }
}