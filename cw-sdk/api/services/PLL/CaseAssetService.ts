import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { CaseAssetServiceTypes as SvcDef } from '../../interfaces/PLL/CaseAssetService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ICaseAssetService = SvcDef.ICaseAssetService;

export * from '../../../http/api-service';
export * from '../../interfaces/PLL/CaseAssetService';

export class CaseAssetService implements ICaseAssetService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public ByCaObjectId(request: Requests.ByCaObjectId): AbortablePromise<Responses.ByCaObjectId> {
        return this._service.call(request, 'PLL/CaseAsset/ByCaObjectId');
    }
    public DeleteByCaObjectId(request: Requests.DeleteByCaObjectId): AbortablePromise<Responses.DeleteByCaObjectId> {
        return this._service.call(request, 'PLL/CaseAsset/DeleteByCaObjectId');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'PLL/CaseAsset/Search');
    }
    public SearchObject(request: Requests.SearchObject): AbortablePromise<Responses.SearchObject> {
        return this._service.call(request, 'PLL/CaseAsset/SearchObject');
    }
}