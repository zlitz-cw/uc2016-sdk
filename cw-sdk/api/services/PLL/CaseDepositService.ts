import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { CaseDepositServiceTypes as SvcDef } from '../../interfaces/PLL/CaseDepositService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ICaseDepositService = SvcDef.ICaseDepositService;

export * from '../../../http/api-service';
export * from '../../interfaces/PLL/CaseDepositService';

export class CaseDepositService implements ICaseDepositService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Add(request: Requests.Add): AbortablePromise<Responses.Add> {
        return this._service.call(request, 'PLL/CaseDeposit/Add');
    }
    public AddDefault(request: Requests.AddDefault): AbortablePromise<Responses.AddDefault> {
        return this._service.call(request, 'PLL/CaseDeposit/AddDefault');
    }
    public ByCaObjectId(request: Requests.ByCaObjectId): AbortablePromise<Responses.ByCaObjectId> {
        return this._service.call(request, 'PLL/CaseDeposit/ByCaObjectId');
    }
    public Delete(request: Requests.Delete): AbortablePromise<Responses.Delete> {
        return this._service.call(request, 'PLL/CaseDeposit/Delete');
    }
    public DeleteByCaObjectId(request: Requests.DeleteByCaObjectId): AbortablePromise<Responses.DeleteByCaObjectId> {
        return this._service.call(request, 'PLL/CaseDeposit/DeleteByCaObjectId');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'PLL/CaseDeposit/Search');
    }
}