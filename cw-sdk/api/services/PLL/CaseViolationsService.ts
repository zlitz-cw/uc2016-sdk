import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { CaseViolationsServiceTypes as SvcDef } from '../../interfaces/PLL/CaseViolationsService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ICaseViolationsService = SvcDef.ICaseViolationsService;

export * from '../../../http/api-service';
export * from '../../interfaces/PLL/CaseViolationsService';

export class CaseViolationsService implements ICaseViolationsService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Add(request: Requests.Add): AbortablePromise<Responses.Add> {
        return this._service.call(request, 'PLL/CaseViolations/Add');
    }
    public ByCaObjectId(request: Requests.ByCaObjectId): AbortablePromise<Responses.ByCaObjectId> {
        return this._service.call(request, 'PLL/CaseViolations/ByCaObjectId');
    }
    public CompleteViolations(request: Requests.CompleteViolations): AbortablePromise<Responses.CompleteViolations> {
        return this._service.call(request, 'PLL/CaseViolations/CompleteViolations');
    }
    public Delete(request: Requests.Delete): AbortablePromise<Responses.Delete> {
        return this._service.call(request, 'PLL/CaseViolations/Delete');
    }
    public DeleteByCaObjectId(request: Requests.DeleteByCaObjectId): AbortablePromise<Responses.DeleteByCaObjectId> {
        return this._service.call(request, 'PLL/CaseViolations/DeleteByCaObjectId');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'PLL/CaseViolations/Search');
    }
}