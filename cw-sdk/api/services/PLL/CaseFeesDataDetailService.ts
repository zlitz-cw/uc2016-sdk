import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { CaseFeesDataDetailServiceTypes as SvcDef } from '../../interfaces/PLL/CaseFeesDataDetailService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ICaseFeesDataDetailService = SvcDef.ICaseFeesDataDetailService;

export * from '../../../http/api-service';
export * from '../../interfaces/PLL/CaseFeesDataDetailService';

export class CaseFeesDataDetailService implements ICaseFeesDataDetailService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Add(request: Requests.Add): AbortablePromise<Responses.Add> {
        return this._service.call(request, 'PLL/CaseFeesDataDetail/Add');
    }
    public Delete(request: Requests.Delete): AbortablePromise<Responses.Delete> {
        return this._service.call(request, 'PLL/CaseFeesDataDetail/Delete');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'PLL/CaseFeesDataDetail/Search');
    }
}