import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { CaseConditionServiceTypes as SvcDef } from '../../interfaces/PLL/CaseConditionService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ICaseConditionService = SvcDef.ICaseConditionService;

export * from '../../../http/api-service';
export * from '../../interfaces/PLL/CaseConditionService';

export class CaseConditionService implements ICaseConditionService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Add(request: Requests.Add): AbortablePromise<Responses.Add> {
        return this._service.call(request, 'PLL/CaseCondition/Add');
    }
    public ByCaObjectId(request: Requests.ByCaObjectId): AbortablePromise<Responses.ByCaObjectId> {
        return this._service.call(request, 'PLL/CaseCondition/ByCaObjectId');
    }
    public DeleteByCaObjectId(request: Requests.DeleteByCaObjectId): AbortablePromise<Responses.DeleteByCaObjectId> {
        return this._service.call(request, 'PLL/CaseCondition/DeleteByCaObjectId');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'PLL/CaseCondition/Search');
    }
    public SearchObject(request: Requests.SearchObject): AbortablePromise<Responses.SearchObject> {
        return this._service.call(request, 'PLL/CaseCondition/SearchObject');
    }
}