import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { CasePaymentRefundServiceTypes as SvcDef } from '../../interfaces/PLL/CasePaymentRefundService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ICasePaymentRefundService = SvcDef.ICasePaymentRefundService;

export * from '../../../http/api-service';
export * from '../../interfaces/PLL/CasePaymentRefundService';

export class CasePaymentRefundService implements ICasePaymentRefundService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Add(request: Requests.Add): AbortablePromise<Responses.Add> {
        return this._service.call(request, 'PLL/CasePaymentRefund/Add');
    }
    public Delete(request: Requests.Delete): AbortablePromise<Responses.Delete> {
        return this._service.call(request, 'PLL/CasePaymentRefund/Delete');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'PLL/CasePaymentRefund/Search');
    }
}