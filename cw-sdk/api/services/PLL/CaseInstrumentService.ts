import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { CaseInstrumentServiceTypes as SvcDef } from '../../interfaces/PLL/CaseInstrumentService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ICaseInstrumentService = SvcDef.ICaseInstrumentService;

export * from '../../../http/api-service';
export * from '../../interfaces/PLL/CaseInstrumentService';

export class CaseInstrumentService implements ICaseInstrumentService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Add(request: Requests.Add): AbortablePromise<Responses.Add> {
        return this._service.call(request, 'PLL/CaseInstrument/Add');
    }
    public ByCaObjectId(request: Requests.ByCaObjectId): AbortablePromise<Responses.ByCaObjectId> {
        return this._service.call(request, 'PLL/CaseInstrument/ByCaObjectId');
    }
    public Delete(request: Requests.Delete): AbortablePromise<Responses.Delete> {
        return this._service.call(request, 'PLL/CaseInstrument/Delete');
    }
    public DeleteByCaObjectId(request: Requests.DeleteByCaObjectId): AbortablePromise<Responses.DeleteByCaObjectId> {
        return this._service.call(request, 'PLL/CaseInstrument/DeleteByCaObjectId');
    }
    public GetList(request: Requests.GetList): AbortablePromise<Responses.GetList> {
        return this._service.call(request, 'PLL/CaseInstrument/GetList');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'PLL/CaseInstrument/Search');
    }
}