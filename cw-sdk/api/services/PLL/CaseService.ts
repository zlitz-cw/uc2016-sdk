import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { CaseServiceTypes as SvcDef } from '../../interfaces/PLL/CaseService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ICaseService = SvcDef.ICaseService;

export * from '../../../http/api-service';
export * from '../../interfaces/PLL/CaseService';

export class CaseService implements ICaseService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Create(request: Requests.Create): AbortablePromise<Responses.Create> {
        return this._service.call(request, 'PLL/Case/Create');
    }
}