import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { DepositServiceTypes as SvcDef } from '../../interfaces/PLL/DepositService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import IDepositService = SvcDef.IDepositService;

export * from '../../../http/api-service';
export * from '../../interfaces/PLL/DepositService';

export class DepositService implements IDepositService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public All(request: Requests.All): AbortablePromise<Responses.All> {
        return this._service.call(request, 'PLL/Deposit/All');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'PLL/Deposit/Search');
    }
}