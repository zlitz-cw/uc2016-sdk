import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { CaseCorrectionsServiceTypes as SvcDef } from '../../interfaces/PLL/CaseCorrectionsService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ICaseCorrectionsService = SvcDef.ICaseCorrectionsService;

export * from '../../../http/api-service';
export * from '../../interfaces/PLL/CaseCorrectionsService';

export class CaseCorrectionsService implements ICaseCorrectionsService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Add(request: Requests.Add): AbortablePromise<Responses.Add> {
        return this._service.call(request, 'PLL/CaseCorrections/Add');
    }
    public ByCaTaskIds(request: Requests.ByCaTaskIds): AbortablePromise<Responses.ByCaTaskIds> {
        return this._service.call(request, 'PLL/CaseCorrections/ByCaTaskIds');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'PLL/CaseCorrections/Search');
    }
    public Update(request: Requests.Update): AbortablePromise<Responses.Update> {
        return this._service.call(request, 'PLL/CaseCorrections/Update');
    }
}