import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { CaseDataListValuesServiceTypes as SvcDef } from '../../interfaces/PLL/CaseDataListValuesService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ICaseDataListValuesService = SvcDef.ICaseDataListValuesService;

export * from '../../../http/api-service';
export * from '../../interfaces/PLL/CaseDataListValuesService';

export class CaseDataListValuesService implements ICaseDataListValuesService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Add(request: Requests.Add): AbortablePromise<Responses.Add> {
        return this._service.call(request, 'PLL/CaseDataListValues/Add');
    }
    public Delete(request: Requests.Delete): AbortablePromise<Responses.Delete> {
        return this._service.call(request, 'PLL/CaseDataListValues/Delete');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'PLL/CaseDataListValues/Search');
    }
    public SearchObject(request: Requests.SearchObject): AbortablePromise<Responses.SearchObject> {
        return this._service.call(request, 'PLL/CaseDataListValues/SearchObject');
    }
}