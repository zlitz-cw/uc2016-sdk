import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { CaseFeesDataGroupServiceTypes as SvcDef } from '../../interfaces/PLL/CaseFeesDataGroupService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ICaseFeesDataGroupService = SvcDef.ICaseFeesDataGroupService;

export * from '../../../http/api-service';
export * from '../../interfaces/PLL/CaseFeesDataGroupService';

export class CaseFeesDataGroupService implements ICaseFeesDataGroupService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Add(request: Requests.Add): AbortablePromise<Responses.Add> {
        return this._service.call(request, 'PLL/CaseFeesDataGroup/Add');
    }
    public Delete(request: Requests.Delete): AbortablePromise<Responses.Delete> {
        return this._service.call(request, 'PLL/CaseFeesDataGroup/Delete');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'PLL/CaseFeesDataGroup/Search');
    }
}