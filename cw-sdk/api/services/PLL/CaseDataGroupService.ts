import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { CaseDataGroupServiceTypes as SvcDef } from '../../interfaces/PLL/CaseDataGroupService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ICaseDataGroupService = SvcDef.ICaseDataGroupService;

export * from '../../../http/api-service';
export * from '../../interfaces/PLL/CaseDataGroupService';

export class CaseDataGroupService implements ICaseDataGroupService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Add(request: Requests.Add): AbortablePromise<Responses.Add> {
        return this._service.call(request, 'PLL/CaseDataGroup/Add');
    }
    public AddDefault(request: Requests.AddDefault): AbortablePromise<Responses.AddDefault> {
        return this._service.call(request, 'PLL/CaseDataGroup/AddDefault');
    }
    public ByCaObjectId(request: Requests.ByCaObjectId): AbortablePromise<Responses.ByCaObjectId> {
        return this._service.call(request, 'PLL/CaseDataGroup/ByCaObjectId');
    }
    public DeleteByCaObjectId(request: Requests.DeleteByCaObjectId): AbortablePromise<Responses.DeleteByCaObjectId> {
        return this._service.call(request, 'PLL/CaseDataGroup/DeleteByCaObjectId');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'PLL/CaseDataGroup/Search');
    }
}