import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { CasePaymentServiceTypes as SvcDef } from '../../interfaces/PLL/CasePaymentService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ICasePaymentService = SvcDef.ICasePaymentService;

export * from '../../../http/api-service';
export * from '../../interfaces/PLL/CasePaymentService';

export class CasePaymentService implements ICasePaymentService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Add(request: Requests.Add): AbortablePromise<Responses.Add> {
        return this._service.call(request, 'PLL/CasePayment/Add');
    }
    public ByCaObjectId(request: Requests.ByCaObjectId): AbortablePromise<Responses.ByCaObjectId> {
        return this._service.call(request, 'PLL/CasePayment/ByCaObjectId');
    }
    public Delete(request: Requests.Delete): AbortablePromise<Responses.Delete> {
        return this._service.call(request, 'PLL/CasePayment/Delete');
    }
    public DeleteByCaObjectId(request: Requests.DeleteByCaObjectId): AbortablePromise<Responses.DeleteByCaObjectId> {
        return this._service.call(request, 'PLL/CasePayment/DeleteByCaObjectId');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'PLL/CasePayment/Search');
    }
}