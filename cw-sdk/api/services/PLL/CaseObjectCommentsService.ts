import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { CaseObjectCommentsServiceTypes as SvcDef } from '../../interfaces/PLL/CaseObjectCommentsService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ICaseObjectCommentsService = SvcDef.ICaseObjectCommentsService;

export * from '../../../http/api-service';
export * from '../../interfaces/PLL/CaseObjectCommentsService';

export class CaseObjectCommentsService implements ICaseObjectCommentsService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Add(request: Requests.Add): AbortablePromise<Responses.Add> {
        return this._service.call(request, 'PLL/CaseObjectComments/Add');
    }
    public ByCaObjectId(request: Requests.ByCaObjectId): AbortablePromise<Responses.ByCaObjectId> {
        return this._service.call(request, 'PLL/CaseObjectComments/ByCaObjectId');
    }
    public Delete(request: Requests.Delete): AbortablePromise<Responses.Delete> {
        return this._service.call(request, 'PLL/CaseObjectComments/Delete');
    }
    public Update(request: Requests.Update): AbortablePromise<Responses.Update> {
        return this._service.call(request, 'PLL/CaseObjectComments/Update');
    }
}