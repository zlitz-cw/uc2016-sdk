import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { CaseTaskResultsServiceTypes as SvcDef } from '../../interfaces/PLL/CaseTaskResultsService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ICaseTaskResultsService = SvcDef.ICaseTaskResultsService;

export * from '../../../http/api-service';
export * from '../../interfaces/PLL/CaseTaskResultsService';

export class CaseTaskResultsService implements ICaseTaskResultsService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Add(request: Requests.Add): AbortablePromise<Responses.Add> {
        return this._service.call(request, 'PLL/CaseTaskResults/Add');
    }
    public ByCaTaskIds(request: Requests.ByCaTaskIds): AbortablePromise<Responses.ByCaTaskIds> {
        return this._service.call(request, 'PLL/CaseTaskResults/ByCaTaskIds');
    }
    public SearchObject(request: Requests.SearchObject): AbortablePromise<Responses.SearchObject> {
        return this._service.call(request, 'PLL/CaseTaskResults/SearchObject');
    }
}