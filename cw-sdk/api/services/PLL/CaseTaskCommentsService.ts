import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { CaseTaskCommentsServiceTypes as SvcDef } from '../../interfaces/PLL/CaseTaskCommentsService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ICaseTaskCommentsService = SvcDef.ICaseTaskCommentsService;

export * from '../../../http/api-service';
export * from '../../interfaces/PLL/CaseTaskCommentsService';

export class CaseTaskCommentsService implements ICaseTaskCommentsService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Add(request: Requests.Add): AbortablePromise<Responses.Add> {
        return this._service.call(request, 'PLL/CaseTaskComments/Add');
    }
    public ByCaTaskId(request: Requests.ByCaTaskId): AbortablePromise<Responses.ByCaTaskId> {
        return this._service.call(request, 'PLL/CaseTaskComments/ByCaTaskId');
    }
    public ByCaTaskIds(request: Requests.ByCaTaskIds): AbortablePromise<Responses.ByCaTaskIds> {
        return this._service.call(request, 'PLL/CaseTaskComments/ByCaTaskIds');
    }
    public Delete(request: Requests.Delete): AbortablePromise<Responses.Delete> {
        return this._service.call(request, 'PLL/CaseTaskComments/Delete');
    }
    public Update(request: Requests.Update): AbortablePromise<Responses.Update> {
        return this._service.call(request, 'PLL/CaseTaskComments/Update');
    }
}