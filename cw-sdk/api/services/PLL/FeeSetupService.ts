import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { FeeSetupServiceTypes as SvcDef } from '../../interfaces/PLL/FeeSetupService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import IFeeSetupService = SvcDef.IFeeSetupService;

export * from '../../../http/api-service';
export * from '../../interfaces/PLL/FeeSetupService';

export class FeeSetupService implements IFeeSetupService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public All(request: Requests.All): AbortablePromise<Responses.All> {
        return this._service.call(request, 'PLL/FeeSetup/All');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'PLL/FeeSetup/Search');
    }
}