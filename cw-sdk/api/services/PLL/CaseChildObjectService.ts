import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { CaseChildObjectServiceTypes as SvcDef } from '../../interfaces/PLL/CaseChildObjectService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ICaseChildObjectService = SvcDef.ICaseChildObjectService;

export * from '../../../http/api-service';
export * from '../../interfaces/PLL/CaseChildObjectService';

export class CaseChildObjectService implements ICaseChildObjectService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public ByCaObjectId(request: Requests.ByCaObjectId): AbortablePromise<Responses.ByCaObjectId> {
        return this._service.call(request, 'PLL/CaseChildObject/ByCaObjectId');
    }
    public DeleteByCaObjectId(request: Requests.DeleteByCaObjectId): AbortablePromise<Responses.DeleteByCaObjectId> {
        return this._service.call(request, 'PLL/CaseChildObject/DeleteByCaObjectId');
    }
    public GetList(request: Requests.GetList): AbortablePromise<Responses.GetList> {
        return this._service.call(request, 'PLL/CaseChildObject/GetList');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'PLL/CaseChildObject/Search');
    }
    public SearchObject(request: Requests.SearchObject): AbortablePromise<Responses.SearchObject> {
        return this._service.call(request, 'PLL/CaseChildObject/SearchObject');
    }
}