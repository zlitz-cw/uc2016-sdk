import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { CaseObjectServiceTypes as SvcDef } from '../../interfaces/PLL/CaseObjectService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import ICaseObjectService = SvcDef.ICaseObjectService;

export * from '../../../http/api-service';
export * from '../../interfaces/PLL/CaseObjectService';

export class CaseObjectService implements ICaseObjectService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public ByIds(request: Requests.ByIds): AbortablePromise<Responses.ByIds> {
        return this._service.call(request, 'PLL/CaseObject/ByIds');
    }
    public CreateCaseFromServiceRequest(request: Requests.CreateCaseFromServiceRequest): AbortablePromise<Responses.CreateCase> {
        return this._service.call(request, 'PLL/CaseObject/CreateCaseFromServiceRequest');
    }
    public DeleteCase(request: Requests.DeleteCase): AbortablePromise<Responses.DeleteCase> {
        return this._service.call(request, 'PLL/CaseObject/DeleteCase');
    }
    public Move(request: Requests.Move): AbortablePromise<Responses.Move> {
        return this._service.call(request, 'PLL/CaseObject/Move');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'PLL/CaseObject/Search');
    }
}