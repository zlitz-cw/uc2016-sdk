import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { AuthenticationServiceTypes as SvcDef } from '../../interfaces/General/AuthenticationService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import IAuthenticationService = SvcDef.IAuthenticationService;

export * from '../../../http/api-service';
export * from '../../interfaces/General/AuthenticationService';

export class AuthenticationService implements IAuthenticationService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public Authenticate(request: Requests.Authenticate): AbortablePromise<Responses.Authenticate> {
        return this._service.call(request, 'General/Authentication/Authenticate');
    }
    public CityworksOnlineAuthenticate(request: Requests.CityworksOnlineAuthenticate): AbortablePromise<Responses.CityworksOnlineAuthenticate> {
        return this._service.call(request, 'General/Authentication/CityworksOnlineAuthenticate');
    }
    public CityworksOnlineSites(request: Requests.CityworksOnlineSites): AbortablePromise<Responses.CityworksOnlineSites> {
        return this._service.call(request, 'General/Authentication/CityworksOnlineSites');
    }
    public Domains(request: Requests.Domains): AbortablePromise<Responses.Domains> {
        return this._service.call(request, 'General/Authentication/Domains');
    }
    public User(request: Requests.User): AbortablePromise<Responses.User> {
        return this._service.call(request, 'General/Authentication/User');
    }
    public Validate(request: Requests.Validate): AbortablePromise<Responses.Validate> {
        return this._service.call(request, 'General/Authentication/Validate');
    }
    public Version(request: Requests.Version): AbortablePromise<Responses.Version> {
        return this._service.call(request, 'General/Authentication/Version');
    }
}