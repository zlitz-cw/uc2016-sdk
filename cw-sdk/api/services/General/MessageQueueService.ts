import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { MessageQueueServiceTypes as SvcDef } from '../../interfaces/General/MessageQueueService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import IMessageQueueService = SvcDef.IMessageQueueService;

export * from '../../../http/api-service';
export * from '../../interfaces/General/MessageQueueService';

export class MessageQueueService implements IMessageQueueService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public ByIds(request: Requests.ByIds): AbortablePromise<Responses.ByIds> {
        return this._service.call(request, 'General/MessageQueue/ByIds');
    }
    public Delete(request: Requests.Delete): AbortablePromise<Responses.Delete> {
        return this._service.call(request, 'General/MessageQueue/Delete');
    }
    public Preferences(request: Requests.Preferences): AbortablePromise<Responses.Preferences> {
        return this._service.call(request, 'General/MessageQueue/Preferences');
    }
    public Search(request: Requests.Search): AbortablePromise<Responses.Search> {
        return this._service.call(request, 'General/MessageQueue/Search');
    }
    public Update(request: Requests.Update): AbortablePromise<Responses.Update> {
        return this._service.call(request, 'General/MessageQueue/Update');
    }
    public UpdateMessageStatus(request: Requests.UpdateMessageStatus): AbortablePromise<Responses.UpdateMessageStatus> {
        return this._service.call(request, 'General/MessageQueue/UpdateMessageStatus');
    }
    public WebHooks(request: Requests.WebHooks): AbortablePromise<Responses.WebHooks> {
        return this._service.call(request, 'General/MessageQueue/WebHooks');
    }
}