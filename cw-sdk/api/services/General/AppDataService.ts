import { PromiseTypes } from '../../../core/promise';
import { IApiService } from '../../../http/api-service';
import { AppDataServiceTypes as SvcDef } from '../../interfaces/General/AppDataService';

import AbortablePromise = PromiseTypes.AbortablePromise;
import Requests = SvcDef.Requests;
import Responses = SvcDef.Responses;
import IAppDataService = SvcDef.IAppDataService;

export * from '../../../http/api-service';
export * from '../../interfaces/General/AppDataService';

export class AppDataService implements IAppDataService { 
    private _service: IApiService;
    public constructor(service: IApiService) { this._service = service }
    public CurrentLocation(request: Requests.CurrentLocation): AbortablePromise<Responses.CurrentLocation> {
        return this._service.call(request, 'General/AppData/CurrentLocation');
    }
    public SelectedEntities(request: Requests.SelectedEntities): AbortablePromise<Responses.SelectedEntities> {
        return this._service.call(request, 'General/AppData/SelectedEntities');
    }
}