﻿import * as Core from '../bundles/core';
import {AuthenticationService, AuthenticationServiceTypes} from '../api/services/general/AuthenticationService';

import AbortablePromise = Core.PromiseTypes.AbortablePromise
import ServiceEnums = Core.ServiceEnums;

import * as http from './http-service';


export interface IApiService {
    getToken(): string;
    login(userName: string, password: string): AbortablePromise<AuthenticationServiceTypes.Responses.Authenticate>;
    call(data: any, url: string): AbortablePromise<any>;
}

export class ApiService implements IApiService {

    private _token: string = null;
    private _baseUrl: string = null;

    constructor(baseUrl: string, token: string = null) {
        if (typeof token === 'undefined' || token === null || token.length < 1) {
            this._token = null;
        } else {
            this._token = token;
        }

        if (typeof baseUrl === 'undefined' || baseUrl === null || baseUrl.length < 1) {
            this._baseUrl = null;
        } else {
            this._baseUrl = cleanUrl(baseUrl);
        }
    }


    public getToken() {
        return this._token;
    }

    public login(userName: string, password: string): AbortablePromise<AuthenticationServiceTypes.Responses.Authenticate> {

        if (this._baseUrl === null) {
            return <any>Promise.reject("No Cityworks URL is set. URL must be set in the constructor.");
        }

        var request: AuthenticationServiceTypes.Requests.Authenticate;
        request = {
            LoginName: userName,
            Password: password
        };

        let svc = new AuthenticationService(this);

        var promise = svc.Authenticate(request);

        promise.then(
            (resp: AuthenticationServiceTypes.Responses.Authenticate) => {

                if (resp.Status === ServiceEnums.CoreResponseStatus.Ok) {
                    this._token = resp.Value.Token;
                } else {
                    this._token = null;
                }

                return resp;
            },
            (response) => {
                console.error("error logging in");
                console.error(response);
                this._token = null;
                return response;
            });
        return promise;
    }


    call(data: any, url: string): AbortablePromise<any> {

        if (this._baseUrl === null) {
            return <any>Promise.reject("No Cityworks URL is set. URL must be set in the constructor.");
        }

        return http.post(this._baseUrl + 'Services/' + url, this._token, data);
    }
}

function cleanUrl(u) {
    var viewUrl = u[u.length - 1] === '/' || u[u.length - 1] == '\\' ? '' : (u.indexOf('/') > -1 ? '/' : '\\');
    return u + viewUrl;
};



