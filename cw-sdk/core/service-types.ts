import { ServiceEnums } from './service-enums';
export namespace ServiceTypes {
    export interface CoreRequestBase {
    }
    export interface CoreRequestPaged extends ServiceTypes.CoreRequestBase {
        Limit?: number;
        Offset?: number;
        SortDir?: number;
        SortField?: string;
    }
    export interface CoreRequestVerified extends ServiceTypes.CoreRequestBase {
        WebServiceRequestId?: string;
    }
    export interface CoreResponseBase {
        ErrorMessages?: ServiceTypes.CoreResponseErrorMessage[];
        Message?: string;
        Status?: ServiceEnums.CoreResponseStatus;
        SuccessMessages?: ServiceTypes.CoreResponseSuccessMessage[];
        WarningMessages?: ServiceTypes.CoreResponseWarningMessage[];
    }
    export interface CoreResponseBase_<T> extends ServiceTypes.CoreResponseBase {
        Value?: T;
    }
    export interface CoreResponseBase_obsolete {
        Message?: string;
        Status?: ServiceEnums.CoreResponseStatus;
    }
    export interface CoreResponseBase_obsolete_<T> extends ServiceTypes.CoreResponseBase_obsolete {
        Value?: T;
    }
    export interface CoreResponseErrorMessage extends ServiceTypes.CoreResponseMessage {
    }
    export interface CoreResponseMessage {
        Code?: number;
        DebugDetails?: string;
        DisplayText?: string;
        InnerMessage?: ServiceTypes.CoreResponseMessage;
        MessageType?: ServiceEnums.CoreResponseMessageType;
        Name?: string;
        Service?: string;
    }
    export interface CoreResponsePaged_<T> extends ServiceTypes.CoreResponseBase_<T> {
        Limit?: number;
        Offset?: number;
        Total?: number;
    }
    export interface CoreResponsePaged_obsolete_<T> extends ServiceTypes.CoreResponseBase_obsolete_<T> {
        Limit?: number;
        Offset?: number;
        Total?: number;
    }
    export interface CoreResponseSuccessMessage extends ServiceTypes.CoreResponseMessage {
    }
    export interface CoreResponseVerified extends ServiceTypes.CoreResponseBase {
        WebServiceRequestId?: string;
    }
    export interface CoreResponseVerified_<T> extends ServiceTypes.CoreResponseBase_<T> {
        WebServiceRequestId?: string;
    }
    export interface CoreResponseVerified_obsolete extends ServiceTypes.CoreResponseBase_obsolete {
        WebServiceRequestId?: string;
    }
    export interface CoreResponseVerified_obsolete_<T> extends ServiceTypes.CoreResponseBase_obsolete_<T> {
        WebServiceRequestId?: string;
    }
    export interface CoreResponseWarningMessage extends ServiceTypes.CoreResponseMessage {
    }
}