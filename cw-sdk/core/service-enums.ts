export namespace ServiceEnums {
    export enum CoreResponseMessageType {
        Success = 0,
        Warning = 1,
        Error = 2
    }
    export enum CoreResponseStatus {
        Ok = 0,
        Error = 1,
        Unauthorized = 2,
        InvalidCredentials = 3,
        ConnectionError = -1
    }
}