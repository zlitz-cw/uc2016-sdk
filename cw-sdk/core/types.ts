import { Enums } from './enums';
export namespace Types {
    export interface AliasAsset {
        Alias?: string;
        Assets?: string[];
    }
    export interface AssemblyMaterial {
        ChildMatSid?: number;
        ParentMatSid?: number;
        Quantity?: number;
    }
    export interface AssemblyMaterialDetail extends Types.AssemblyMaterial {
        ChildMatDesc?: string;
        ChildMatUid?: string;
        ChildStock?: number;
        Description?: string;
        MaterialUid?: string;
        Stock?: number;
        Storeroom?: string;
    }
    export interface AssetAlias {
        AliasNames?: string[];
        AssetName?: string;
    }
    export interface AttachmentBase {
        AttachedBy?: string;
        AttachedBySid?: number;
        Attachment?: string;
        Comments?: string;
        DateTimeAttached?: Date;
    }
    export interface AuthToken {
        Created?: Date;
        Expires?: Date;
        LastUsed?: Date;
        LoginName?: string;
        Token?: string;
        TokenId?: number;
    }
    export interface CaAddressItemBase {
        Address?: string;
        AddressId?: number;
        AssetId?: string;
        AssetObjectId?: number;
        AssetType?: string;
        CaAddressId?: number;
        CaObjectId?: number;
        CityName?: string;
        CreatedBy?: number;
        CrossStreet?: string;
        DateCreated?: Date;
        DateExpired?: Date;
        DateModified?: Date;
        ExpiredFlag?: string;
        FeatureAssetId?: string;
        FeatureClass?: string;
        FeatureObjectId?: number;
        LegacyId?: string;
        LndObjectId?: number;
        Location?: string;
        MasterFlag?: string;
        ModifiedBy?: number;
        ObjectId?: string;
        StateCode?: string;
        StreetDirection?: string;
        StreetFraction?: string;
        StreetName?: string;
        StreetNumber?: number;
        StreetPostDir?: string;
        StreetType?: string;
        Suite?: string;
        TileNo?: string;
        XCoord?: number;
        YCoord?: number;
        ZipCode?: string;
    }
    export interface CaAssetItemBase {
        Address?: string;
        AssetId?: string;
        AssetObjectId?: number;
        AssetType?: string;
        CaAssetId?: number;
        CaObjectId?: number;
        CreatedBy?: number;
        DateCreated?: Date;
        DateModified?: Date;
        FeatureAssetId?: string;
        FeatureClass?: string;
        FeatureObjectId?: number;
        LegacyId?: string;
        Location?: string;
        ModifiedBy?: number;
        TileNo?: string;
        Xcoord?: string;
        Ycoord?: string;
    }
    export interface CaChildObjectItemBase {
        CaChildObjectId?: number;
        CaObjectId?: number;
        ChildId?: number;
        CreatedBy?: number;
        DateCreated?: Date;
        DateModified?: Date;
        ModifiedBy?: number;
    }
    export interface CaConditionItemBase {
        AppliedBy?: number;
        CaConditionId?: number;
        CaObjectId?: number;
        CompletedBy?: number;
        ConditionId?: number;
        CreatedBy?: number;
        DateApplied?: Date;
        DateCompleted?: Date;
        DateCreated?: Date;
        DateModified?: Date;
        DefaultText?: string;
        DisciplineId?: number;
        ModifiedBy?: number;
        Notes?: string;
        PrePostExistFlag?: string;
        TaskId?: number;
    }
    export interface CaContractorItemBase {
        AddressLine1?: string;
        AddressLine2?: string;
        AddressLine3?: string;
        BusinessName?: string;
        CaContractorId?: number;
        CaObjectId?: number;
        CityName?: string;
        CommentText?: string;
        ContractorDesc?: string;
        ContractorId?: number;
        ContractorType?: string;
        ContractorTypeId?: number;
        CreatedBy?: number;
        CreatedByLoginId?: string;
        DateCreated?: Date;
        DateModified?: Date;
        Email?: string;
        FaxNumber?: string;
        FirstName?: string;
        GenLiability?: string;
        GenLiabilityExpDate?: Date;
        LastName?: string;
        LicenseExpirationDate?: Date;
        LicenseNum?: string;
        LocalLicenseId?: number;
        ModifiedBy?: number;
        ModifiedByLoginId?: string;
        PhoneHome?: string;
        PhoneMobile?: string;
        PhoneWork?: string;
        StateCode?: string;
        StateLicenseId?: number;
        WcLiabilityCompany?: string;
        WCLiabilityExpDate?: Date;
        ZipCode?: string;
    }
    export interface CaCorrectionsItemBase {
        CaCorrectionsId?: number;
        CaObjectId?: number;
        CaTaskId?: number;
        CommentText?: string;
        CompletedBy?: number;
        CorrId?: number;
        CorrStatusGrpId?: number;
        CorrStatusId?: number;
        CreatedBy?: number;
        DateCompleted?: Date;
        DateCreated?: Date;
        DateModified?: Date;
        ModifiedBy?: number;
        RecheckCaCorrId?: number;
        RecheckFlag?: string;
        RecheckHistoryFlag?: string;
    }
    export interface CaCorrStatusItemBase {
        CaCorrectionsId?: number;
        CaCorrStatusId?: number;
        CompletedBy?: number;
        CorrStatusCode?: string;
        CorrStatusDesc?: string;
        CorrStatusId?: number;
        CreatedBy?: number;
        DateCompleted?: Date;
        DateCreated?: Date;
        DateModified?: Date;
        ModifiedBy?: number;
    }
    export interface CaDataDetailItemBase {
        CaDataDetailId?: number;
        CaDataGroupId?: number;
        CalcRateFlag?: string;
        CaseDataDetailId?: number;
        ColumnSequence?: string;
        CommentFlag?: string;
        CommentValue?: string;
        CreatedBy?: number;
        DateCreated?: Date;
        DateFlag?: string;
        DateModified?: Date;
        DateValue?: Date;
        DetailCode?: string;
        DetailDesc?: string;
        DetailSequence?: number;
        ListValue?: string;
        ListValuesFlag?: string;
        ModifiedBy?: number;
        NumberFlag?: string;
        NumberValue?: number;
        Q1Q2Q3Flag?: string;
        Q2Value?: number;
        Q3Value?: number;
        Quantity?: number;
        Rate?: number;
        TextFlag?: string;
        TextValue?: string;
        Value?: number;
        ValueFlag?: string;
        YesNoFlag?: string;
        YesNoValue?: string;
    }
    export interface CaDataGroupItemBase {
        CaDataGroupId?: number;
        CaObjectId?: number;
        CaseDataGroupId?: number;
        CreatedBy?: number;
        DateCreated?: Date;
        DateModified?: Date;
        GroupCode?: string;
        GroupDesc?: string;
        GroupSum?: number;
        ModifiedBy?: number;
        SumFlag?: string;
    }
    export interface CaDataListValuesItemBase {
        CaDataDetailId?: number;
        CaDataListId?: number;
        CreatedBy?: number;
        DateCreated?: Date;
        DateModified?: Date;
        ListValue?: string;
        ModifiedBy?: number;
    }
    export interface CaDepositItemBase {
        Amount?: number;
        CaDepositId?: number;
        CaObjectId?: number;
        CommentText?: string;
        CreatedBy?: number;
        DateCreated?: Date;
        DateModified?: Date;
        DepositId?: number;
        ModifiedBy?: number;
    }
    export interface CaFeesDataDetailItemBase {
        CaDataDetailId?: number;
        CaFeeId?: number;
        CaFeesDataDetailId?: number;
        CaObjectId?: number;
        CreatedBy?: number;
        DateCreated?: Date;
        DateModified?: Date;
        ModifiedBy?: number;
        Sequence?: number;
    }
    export interface CaFeesDataGroupItemBase {
        CaDataGroupId?: number;
        CaFeeId?: number;
        CaFeesDataGroupId?: number;
        CaObjectId?: number;
        CreatedBy?: number;
        DateCreated?: Date;
        DateModified?: Date;
        ModifiedBy?: number;
        Sequence?: number;
    }
    export interface CaFeesItemBase {
        Amount?: number;
        AutoRecalculate?: string;
        CaDataDetailId?: number;
        CaDataGroupId?: number;
        CaFeeId?: number;
        CaObjectId?: number;
        CommentText?: string;
        CreatedBy?: number;
        CustFeeSeq?: number;
        DateCreated?: Date;
        DateModified?: Date;
        Factor?: number;
        FeeCode?: string;
        FeeDesc?: string;
        FeeSetupId?: number;
        FeeTypeId?: number;
        Invoiced?: string;
        ModifiedBy?: number;
        PaymentAmount?: number;
        Quantity?: number;
        Rate?: number;
        WaiveFee?: string;
    }
    export interface CaFlagsItemBase {
        AppliedBy?: number;
        CaFlagId?: number;
        CaObjectId?: number;
        CompletedBy?: number;
        CreatedBy?: number;
        DateApplied?: Date;
        DateCompleted?: Date;
        DateCreated?: Date;
        DateModified?: Date;
        DisciplineId?: number;
        FlagId?: number;
        ModifiedBy?: number;
        Notes?: string;
        Severity?: string;
    }
    export interface CaInspectionRequestItemBase {
        CaObjectId?: number;
        ConfirmationId?: number;
        CreatedBy?: number;
        DateCreated?: Date;
        DateModified?: Date;
        LocationSpecific?: string;
        ModifiedBy?: number;
        RequestId?: number;
        RequestorComment?: string;
        RequestorName?: string;
        RequestorPhone?: string;
        RequestorPhoneExt?: string;
        RequestSource?: string;
    }
    export interface CaInstReleasesItemBase {
        AmountReleased?: number;
        CaInstReleasesId?: number;
        CaInstrumentId?: number;
        CommentText?: string;
        CreatedBy?: number;
        DateCreated?: Date;
        DateModified?: Date;
        DateReleased?: Date;
        ModifiedBy?: number;
        PercentReleased?: number;
        ReleasedBy?: number;
    }
    export interface CaInstrumentItem extends Types.CaInstrumentItemBase {
        CaseName?: string;
        CaseNumber?: string;
        CaseStatus?: string;
        CaseType?: string;
        CaseTypeDesc?: string;
        CaseTypeId?: number;
        CountryName?: string;
        CreatedByLoginId?: string;
        InstDesc?: string;
        InstType?: string;
        IsSingleEntry?: boolean;
        ModifiedByLoginId?: string;
        ProjectCode?: string;
        ProjectDesc?: string;
        ProjectId?: number;
        StateName?: string;
        SubType?: string;
        SubTypeDesc?: string;
        SubTypeId?: number;
        TableName?: string;
    }
    export interface CaInstrumentItemBase {
        AddressLine1?: string;
        AddressLine2?: string;
        Amount?: number;
        CaInstrumentId?: number;
        CaObjectId?: number;
        CityName?: string;
        CommentText?: string;
        Company?: string;
        ContactEmail?: string;
        ContactName?: string;
        ContactPhone?: string;
        CountryCode?: string;
        CreatedBy?: number;
        DateCreated?: Date;
        DateExpire?: Date;
        DateModified?: Date;
        EffectiveDate?: Date;
        InstTypeId?: number;
        IssueDate?: Date;
        ModifiedBy?: number;
        SerialNumber?: string;
        StateCode?: string;
        ZipCode?: string;
    }
    export interface CaLicenseItemBase {
        CaFeeId?: number;
        CaLicenseId?: number;
        CaObjectId?: number;
        Code?: string;
        Comments?: string;
        CreatedBy?: number;
        DateApproved?: Date;
        DateCreated?: Date;
        DateModified?: Date;
        Description?: string;
        FeeCode?: string;
        FeeSetupId?: number;
        LicenseNumber?: number;
        ModifiedBy?: number;
        StatusCode?: string;
    }
    export interface CaNotesItemBase {
        CaNotesId?: number;
        CaObjectId?: number;
        CommentId?: number;
        CreatedBy?: number;
        DateCreated?: Date;
        DateModified?: Date;
        ModifiedBy?: number;
        Notes?: string;
        printOnCase?: string;
    }
    export interface CaObjectCommentsItemBase {
        CaObjectCommentId?: number;
        CaObjectId?: number;
        CommentText?: string;
        CreatedBy?: number;
        DateCreated?: Date;
        DateModified?: Date;
        ModifiedBy?: number;
    }
    export interface CaObjectItem extends Types.CaObjectItemBase {
        AcceptedByLoginId?: string;
        AnonymousFlag?: string;
        CaseGroup?: string;
        CloneCaseRelation?: string;
        CreatedByLoginId?: string;
        EnteredByLoginid?: string;
        GetViewColumns?: string[];
        ModifiedByLoginId?: string;
        PendingFlag?: string;
        RegisteredFlag?: string;
        ServiceRequestDesc?: string;
        ServiceRequestId?: number;
        ServiceRequestStatus?: string;
        TableName?: string;
        TempTableName?: string;
        WorkOrderDesc?: string;
        WorkOrderId?: number;
        WorkOrderStatus?: string;
    }
    export interface CaObjectItemBase {
        AcceptedBy?: number;
        ActiveFlag?: string;
        BLicenseFlag?: string;
        BusinessCategory?: string;
        BusinessName?: string;
        BusinessOrgType?: string;
        CaObjectId?: number;
        CaseName?: string;
        CaseNumber?: string;
        CaseStatus?: string;
        CaseStatusId?: number;
        CaseType?: string;
        CaseTypeDesc?: string;
        CaseTypeId?: number;
        CreatedBy?: number;
        CX?: number;
        CY?: number;
        DateAccepted?: Date;
        DateCreated?: Date;
        DateEntered?: Date;
        DateExpiration?: Date;
        DateIssued?: Date;
        DateModified?: Date;
        EnteredBy?: number;
        ExpiredFlag?: string;
        FedTaxId?: string;
        IssuedBy?: number;
        IssuedFlag?: string;
        Location?: string;
        ModifiedBy?: number;
        OrgId?: number;
        PACaseFlag?: string;
        PriorityLevel?: string;
        ProjectCode?: string;
        ProjectDesc?: string;
        ProjectId?: number;
        StateTaxId?: string;
        StatusCode?: string;
        SubType?: string;
        SubTypeDefaultText?: string;
        SubTypeDesc?: string;
        SubTypeId?: number;
    }
    export interface CaPaymentItemBase {
        CaDepositId?: number;
        CaFeeId?: number;
        CaObjectId?: number;
        CaPaymentId?: number;
        CaReceiptId?: number;
        CommentText?: string;
        CreatedBy?: number;
        CustFeeSeq?: number;
        DateCreated?: Date;
        DateModified?: Date;
        DateReceived?: Date;
        DateVoided?: Date;
        DepositCode?: string;
        DepositId?: number;
        FeeAmount?: number;
        FeeCode?: string;
        ModifiedBy?: number;
        PaymentAccount?: string;
        PaymentAmount?: number;
        PaymentDate?: Date;
        ReceivedBy?: number;
        ReferenceInfo?: string;
        TenderType?: string;
        TenderTypeId?: number;
        VoidedBy?: number;
    }
    export interface CaPaymentRefundItemBase {
        CaFeeId?: number;
        CaPaymentId?: number;
        CaPaymentRefundId?: number;
        Comments?: string;
        CreatedBy?: number;
        DateCreated?: Date;
        RefundAmount?: number;
    }
    export interface CaPeopleItemBase {
        AddressLine1?: string;
        AddressLine2?: string;
        AddressLine3?: string;
        CaObjectId?: number;
        CaPeopleId?: number;
        CityName?: string;
        CommentText?: string;
        CompanyName?: string;
        CountryCode?: string;
        CreatedBy?: number;
        DateCreated?: Date;
        DateModified?: Date;
        Email?: string;
        FaxNumber?: string;
        ModifiedBy?: number;
        Name?: string;
        PeopleId?: number;
        PhoneHome?: string;
        PhoneMobile?: string;
        PhoneWork?: string;
        PhoneWorkExt?: string;
        RoleCode?: string;
        RoleDesc?: string;
        RoleId?: number;
        StateCode?: string;
        WebSiteUrl?: string;
        ZipCode?: string;
    }
    export interface CaRelDocsItemBase {
        CaObjectId?: number;
        CaRelDocId?: number;
        CommentText?: string;
        CreatedBy?: number;
        CreatedByLoginId?: string;
        DateCreated?: Date;
        DateModified?: Date;
        DocContentInString?: string;
        DocName?: string;
        DocumentContent?: number[];
        Location?: string;
        LocationType?: string;
        ModifiedBy?: number;
    }
    export interface CaTaskCommentsItemBase {
        CaObjectId?: number;
        CaTaskCommentId?: number;
        CaTaskId?: number;
        CommentId?: number;
        Commenttext?: string;
        CreatedBy?: number;
        DateCreated?: Date;
        DateModified?: Date;
        ModifiedBy?: number;
    }
    export interface CaTaskItemBase {
        ActualEndDate?: Date;
        ActualStartDate?: Date;
        AutoSchduleInspFlg?: string;
        CalWeekDayFlag?: string;
        CaObjectId?: number;
        CaTaskId?: number;
        CorrGroupId?: number;
        CreatedBy?: number;
        DateCreated?: Date;
        DateExpired?: Date;
        DateModified?: Date;
        DefDurationDays?: number;
        DisciplineId?: number;
        EndPoint?: number;
        ExpiredFlag?: string;
        GeoAreaId?: number;
        GeoDetailId?: number;
        HearingTypeId?: number;
        LeadDays?: number;
        ModifiedBy?: number;
        RescheduleAfterDays?: number;
        ResponsibleDeptId?: number;
        ResponsibleDivId?: number;
        ResponsibleUserId?: number;
        ResultCode?: string;
        ResultId?: number;
        ResultSetDesc?: string;
        ResultSetId?: number;
        StartPoint?: number;
        TargetEndDate?: Date;
        TargetStartDate?: Date;
        TaskAvailableDate?: Date;
        TaskAvailableFlag?: string;
        TaskCode?: string;
        TaskCompleteDate?: Date;
        TaskCompletedBy?: number;
        TaskCompleteFlag?: string;
        TaskDesc?: string;
        TaskGeoFlag?: string;
        TaskId?: number;
        TaskType?: string;
        TimeCut?: number;
        WorkUnit?: number;
    }
    export interface CaTaskResultsItem extends Types.CaTaskResultsItemBase {
        CaseStatus?: string;
        IsSingleEntry?: boolean;
        StatusCode?: string;
    }
    export interface CaTaskResultsItemBase {
        CaObjectId?: number;
        CaseStatusId?: number;
        CaTaskId?: number;
        CaTaskResultsId?: number;
        CloseTaskFlag?: string;
        ExtendExpirationDateFlag?: string;
        InsertFlag?: string;
        NextTaskFlag?: string;
        ReInsertFlag?: string;
        ResultCode?: string;
        ResultDesc?: string;
        ResultID?: number;
        ResultSetID?: number;
        SkipTaskFlag?: string;
    }
    export interface CategoryCustField {
        CategoryId?: number;
        CodeDescList?: Types.CodeDesc[];
        CodeType?: string;
        CustFieldId?: number;
        CustFieldName?: string;
        CustFieldType?: Enums.FieldDataType;
        CustFieldValue?: string;
        DefaultValue?: string;
        ForceSortByCode?: boolean;
        IsRequired?: boolean;
        IsVisible?: boolean;
        LinkFieldName?: string;
        MaxValue?: number;
        MinValue?: number;
        UseCodeDesc?: string;
        UseCodeForDisplay?: boolean;
    }
    export interface CaViolationsItemBase {
        CaFeeId?: number;
        CaObjectId?: number;
        CaTaskId?: number;
        CaViolationId?: number;
        CompletedBy?: number;
        CreatedBy?: number;
        DateCompleted?: Date;
        DateCreated?: Date;
        DateIssued?: Date;
        DateModified?: Date;
        Issuedby?: number;
        ModifiedBy?: number;
        RemedialText?: string;
        ViolationId?: number;
        ViolationText?: string;
    }
    export interface ChartWidget {
        Header?: string;
        Id?: number;
        Position?: number;
        Settings?: Types.ChartWidgetSettings;
        WidgetZoneId?: number;
    }
    export interface ChartWidgetSettings {
        Analysis?: string;
        BottomMargin?: number;
        ChartType?: string;
        Height?: number;
        Items?: Types.ChartWidgetSettingsItem[];
        LeftMargin?: number;
        TextRotation?: number;
    }
    export interface ChartWidgetSettingsItem {
        Kind?: string;
        SearchId?: number;
        XColumn?: string;
        YColumn?: string;
    }
    export interface CityworksOnlineAuthToken {
        Token?: string;
    }
    export interface CityworksOnlineSite {
        Description?: string;
        Url?: string;
    }
    export interface CodeDesc {
        Code?: string;
        CodeType?: string;
        Description?: string;
        IsActive?: boolean;
    }
    export interface CodeDescScore {
        Code?: string;
        Description?: string;
        Score?: number;
    }
    export interface CommentRecord {
        ActivityId?: string;
        ActivityType?: Enums.CommentActivityType;
        AuthorName?: string;
        AuthorSid?: number;
        CommentId?: number;
        Comments?: string;
        DateCreated?: Date;
        LastModified?: Date;
        LastModifiedByName?: string;
        LastModifiedBySid?: number;
    }
    export interface ConditionHistory {
        ActivityId?: string;
        DateGenerated?: Date;
        EntityType?: string;
        EntityUid?: string;
        Id?: number;
        Kind?: string;
        Score?: number;
    }
    export interface ContractorBase {
        Address?: string;
        AutomobileInsAmount?: number;
        AutomobileInsCertificate?: string;
        AutomobileInsEffectDate?: Date;
        AutomobileInsExpireDate?: Date;
        CellPhone?: string;
        City?: string;
        Comments?: string;
        ContactName?: string;
        ContractorName?: string;
        ContractorNumber?: string;
        ContractorSid?: number;
        Description?: string;
        Email?: string;
        EmergencyFactor?: number;
        Fax?: string;
        FederalTaxId?: string;
        FMSNo?: string;
        GeneralLiabilityAmount?: number;
        GeneralLiabilityCertificate?: string;
        GeneralLiabilityEffectDate?: Date;
        GeneralLiabilityExpireDate?: Date;
        LiabilityInsAmount?: number;
        LiabilityInsCertificate?: string;
        LiabilityInsEffectDate?: Date;
        LiabilityInsExpireDate?: Date;
        Licensed?: boolean;
        LicensedWork?: string;
        LicenseExpDate?: Date;
        LocallyBased?: boolean;
        MWBE?: boolean;
        OfficePhone?: string;
        OtherPhone?: string;
        OverheadRate?: number;
        OverheadType?: Enums.RateAddMethod;
        OvertimeFactor?: number;
        PIN?: string;
        ProviderType?: string;
        ProvidesEquipment?: boolean;
        ProvidesLabor?: boolean;
        ProvidesMaterial?: boolean;
        Rate?: number;
        RateType?: Enums.ContractorRateType;
        RegistrationDate?: Date;
        State?: string;
        Viewable?: boolean;
        WorkersCompAmount?: number;
        WorkersCompCertificate?: string;
        WorkersCompEffectDate?: Date;
        WorkersCompExpireDate?: Date;
        Zip?: string;
    }
    export interface ContractorKeywordBase {
        ContractorSid?: number;
        Keyword?: string;
    }
    export interface CoreDomainBase {
        Id?: number;
    }
    export interface CostCode {
        Code?: string;
        Description?: string;
        SequenceId?: number;
    }
    export interface CrewBase {
        Category?: string;
        CrewId?: number;
        CrewName?: string;
        DefaultStoreroom?: string;
        Description?: string;
        DomainId?: number;
        SharedWithin?: Enums.ApplyLevel;
    }
    export interface CustCallQuestionBase {
        Answer?: string;
        AnswerId?: number;
        CallQuestionId?: number;
        DispatchToField?: string;
        IncidentNum?: number;
        Question?: string;
        QuestionSequence?: number;
        SubmitToField?: string;
        SubmitToLayer?: string;
    }
    export interface CustFieldCategoryBase {
        ApplyToTable?: string;
        Category?: string;
        CategoryId?: number;
        Description?: string;
        IsActive?: boolean;
    }
    export interface CustomerCallBase {
        AcctNum?: string;
        AptNum?: string;
        CallerType?: string;
        CCX?: number;
        CCY?: number;
        CellPhone?: string;
        Comments?: string;
        CustAddress?: string;
        CustAddType?: string;
        CustCallback?: boolean;
        CustCity?: string;
        CustContact?: boolean;
        CustDistrict?: string;
        CustState?: string;
        CustZip?: string;
        DateTimeCall?: Date;
        DateTimeCallback?: Date;
        DateTimeContact?: Date;
        Email?: string;
        Employee?: string;
        EmployeeSid?: number;
        Fax?: string;
        FirstName?: string;
        HomePhone?: string;
        IncidentNum?: number;
        IsFollowUpCall?: boolean;
        IsResident?: boolean;
        LastName?: string;
        MiddleInitial?: string;
        OtherPhone?: string;
        ProbDetails?: string;
        RequestId?: number;
        Text1?: string;
        Text2?: string;
        Text3?: string;
        Text4?: string;
        Text5?: string;
        Title?: string;
        WorkPhone?: string;
    }
    export interface CWDomain {
        Description?: string;
        DomainId?: number;
        DomainName?: string;
        EditButtonsOn?: boolean;
        ImageUrl?: string;
        MapServiceId?: number;
        MobileMapCacheId?: number;
        WebTitle?: string;
    }
    export interface CWUser {
        ADDomain?: string;
        Districts?: string[];
        DomainId?: number;
        Domains?: Types.CWDomain[];
        EmployeeSid?: number;
        FullName?: string;
        GISRight?: Enums.GISRight;
        GroupId?: number;
        GroupIds?: string;
        Groups?: number[];
        IsAnonymousUser?: boolean;
        IsCwdba?: boolean;
        IsDomainSuperuser?: boolean;
        IsRegisteredUser?: boolean;
        IsStoreDomainSuperuser?: boolean;
        IsWindowsIdentity?: boolean;
        LoginId?: string;
        LoginName?: string;
        OrgId?: number;
        RoleId?: string;
        Roles?: string;
        StoreDomainId?: number;
        StoreDomainsWhereAdmin?: Types.StoreDomainBase[];
        TableName?: string;
        UniqueName?: string;
        UserId?: number;
        UserName?: string;
        UserPwd?: string;
        UserType?: string;
    }
    export interface DateFieldValue {
        DateEnd?: Date;
        DateIncrement?: number;
        DateSelectMode?: Enums.DateSelection;
        DateStart?: Date;
        DateUnit?: Enums.RelativeDateUnit;
        IncludeCurrent?: boolean;
        LastNext?: Enums.RelativeDate;
        NextDateIncrement?: number;
        NotInDateRange?: boolean;
    }
    export interface DepositItemBase {
        AccountCode?: string;
        CreatedBy?: number;
        DateCreated?: Date;
        DateExpired?: Date;
        DateModified?: Date;
        DepositCode?: string;
        DepositDesc?: string;
        DepositId?: number;
        DepositTypeId?: number;
        ExpiredFlag?: string;
        ModifiedBy?: number;
        OrgId?: number;
    }
    export interface DescScore {
        CodeType?: string;
        Description?: string;
        Score?: number;
    }
    export interface EmployeeBase {
        AdDomain?: string;
        BenefitRate?: number;
        BenefitType?: Enums.RateAddMethod;
        DefaultImgPath?: string;
        DomainId?: number;
        Email?: string;
        EmailReq?: string;
        EmployeeId?: string;
        EmployeeSid?: number;
        FirstName?: string;
        FullName?: string;
        HolidayRate?: number;
        HolidayType?: Enums.RateAddMethod;
        HourlyRate?: number;
        IsActive?: boolean;
        LastName?: string;
        LoginName?: string;
        MapServiceId?: number;
        MiddleInitial?: string;
        MobileMapCacheId?: number;
        Organization?: string;
        OtherRate?: number;
        OtherRateType?: Enums.RateAddMethod;
        OverheadRate?: number;
        OverheadType?: Enums.RateAddMethod;
        OvertimeRate?: number;
        OvertimeType?: Enums.RateAddMethod;
        Pager?: string;
        Password?: string;
        ShiftDiffRate?: number;
        ShiftDiffType?: Enums.RateAddMethod;
        StandbyRate?: number;
        StandbyType?: Enums.RateAddMethod;
        Title?: string;
        UniqueName?: string;
        WorkPhone?: string;
    }
    export interface EmployeeNameId {
        EmployeeName?: string;
        EmployeeSid?: number;
        IsActive?: boolean;
    }
    export interface EmployeeRelate {
        DomainId?: number;
        EmployeeName?: string;
        EmployeeSid?: number;
        RelateName?: string;
    }
    export interface EntityConfiguration {
        CondScoreDateField?: string;
        CondScoreField?: string;
        EntityType?: string;
        EntityUidField?: string;
        FieldAlias?: string;
        FieldBackColor?: string;
        FieldFontBold?: boolean;
        FieldForeColor?: string;
        FieldName?: string;
        IdField?: string;
        ImageNameField?: string;
        ImagePath?: string;
        IsFeatClass?: boolean;
        IsFilePath?: boolean;
        IsPrintable?: boolean;
        IsRequired?: boolean;
        IsVisible?: boolean;
        ReservedCWFieldName?: string;
        SequenceId?: number;
        TableName?: string;
        Tag?: Object;
    }
    export interface EntityCostTotal {
        EntityType?: string;
        EntityUid?: string;
        TotalCost?: number;
        TotalHours?: number;
    }
    export interface EntityHistory {
        ActualFinish?: Date;
        ActualStart?: Date;
        Closed?: Date;
        Created?: Date;
        Description?: string;
        EntityType?: string;
        EntityUid?: string;
        ExpenseType?: Enums.WOExpenseType;
        Id?: string;
        Inspected?: Date;
        Kind?: string;
        Status?: string;
    }
    export interface EntityReading {
        ActionType?: Enums.ReadingActionType;
        Comments?: string;
        EntityId?: string;
        EntityType?: string;
        Reading?: number;
        ReadingDate?: Date;
        ReadingId?: number;
        ReadingName?: string;
        WOReading?: number;
        WorkOrderId?: string;
    }
    export interface EntityReadingConfiguration {
        EntityId?: string;
        EntityType?: string;
        ReadingName?: string;
    }
    export interface EquipmentBase {
        DefaultImgPath?: string;
        Description?: string;
        EquipmentSid?: number;
        EquipmentUid?: string;
        ForCheckout?: string;
        Manufacturer?: string;
        Model?: string;
        RateType?: Enums.EquipmentRateType;
        UnitCost?: number;
        Viewable?: boolean;
        WarranteeDate?: Date;
    }
    export interface EquipmentCost {
        AcctNum?: string;
        Cost?: number;
        Description?: string;
        DomainId?: number;
        EntityType?: string;
        EntityUid?: string;
        EquipmentCostId?: number;
        EquipmentSid?: number;
        EquipmentUid?: string;
        FinishDate?: Date;
        HoursRequired?: number;
        RateType?: Enums.EquipmentRateType;
        ReserveTransId?: number;
        Source?: string;
        StartDate?: Date;
        TaskName?: string;
        TransDate?: Date;
        UnitsRequired?: number;
        UsageType?: Enums.CostUsage;
        WorkOrderId?: string;
        WOTaskId?: number;
    }
    export interface EsriAddUpdate {
        features?: Types.EsriRecord[];
    }
    export interface EsriApplyEdit {
        adds?: Types.EsriRecord[];
        deletes?: number[];
        id?: number;
        updates?: Types.EsriRecord[];
    }
    export interface EsriApplyEdits {
        features?: Types.EsriApplyEdit[];
    }
    export interface EsriApplyEditsResults {
        addResults?: Types.EsriUpdateResult[];
        deleteResults?: Types.EsriUpdateResult[];
        id?: number;
        updateResults?: Types.EsriUpdateResult[];
    }
    export interface EsriCodeValue {
        code?: Object;
        IsSelected?: boolean;
        name?: string;
    }
    export interface EsriDomain {
        codedValues?: Types.EsriCodeValue[];
        description?: string;
        IsEmpty?: boolean;
        name?: string;
        range?: number[];
        type?: string;
    }
    export interface EsriError {
        code?: string;
        description?: string;
        message?: string;
    }
    export interface EsriExtent {
        isEmpty?: boolean;
        spatialReference?: Types.EsriSpatialReference;
        xmax?: number;
        xmin?: number;
        ymax?: number;
        ymin?: number;
    }
    export interface EsriQueryParams {
        callback?: string;
        cityworksCurrentUserValue?: number;
        cityworksEntityType?: string;
        cityworksIsCanceled?: boolean;
        cityworksIsClosed?: boolean;
        cityworksTemplateId?: number;
        cityworksType?: number;
        definitionExpression?: string;
        domainId?: number;
        f?: string;
        gdbVersion?: string;
        geometry?: string;
        geometryPrecision?: number;
        geometryType?: string;
        groupByFieldsForStatistics?: string;
        inSR?: string;
        layerId?: number;
        layers?: string;
        mapExtent?: Types.EsriExtent;
        maxAllowableOffset?: number;
        objectIds?: string;
        orderByFields?: string;
        outFields?: string;
        outSR?: string;
        outStatistics?: string;
        pretty?: boolean;
        relationParam?: string;
        relationshipId?: number;
        resultOffset?: number;
        resultRecordCount?: number;
        returnCountOnly?: boolean;
        returnDistinctValues?: boolean;
        returnGeometry?: boolean;
        returnIdsOnly?: boolean;
        returnM?: boolean;
        returnTrueCurves?: boolean;
        returnUpdates?: boolean;
        returnZ?: boolean;
        searchId?: number;
        serviceName?: string;
        spatialRel?: string;
        sr?: number;
        text?: string;
        time?: number[];
        tolerance?: number;
        units?: string;
        where?: string;
    }
    export interface EsriQueryResult {
        count?: number;
        displayFieldName?: string;
        features?: Types.EsriRecord[];
        fieldAliases?: {[key: string]: string};
        fields?: Types.EsriServiceLayerAttribute[];
        geometryType?: string;
        globalIdFieldName?: string;
        objectIdFieldName?: string;
        spatialReference?: Types.EsriSpatialReference;
    }
    export interface EsriRecord {
        attributes?: {[key: string]: Object};
        geometry?: Types.IEsriGeometry;
    }
    export interface EsriRelatedRecordBase {
        fields?: Types.EsriServiceLayerAttribute[];
        geometryType?: string;
        hasM?: boolean;
        hasZ?: boolean;
        relatedRecordGroups?: Types.EsriRelatedRecordGroup[];
        spatialReference?: Types.EsriSpatialReference;
    }
    export interface EsriRelatedRecordGroup {
        objectId?: number;
        relatedRecords?: Types.EsriRecord[];
    }
    export interface EsriServiceLayerAttribute {
        alias?: string;
        domain?: Types.EsriDomain;
        editable?: boolean;
        length?: number;
        localizedNames?: {[key: string]: string};
        name?: string;
        nullable?: boolean;
        required?: boolean;
        Tag?: Object;
        type?: string;
        visible?: boolean;
    }
    export interface EsriSpatialReference {
        latestVcsWkid?: number;
        latestWkid?: number;
        vcsWkid?: number;
        wkid?: number;
        wkt?: string;
    }
    export interface EsriUpdateResult {
        error?: Types.EsriError;
        globalId?: string;
        objectId?: number;
        success?: boolean;
    }
    export interface EventBase {
        Description?: string;
        EventType?: Enums.SourceEventType;
        TriggerField?: string;
        TriggerType?: Enums.EventTriggerType;
        TriggerValue?: string;
    }
    export interface EventLayer {
        AssetUrl?: string;
        CityworksDescriptionFieldName?: string;
        CityworksEntitySidFieldName?: string;
        CityworksEntityTypeFieldName?: string;
        CityworksEntityUidFieldName?: string;
        CityworksTemplateFieldName?: string;
        Description?: string;
        DomainFieldName?: string;
        DomainId?: number;
        Enabled?: boolean;
        EventKeyField?: string;
        EventType?: Enums.SearchType;
        FillColor?: string;
        FillSymbolEnum?: number;
        ImageURL?: string;
        LayerId?: number;
        LayerName?: string;
        LineSymbolEnum?: number;
        MapUrl?: string;
        MarkerSymbolEnum?: number;
        MarkerSymbolSize?: number;
        OffsetX?: number;
        OffsetY?: number;
        OutlineColor?: string;
        OutlineColorWidth?: number;
        RefreshInterval?: number;
        SearchId?: number;
        SearchName?: string;
        SharedWithin?: Enums.ApplyLevel;
        StatusFieldName?: string;
        TableName?: string;
        UserId?: number;
        XField?: string;
        YField?: string;
    }
    export interface FeeSetupItemBase {
        AccountCode?: string;
        AnonymousFlag?: string;
        AutoRecalculate?: string;
        CaseDataDetailId?: number;
        CaseDataGroupId?: number;
        CreatedBy?: number;
        DateCreated?: Date;
        DateExpired?: Date;
        DateModified?: Date;
        ExpiredFlag?: string;
        FeeCategory?: string;
        FeeCode?: string;
        FeeDesc?: string;
        FeeDetails?: string;
        FeeSetupId?: number;
        FeeTypeId?: number;
        GrandSumFlag?: string;
        ModifiedBy?: number;
        OrgId?: number;
        RegisteredFlag?: string;
        TableName?: string;
    }
    export interface GeocodeRequest {
        Address?: string;
        City?: string;
        DispatchTo?: string;
        DispatchToField?: string;
        District?: string;
        DomainId?: number;
        ListWOSR?: boolean;
        MapPage?: string;
        MaxResults?: number;
        SearchExtent?: Types.GISExtent;
        Shop?: string;
        State?: string;
        StreetName?: string;
        SubmitTo?: string;
        SubmitToField?: string;
        SubmitToLayer?: string;
        TileNo?: string;
        WithinDistance?: number;
        X?: number;
        Y?: number;
        Zip?: string;
    }
    export interface GISExtent {
        XMax?: number;
        XMin?: number;
        YMax?: number;
        YMin?: number;
    }
    export interface GISPoint {
        Tag?: Object;
        X?: number;
        Y?: number;
    }
    export interface GlobalPreference extends Types.Preference {
    }
    export interface HtmlWidget {
        Header?: string;
        Height?: string;
        Html?: string;
        Id?: number;
        Position?: number;
        Url?: string;
        WidgetZoneId?: number;
    }
    export interface IEsriGeometry {
        extent?: Types.EsriExtent;
        geometryType?: string;
        spatialReference?: Types.EsriSpatialReference;
    }
    export interface InspAttachment extends Types.AttachmentBase {
        Id?: number;
        InspectionId?: number;
    }
    export interface InspectionAnswer {
        AnswerId?: number;
        AnswerValue?: string;
        InspectionId?: number;
        QuestionId?: number;
    }
    export interface InspectionBase {
        ActFinishDate?: Date;
        Cancel?: boolean;
        CancelledBy?: string;
        CancelledBySid?: number;
        CancelReason?: string;
        ClosedBy?: string;
        ClosedBySid?: number;
        CloseSR?: boolean;
        CondRating?: number;
        CondScore?: number;
        CreatedByCycle?: boolean;
        CycleFrom?: Enums.RepeatFromDate;
        CycleIntervalNum?: number;
        CycleIntervalUnit?: Enums.RepeatIntervalUnit;
        CycleType?: Enums.RepeatType;
        Date1?: Date;
        Date2?: Date;
        Date3?: Date;
        Date4?: Date;
        Date5?: Date;
        DateCancelled?: Date;
        DateClosed?: Date;
        DateSubmitTo?: Date;
        District?: string;
        DomainId?: number;
        Effort?: number;
        EntitySid?: number;
        EntityType?: string;
        EntityUid?: string;
        FeatureId?: number;
        FeatureType?: string;
        FeatureUid?: string;
        ForemanRecomnd?: string;
        FromDate?: Date;
        InitiateDate?: Date;
        InitiatedBy?: string;
        InitiatedByApp?: string;
        InitiatedBySid?: number;
        InspDate?: Date;
        InspectedBy?: string;
        InspectedBySid?: number;
        InspectionId?: number;
        InspTemplateId?: number;
        InspTemplateName?: string;
        InspX?: number;
        InspY?: number;
        IsClosed?: boolean;
        Location?: string;
        MapPage?: string;
        Num1?: number;
        Num2?: number;
        Num3?: number;
        Num4?: number;
        Num5?: number;
        ObservationSum?: string;
        ParentInspId?: number;
        Priority?: string;
        PrjFinishDate?: Date;
        PrjStartDate?: Date;
        RepairsMade?: string;
        RequestId?: number;
        Resolution?: string;
        Shop?: string;
        Status?: string;
        StreetName?: string;
        SubmitTo?: string;
        SubmitToEmployeeSid?: number;
        SubmitToName?: string;
        SubmitToSid?: number;
        Text1?: string;
        Text10?: string;
        Text2?: string;
        Text3?: string;
        Text4?: string;
        Text5?: string;
        Text6?: string;
        Text7?: string;
        Text8?: string;
        Text9?: string;
        TileNo?: string;
        UpdateMap?: boolean;
        WorkOrderId?: string;
    }
    export interface InspectionQuestionDetails {
        Explanation?: string;
        Instruction?: string;
        QuestionId?: number;
    }
    export interface InspectionRelatedInspection {
        InspectionId?: number;
        RelatedInspectionId?: number;
    }
    export interface InspectionSecurity {
        CanCancel?: boolean;
        CanClose?: boolean;
        CanDelete?: boolean;
        CanUpdate?: boolean;
        CanView?: boolean;
        InspectionId?: number;
    }
    export interface InspectionTemplateSecurity {
        CanCreate?: boolean;
        InspTemplateId?: number;
    }
    export interface InspQuestAnswer {
        Answer?: string;
        AnswerFormat?: Enums.QuestAnswerFormat;
        AnswerId?: number;
        CodeType?: string;
        Config?: string;
        Explanation?: string;
        Instruction?: string;
        MaxValue?: number;
        MinValue?: number;
        NextQuestionId?: number;
        QuestionId?: number;
        Score?: number;
    }
    export interface InspQuestionPanelBase {
        InspTemplateId?: number;
        PanelId?: number;
        Sequence?: number;
        Title?: string;
    }
    export interface InspTemplateBase {
        CycleFrom?: Enums.RepeatFromDate;
        CycleIncludeWeekends?: boolean;
        CycleIntervalNum?: number;
        CycleIntervalUnit?: Enums.RepeatIntervalUnit;
        CycleType?: Enums.RepeatType;
        DateModified?: Date;
        Description?: string;
        Duration?: number;
        DurationUnit?: Enums.WorkDurationUnit;
        Effort?: number;
        InspTemplateId?: number;
        InspTemplateName?: string;
        IsActive?: boolean;
        PrintTemplate?: string;
        Priority?: string;
        QAModel?: Enums.QASequenceModel;
        RatingMethod?: Enums.AssetRatingMethod;
        RequireAssetOnClose?: boolean;
        SubmitToEmployeeSid?: number;
        SubmitToName?: string;
        WorkMonth?: string;
    }
    export interface InspTemplateQA {
        Answers?: Types.InspQuestAnswer[];
        BranchingModel?: boolean;
        Panels?: Types.InspQuestionPanelBase[];
        Questions?: Types.InspTempQuestion[];
    }
    export interface InspTempQuestion {
        InspTemplateId?: number;
        PanelId?: number;
        Question?: string;
        QuestionId?: number;
        QuestionSequence?: number;
        QuestionType?: Enums.InspQuestionType;
        Required?: boolean;
        Weight?: number;
    }
    export interface IWidget {
        Id?: number;
        Position?: number;
        WidgetZoneId?: number;
    }
    export interface IWidgetContainer {
        ContainerType?: Enums.WidgetContainerType;
        DomainId?: number;
        EmployeeSid?: number;
        Header?: string;
        Id?: number;
        Position?: number;
        Zones?: Types.IWidgetZone[];
    }
    export interface IWidgetContainerTab {
        Containers?: Types.IWidgetContainer[];
        DomainId?: number;
        EmployeeSid?: number;
        Header?: string;
        Id?: number;
        Position?: number;
        TabType?: Enums.WidgetContainerTabType;
    }
    export interface IWidgetZone {
        Header?: string;
        Id?: number;
        Position?: number;
        WidgetContainerId?: number;
        Widgets?: Types.IWidget[];
        Width?: string;
    }
    export interface JobCode {
        Code?: string;
        Description?: string;
    }
    export interface LaborCostDetail {
        Cost?: number;
        CostCode?: string;
        Description?: string;
        LaborCostId?: number;
    }
    export interface MapViewExtent {
        XMax?: number;
        XMin?: number;
        YMax?: number;
        YMin?: number;
    }
    export interface MaterialBase {
        AuditInterval?: string;
        BinImagePath?: string;
        BinLocation?: string;
        CatClass?: Enums.AbcCatClass;
        Category?: string;
        CostType?: Enums.MaterialCostType;
        CUCategory?: string;
        Custom1?: number;
        Custom2?: number;
        Custom3?: string;
        Custom4?: string;
        Custom5?: string;
        DefaultImgPath?: string;
        Description?: string;
        Detail?: string;
        ExpirationDate?: Date;
        GdbSubtype?: string;
        IsAssembly?: boolean;
        JustificationRequired?: boolean;
        Manufacturer?: string;
        MaterialSid?: number;
        MaterialUid?: string;
        MinQuantity?: number;
        Model?: string;
        PartNumber?: string;
        SecBinImagePath?: string;
        SecBinLocation?: string;
        Splittable?: boolean;
        StockOnHand?: number;
        Storeroom?: string;
        Supplier?: string;
        UnitCost?: number;
        UnitOfMeasure?: string;
        Viewable?: boolean;
    }
    export interface MaterialCost {
        AcctNum?: string;
        Cost?: number;
        Description?: string;
        DomainId?: number;
        EntityType?: string;
        EntityUid?: string;
        MaterialCostId?: number;
        MaterialSid?: number;
        MaterialUid?: string;
        Source?: string;
        StockModified?: boolean;
        TaskName?: string;
        TransactionId?: number;
        TransDate?: Date;
        UnitsRequired?: number;
        UsageType?: Enums.CostUsage;
        WorkOrderId?: string;
        WOTaskId?: number;
    }
    export interface MaterialKeywordBase {
        Keyword?: string;
        MaterialSid?: number;
    }
    export interface MaterialNode extends Types.NodeBase {
    }
    export interface NodeBase {
        Description?: string;
        DomainId?: number;
        NodeName?: string;
        NodeSid?: number;
        NodeTextType?: Enums.TreeNodeTextType;
        NodeType?: Enums.TreeNodeType;
        ParentSid?: number;
        Uid?: string;
    }
    export interface NotesWidget {
        Header?: string;
        Height?: string;
        Id?: number;
        Notes?: string;
        Position?: number;
        WidgetZoneId?: number;
    }
    export interface NumericFieldValue {
        FieldValueList?: string[];
        IncludeNulls?: boolean;
        IsMaxValueExclusive?: boolean;
        IsMinValueExclusive?: boolean;
        MaxTableFieldMultiplier?: number;
        MaxTableFieldName?: string;
        MaxValue?: number;
        MinTableFieldMultiplier?: number;
        MinTableFieldName?: string;
        MinValue?: number;
        UseTableFieldForMax?: boolean;
        UseTableFieldForMin?: boolean;
        ValueMode?: Enums.NumericValueMode;
    }
    export interface PacketConfiguration {
        EpochDates?: boolean;
        Model?: Enums.WebHookPacketModel;
        Referer?: string;
        Template?: string;
    }
    export interface PllPreferenceItem {
        Name?: string;
        OrgId?: number;
        Value?: string;
    }
    export interface Preference {
        Category?: string;
        DefaultValue?: string;
        Element?: string;
    }
    export interface ProblemLeafBase {
        AutoClose?: boolean;
        Cancel?: boolean;
        Comments?: string;
        DateModified?: Date;
        DefaultProject?: string;
        DefaultProjectSid?: number;
        Description?: string;
        DispatchTo?: number;
        DomainId?: number;
        Duration?: number;
        DurationUnit?: Enums.WorkDurationUnit;
        Effort?: number;
        ForPublicSite?: boolean;
        OtherSysCode?: string;
        OtherSysCodeCWId?: number;
        OtherSysDesc1?: string;
        OtherSysDesc2?: string;
        Printer?: string;
        Priority?: string;
        ProbCategory?: string;
        ProblemCode?: string;
        ProblemSid?: number;
        QAModel?: Enums.QASequenceModel;
        ReqCustFieldCatId?: number;
        SRPrintTmpt?: string;
        SubmitTo?: number;
    }
    export interface ProblemName {
        Description?: string;
        DomainId?: number;
        OtherSysCode?: string;
        OtherSysDesc1?: string;
        OtherSysDesc2?: string;
        ProblemCode?: string;
        ProblemSid?: number;
    }
    export interface ProblemNode extends Types.NodeBase {
        Cancel?: boolean;
    }
    export interface ProblemQA {
        Answers?: Types.QuestionAnswerBase[];
        BranchingModel?: boolean;
        Questions?: Types.ProblemQuestionBase[];
    }
    export interface ProblemQuestionBase {
        ProblemSid?: number;
        Question?: string;
        QuestionId?: number;
        QuestionSequence?: number;
    }
    export interface ProblemSecurity {
        CanCreate?: boolean;
        ProblemSid?: number;
    }
    export interface PWEntity {
        Code?: string;
        Description?: string;
        EntityTableType?: Enums.CWEntityType;
        Module?: string;
        Tag?: Object;
    }
    export interface PWModule {
        Module?: string;
        ModuleName?: string;
    }
    export interface QuestionAnswerBase {
        Answer?: string;
        AnswerFormat?: string;
        AnswerId?: number;
        AnswerSequence?: number;
        DispatchTo?: number;
        DispatchToFieldName?: string;
        NextQuestionId?: number;
        Priority?: string;
        QuestionId?: number;
        SubmitTo?: number;
        SubmitToFieldName?: string;
        SubmitToLayerName?: string;
        TellCaller?: string;
    }
    export interface QueueMessage extends Types.QueueMessageBase {
        HookId?: number;
        Id?: number;
        Result?: string;
        Status?: Enums.QueueStatus;
    }
    export interface QueueMessageBase {
        DateCreated?: Date;
        DateUpdated?: Date;
        HookType?: Enums.ActionType;
        Packet?: string;
    }
    export interface QueueMessagePacket {
        SimpleHooks?: Types.WebHookPacket[];
        TemplateHooks?: Types.WebHookPacket[];
    }
    export interface QuickSearchItem {
        Description?: string;
        Id?: string;
        Name?: string;
        Type?: string;
    }
    export interface QuickSearchResult {
        Inspections?: Types.QuickSearchItem[];
        Permits?: Types.QuickSearchItem[];
        ServiceRequests?: Types.QuickSearchItem[];
        WorkOrders?: Types.QuickSearchItem[];
    }
    export interface RecentActivity extends Types.CoreDomainBase {
        ActivityId?: string;
        DateTimeStamp?: Date;
        Description?: string;
        DisplayId?: string;
        EmployeeSid?: number;
        Kind?: string;
    }
    export interface ReportLinksWidget {
        Header?: string;
        Height?: number;
        Id?: number;
        Position?: number;
        Settings?: Types.ReportLinksWidgetSettings;
        WidgetZoneId?: number;
    }
    export interface ReportLinksWidgetSettings {
        ReportIdList?: number[];
    }
    export interface ReqCustField extends Types.CategoryCustField {
        RequestId?: number;
    }
    export interface RequestAttachment extends Types.AttachmentBase {
        Id?: number;
        RequestId?: number;
    }
    export interface RequestBase {
        Cancel?: boolean;
        CancelledBy?: string;
        CancelledBySid?: number;
        CancelReason?: string;
        ClosedBy?: string;
        ClosedBySid?: number;
        Date1?: Date;
        Date2?: Date;
        Date3?: Date;
        Date4?: Date;
        Date5?: Date;
        DateCancelled?: Date;
        DateDispatchOpen?: Date;
        DateDispatchTo?: Date;
        DateInvtDone?: Date;
        DateSubmitTo?: Date;
        DateSubmitToOpen?: Date;
        DateTimeClosed?: Date;
        DateTimeInit?: Date;
        Description?: string;
        Details?: string;
        DispatchOpenBy?: string;
        DispatchOpenBySid?: number;
        DispatchTo?: string;
        DispatchToSid?: number;
        DomainId?: number;
        Effort?: number;
        Excursion?: boolean;
        FieldInvtDone?: boolean;
        InitiatedBy?: string;
        InitiatedByApp?: string;
        InitiatedBySid?: number;
        IsClosed?: boolean;
        LaborCost?: number;
        LockedByDesktopUser?: string;
        MapPage?: string;
        Num1?: number;
        Num2?: number;
        Num3?: number;
        Num4?: number;
        Num5?: number;
        OtherSystemCode?: string;
        OtherSystemDesc?: string;
        OtherSystemDesc2?: string;
        OtherSystemId?: string;
        OtherSystemStatus?: string;
        Priority?: string;
        PrjCompleteDate?: Date;
        ProbAddress?: string;
        ProbAddType?: string;
        ProbAptNum?: string;
        ProbCity?: string;
        ProbDistrict?: string;
        ProbLandmark?: string;
        ProblemCode?: string;
        ProblemSid?: number;
        ProbLocation?: string;
        ProbState?: string;
        ProbZip?: string;
        ProjectName?: string;
        ProjectSid?: number;
        ReqCategory?: string;
        ReqCustFieldCatId?: number;
        RequestId?: number;
        Resolution?: string;
        Shop?: string;
        SRX?: number;
        SRY?: number;
        Status?: string;
        StreetName?: string;
        SubmitTo?: string;
        SubmitToEmail?: string;
        SubmitToOpenBy?: string;
        SubmitToOpenBySid?: number;
        SubmitToPager?: string;
        SubmitToPhone?: string;
        SubmitToSid?: number;
        Text1?: string;
        Text10?: string;
        Text11?: string;
        Text12?: string;
        Text13?: string;
        Text14?: string;
        Text15?: string;
        Text16?: string;
        Text17?: string;
        Text18?: string;
        Text19?: string;
        Text2?: string;
        Text20?: string;
        Text3?: string;
        Text4?: string;
        Text5?: string;
        Text6?: string;
        Text7?: string;
        Text8?: string;
        Text9?: string;
        TileNo?: string;
        WONeeded?: boolean;
        WorkOrderId?: string;
    }
    export interface RequestInspection {
        InspectionId?: number;
        RequestId?: number;
    }
    export interface RequestLaborCost {
        AcctNum?: string;
        BenefitCost?: number;
        Cost?: number;
        Description?: string;
        DomainId?: number;
        FinishTime?: Date;
        GroupName?: string;
        HolidayCost?: number;
        Hours?: number;
        LaborCostDetails?: Types.LaborCostDetail[];
        LaborCostId?: number;
        LaborName?: string;
        LaborSid?: number;
        OccupationCode?: string;
        OccupationId?: number;
        OtherCost?: number;
        OverheadCost?: number;
        OvertimeCost?: number;
        RateType?: Enums.LaborRateType;
        RegularCost?: number;
        RequestId?: number;
        ShiftDiffCost?: number;
        StandbyCost?: number;
        StartTime?: Date;
        TimesheetId?: number;
        TransDate?: Date;
    }
    export interface RequestWorkOrder {
        RequestId?: number;
        WorkOrderId?: string;
    }
    export interface SearchCustomField {
        AnswerList?: string[];
        Answers?: string;
        CustFieldId?: number;
        SearchId?: number;
        TableName?: string;
    }
    export interface SearchDateField extends Types.DateFieldValue {
        FieldName?: string;
        SearchId?: number;
        TableName?: string;
    }
    export interface SearchDefinition extends Types.SearchDefinitionName {
        SearchCustomFields?: Types.SearchCustomField[];
        SearchDateFields?: Types.SearchDateField[];
        SearchFields?: Types.SearchField[];
        SearchGroupByFields?: Types.SearchResultField[];
        SearchMapLayerFields?: Types.SearchMapLayerField[];
        SearchNumericFields?: Types.SearchNumericField[];
        SearchPolygonFields?: Types.SearchPolygonField[];
        SearchQAFields?: Types.SearchQAField[];
        SearchQANumericFields?: Types.SearchQANumericField[];
        SearchResultFields?: Types.SearchResultField[];
    }
    export interface SearchDefinitionName {
        AlternateMapUrl?: string;
        AlternateServiceUrl?: string;
        ApplyToEventLayer?: boolean;
        ApplyToInbox?: boolean;
        DateTimeModified?: Date;
        Description?: string;
        DomainId?: number;
        EmployeeName?: string;
        EmployeeSid?: number;
        MapUrl?: string;
        SearchFor?: Enums.SearchType;
        SearchId?: number;
        SearchName?: string;
        ServiceUrl?: string;
        SharedWithin?: Enums.ApplyLevel;
        StartDayOfWeek?: Enums.DayOfWeek;
    }
    export interface SearchField {
        FieldName?: string;
        FieldValueList?: string[];
        FieldValues?: string;
        SearchId?: number;
        TableName?: string;
    }
    export interface SearchMapLayerField {
        FieldValueList?: string[];
        FieldValues?: string;
        MapLayer?: string;
        MapLayerField?: string;
        SearchId?: number;
        TemplateId?: number;
    }
    export interface SearchNumericField extends Types.NumericFieldValue {
        FieldName?: string;
        FieldValues?: string;
        FormattedString?: string;
        SearchId?: number;
        TableName?: string;
    }
    export interface SearchPolygonField {
        FieldName?: string;
        SearchId?: number;
        TableName?: string;
    }
    export interface SearchQAField {
        AnswerList?: string[];
        Answers?: string;
        FieldName?: string;
        QuestionId?: number;
        SearchId?: number;
        TableName?: string;
        TemplateId?: number;
    }
    export interface SearchQANumericField extends Types.NumericFieldValue {
        FieldName?: string;
        FieldValues?: string;
        QuestionId?: number;
        SearchId?: number;
        TableName?: string;
        TemplateId?: number;
    }
    export interface SearchResultField {
        DescendingOrder?: boolean;
        FieldCaption?: string;
        FieldName?: string;
        GroupBy?: boolean;
        GroupBySequence?: number;
        IsOrderByField?: boolean;
        OrderBySequence?: number;
        SearchId?: number;
        SequenceId?: number;
        TableName?: string;
    }
    export interface SearchResultsWidget {
        Header?: string;
        Height?: number;
        Id?: number;
        Position?: number;
        SearchId?: number;
        SearchType?: Enums.SearchType;
        Settings?: Types.SearchResultsWidgetSettings;
        WidgetZoneId?: number;
    }
    export interface SearchResultsWidgetSettings {
        AutosizeColumns?: boolean;
        Rows?: number;
        SearchView?: Enums.SearchView;
    }
    export interface SearchSecurity {
        ContractCanViewCost?: boolean;
        EmployeeSid?: number;
        RequestCanViewLaborCost?: boolean;
        WorkOrderCanViewEquipmentCost?: boolean;
        WorkOrderCanViewLaborCost?: boolean;
        WorkOrderCanViewMaterialCost?: boolean;
    }
    export interface SearchTypeInfo {
        DisplayName?: string;
        Name?: string;
        SearchType?: Enums.SearchType;
    }
    export interface ServiceRequestAnswer {
        AnswerId?: number;
        AnswerValue?: string;
    }
    export interface ServiceRequestSecurity {
        CanAddCaller?: boolean;
        CanAddLabor?: boolean;
        CanCancel?: boolean;
        CanClose?: boolean;
        CanDelete?: boolean;
        CanDeleteCaller?: boolean;
        CanDeleteComment?: boolean;
        CanDeleteLabor?: boolean;
        CanUpdate?: boolean;
        CanUpdateCaller?: boolean;
        CanUpdateComment?: boolean;
        CanUpdateLabor?: boolean;
        CanView?: boolean;
        CanViewCaller?: boolean;
        CanViewLabor?: boolean;
        CanViewLaborCost?: boolean;
        RequestId?: number;
    }
    export interface SimpleGisServiceInfo {
        Active?: boolean;
        DomainId?: number;
        RefreshInterval?: number;
        Security?: string[];
        SecurityId?: number;
        SequenceId?: number;
        Service?: string;
        ServiceId?: number;
        ServiceName?: string;
        ServiceType?: Enums.GISServiceType;
        SharingUrl?: string;
        TokenRequired?: boolean;
    }
    export interface StoreDomainBase {
        Description?: string;
        DomainId?: number;
        DomainName?: string;
    }
    export interface StreetCode {
        City?: string;
        Code?: string;
        District?: string;
        Id?: number;
        State?: string;
        StreetName?: string;
        Zone?: string;
    }
    export interface SystemTimeZone {
        DaylightName?: string;
        DisplayName?: string;
        Id?: string;
        StandardName?: string;
        SupportsDaylightSavingTime?: boolean;
        TotalHoursBaseUtcOffset?: number;
    }
    export interface TaskCode {
        ResponseCode?: string;
        TaskSid?: number;
    }
    export interface TaskKeyword {
        Keyword?: string;
        TaskSid?: number;
    }
    export interface TaskLeafBase {
        AssignedTo?: number;
        AssignedToName?: string;
        Cancel?: boolean;
        Comments?: string;
        Description?: string;
        DomainId?: number;
        Duration?: number;
        NotifyMM?: boolean;
        ResponseLabel?: string;
        Shop?: string;
        TaskName?: string;
        TaskSid?: number;
    }
    export interface TaskNode extends Types.NodeBase {
    }
    export interface UserMapExtent extends Types.MapViewExtent {
        DateTimeModified?: Date;
        ExtentId?: number;
        ExtentName?: string;
        IsDefault?: boolean;
        SharedWithin?: Enums.ApplyLevel;
        UserId?: number;
    }
    export interface UserMapScale {
        DateTimeModified?: Date;
        Scale?: number;
        ScaleId?: number;
        SharedWithin?: Enums.ApplyLevel;
        UserId?: number;
    }
    export interface UserPreference extends Types.Preference {
        EmployeeSid?: number;
    }
    export interface UserPreferences {
        ClusterEventLayers?: boolean;
        Data?: Types.UserPreference[];
        DistrictFieldName?: string;
        DistrictLayerName?: string;
        EquipmentTreeDisplayText?: string;
        MapFavorites?: string[];
        MapPageFieldName?: string;
        MapPageLayerName?: string;
        MapSelectionBaseColor?: string;
        MapSelectionBaseFill?: string;
        MapSelectionBaseStroke?: string;
        MapSelectionSelectedColor?: string;
        MapSelectionSelectedFill?: string;
        MapSelectionSelectedStroke?: string;
        MaterialTreeDisplayText?: string;
        ShopFieldName?: string;
        ShopLayerName?: string;
        SpatialReference?: number;
        TileNoFieldName?: string;
        TileNoLayerName?: string;
    }
    export interface ViewDataUploadFilesResult {
        AttachedBy?: string;
        CaObjectId?: string;
        CaTaskId?: string;
        Comments?: string;
        Date?: Date;
        DeleteType?: string;
        DeleteUrl?: string;
        Name?: string;
        ReldocsId?: string;
        Size?: number;
        TaskCode?: string;
        TaskDesc?: string;
        ThumbnailUrl?: string;
        Type?: string;
        Url?: string;
    }
    export interface WeatherWidget {
        Header?: string;
        Id?: number;
        Position?: number;
        WidgetZoneId?: number;
        ZipCode?: string;
    }
    export interface WebHookBase {
        Description?: string;
        DomainId?: number;
        Fields?: string[];
        Id?: number;
        Method?: string;
        OutputType?: Enums.WebHookOutputType;
        PacketConfig?: Types.PacketConfiguration;
        ParameterName?: string;
        SourceType?: Enums.EventSourceType;
        Url?: string;
    }
    export interface WebHookPacket {
        HookId?: number;
        OutputType?: Enums.WebHookOutputType;
        SimpleData?: string;
        SourceEvent?: Enums.SourceEventType;
        SourceType?: Enums.EventSourceType;
        TemplateData?: string;
    }
    export interface WOAttachment extends Types.AttachmentBase {
        Id?: number;
        TaskName?: string;
        WorkOrderId?: string;
        WOTaskId?: number;
    }
    export interface WOCustField extends Types.CategoryCustField {
        WorkOrderId?: string;
    }
    export interface WorkOrderBase {
        AcctNum?: string;
        ActivityZone?: string;
        ActualFinishDate?: Date;
        ActualStartDate?: Date;
        ApplyToEntity?: string;
        AssetGroup?: string;
        Cancel?: boolean;
        CancelledBy?: string;
        CancelledBySid?: number;
        CancelReason?: string;
        ClosedBySid?: number;
        ContractorName?: string;
        ContractorSid?: number;
        ContractWOId?: string;
        ContrBillable?: boolean;
        CreatedByCycle?: boolean;
        CycleFrom?: Enums.RepeatFromDate;
        CycleIntervalNum?: number;
        CycleIntervalUnit?: Enums.RepeatIntervalUnit;
        CycleType?: Enums.RepeatType;
        Date1?: Date;
        Date2?: Date;
        Date3?: Date;
        Date4?: Date;
        Date5?: Date;
        DateCancelled?: Date;
        DatePrinted?: Date;
        DateSubmitTo?: Date;
        DateSubmitToOpen?: Date;
        DateToBePrinted?: Date;
        DateWOClosed?: Date;
        Description?: string;
        District?: string;
        DomainId?: number;
        Effort?: number;
        ExpenseType?: Enums.WOExpenseType;
        FromDate?: Date;
        InitiateDate?: Date;
        InitiatedBy?: string;
        InitiatedByApp?: string;
        InitiatedBySid?: number;
        IsClosed?: boolean;
        IsReactive?: boolean;
        LegalBillable?: boolean;
        Location?: string;
        LockedByDesktopUser?: string;
        MapPage?: string;
        MapTemplateName?: string;
        Num1?: number;
        Num2?: number;
        Num3?: number;
        Num4?: number;
        Num5?: number;
        NumDaysBefore?: number;
        PrimaryContractId?: number;
        Priority?: string;
        ProjectName?: string;
        ProjectSid?: number;
        ProjFinishDate?: Date;
        ProjStartDate?: Date;
        RequestedBy?: string;
        RequestedBySid?: number;
        Resolution?: string;
        ScheduleDate?: Date;
        Shop?: string;
        SourceWOId?: string;
        Stage?: Enums.WOStage;
        Status?: string;
        StreetName?: string;
        SubmitTo?: string;
        SubmitToOpenBy?: string;
        SubmitToOpenBySid?: number;
        SubmitToSid?: number;
        Supervisor?: string;
        SupervisorSid?: number;
        Text1?: string;
        Text10?: string;
        Text11?: string;
        Text12?: string;
        Text13?: string;
        Text14?: string;
        Text15?: string;
        Text16?: string;
        Text17?: string;
        Text18?: string;
        Text19?: string;
        Text2?: string;
        Text20?: string;
        Text3?: string;
        Text4?: string;
        Text5?: string;
        Text6?: string;
        Text7?: string;
        Text8?: string;
        Text9?: string;
        TileNo?: string;
        TransToWOId?: string;
        Unattached?: boolean;
        UnitsAccompDesc?: string;
        UnitsAccompDescLock?: boolean;
        UnitsAccomplished?: number;
        UpdateMap?: boolean;
        WOAddress?: string;
        WOCategory?: string;
        WOClosedBy?: string;
        WOCost?: number;
        WOCustFieldCatId?: number;
        WOEquipCost?: number;
        WOLaborCost?: number;
        WOMapScale?: number;
        WOMatCost?: number;
        WOOutput?: Enums.WOOutputType;
        WOPermitCost?: number;
        WorkCompletedBy?: string;
        WorkCompletedBySid?: number;
        WorkOrderId?: string;
        WOTemplateId?: string;
        WOXCoordinate?: number;
        WOYCoordinate?: number;
    }
    export interface WorkOrderCostSummary {
        ActivityType?: string;
        AssetGroup?: string;
        AssetId?: string;
        AssetType?: string;
        CloseDate?: Date;
        EquipmentCost?: number;
        LaborCost?: number;
        LineItemCost?: number;
        MaterialCost?: number;
        StartDate?: Date;
        TotalHours?: number;
        WOCategory?: string;
        WorkOrderId?: string;
    }
    export interface WorkOrderEntity extends Types.WorkOrderEntityBase {
        AttributeOIDFieldName?: string;
        Attributes?: {[key: string]: Object};
        AttributeUIDFieldName?: string;
        RelatedFeature?: Types.WorkOrderEntity;
        Tag?: Object;
    }
    export interface WorkOrderEntityBase {
        Address?: string;
        EntitySid?: number;
        EntityType?: string;
        EntityUid?: string;
        FeatureId?: number;
        FeatureType?: string;
        FeatureUid?: string;
        IsBlank?: boolean;
        LegacyId?: string;
        Location?: string;
        ObjectId?: number;
        TileNo?: string;
        WarrantyDate?: Date;
        WorkCompleted?: boolean;
        WorkOrderId?: string;
        X?: number;
        Y?: number;
    }
    export interface WorkOrderInspection {
        InspectionId?: number;
        WorkOrderId?: string;
    }
    export interface WorkOrderLaborCost {
        AcctNum?: string;
        BenefitCost?: number;
        ContractorNumber?: string;
        Cost?: number;
        Description?: string;
        DomainId?: number;
        EntityType?: string;
        EntityUid?: string;
        FinishDate?: Date;
        GroupName?: string;
        HolidayCost?: number;
        Hours?: number;
        LaborCostDetails?: Types.LaborCostDetail[];
        LaborCostId?: number;
        LaborName?: string;
        LaborSid?: number;
        LaborType?: Enums.LaborCategory;
        OccupationCode?: string;
        OccupationId?: number;
        OtherCost?: number;
        OverheadCost?: number;
        OvertimeCost?: number;
        RateType?: Enums.LaborRateType;
        RegularCost?: number;
        ShiftDiffCost?: number;
        StandbyCost?: number;
        StartDate?: Date;
        TaskName?: string;
        TimesheetId?: number;
        TransDate?: Date;
        UsageType?: Enums.CostUsage;
        WorkOrderId?: string;
        WOTaskId?: number;
    }
    export interface WorkOrderRelatedWorkOrder {
        RelatedWorkOrderId?: string;
        WorkOrderId?: string;
    }
    export interface WorkOrderSecurity {
        CanAddEquipment?: boolean;
        CanAddLabor?: boolean;
        CanAddLineItems?: boolean;
        CanAddMaterial?: boolean;
        CanAddTasks?: boolean;
        CanCancel?: boolean;
        CanClose?: boolean;
        CanDelete?: boolean;
        CanDeleteComment?: boolean;
        CanDeleteEquipment?: boolean;
        CanDeleteLabor?: boolean;
        CanDeleteLineItems?: boolean;
        CanDeleteMaterial?: boolean;
        CanDeleteTasks?: boolean;
        CanUpdate?: boolean;
        CanUpdateComment?: boolean;
        CanUpdateEquipment?: boolean;
        CanUpdateLabor?: boolean;
        CanUpdateLineItems?: boolean;
        CanUpdateMaterial?: boolean;
        CanUpdateTasks?: boolean;
        CanView?: boolean;
        CanViewCosts?: boolean;
        CanViewEquipment?: boolean;
        CanViewEquipmentCost?: boolean;
        CanViewLabor?: boolean;
        CanViewLaborCost?: boolean;
        CanViewLineItems?: boolean;
        CanViewMaterial?: boolean;
        CanViewMaterialCost?: boolean;
        CanViewTasks?: boolean;
        WorkOrderId?: string;
    }
    export interface WorkOrderTemplateSecurity {
        CanCreate?: boolean;
        WOTemplateId?: string;
    }
    export interface WOTask {
        ActFinishDate?: Date;
        ActStartDate?: Date;
        AssignedTo?: number;
        AssignedToName?: string;
        Comments?: string;
        Description?: string;
        DomainId?: number;
        Duration?: number;
        Effort?: number;
        EntityType?: string;
        EntityUid?: string;
        IsRework?: boolean;
        PermitNum?: string;
        PermitSid?: number;
        ProceedOk?: boolean;
        ProjFinishDate?: Date;
        ProjStartDate?: Date;
        Shop?: string;
        Status?: string;
        TaskCode?: string;
        TaskName?: string;
        TaskSeqId?: number;
        TaskSid?: number;
        WorkOrderId?: string;
        WOTaskId?: number;
    }
    export interface WOTemplateBase {
        AcctNum?: string;
        ApplyToEntity?: string;
        AutoCreateTask?: boolean;
        Cancel?: boolean;
        Comments?: string;
        CopyCustomFieldVal?: Enums.CustomFieldValueSource;
        CreateDate?: Date;
        CycleFrom?: Enums.RepeatFromDate;
        CycleIncludeWeekends?: boolean;
        CycleIntervalNum?: number;
        CycleIntervalUnit?: Enums.RepeatIntervalUnit;
        CycleType?: Enums.RepeatType;
        DateModified?: Date;
        DaysToComplete?: number;
        DefaultProject?: string;
        DefaultProjectSid?: number;
        Description?: string;
        DomainId?: number;
        Effort?: number;
        ExpenseType?: Enums.WOExpenseType;
        Instructions?: string;
        IsReactive?: boolean;
        MaintScore?: number;
        MapTemplateName?: string;
        NumDaysBefore?: number;
        Printer?: string;
        Priority?: string;
        RequireAssetOnClose?: boolean;
        Shop?: string;
        Stage?: Enums.WOStage;
        Status?: string;
        SubmitToEmployeeSid?: number;
        SupervisorEmployeeSid?: number;
        UnitsAccompDesc?: string;
        UnitsAccompDescLock?: boolean;
        WarrantyDuration?: number;
        WarrantyDurationUnit?: Enums.RepeatIntervalUnit;
        WOCategory?: string;
        WOCustFieldCatId?: number;
        WOMapExtent?: string;
        WOMapScale?: number;
        WOOutput?: Enums.WOOutputType;
        WOPrintTmpt?: string;
        WorkMonth?: string;
        WOTemplateId?: string;
    }
    export interface WOTemplateName {
        ApplyToEntity?: string;
        Description?: string;
        IsTemplateClass?: boolean;
        WOCategory?: string;
        WOTemplateId?: string;
    }
    export interface WOTempTask {
        AssignedTo?: number;
        AssignedToName?: string;
        Comments?: string;
        Description?: string;
        DomainId?: number;
        Duration?: number;
        Effort?: number;
        Shop?: string;
        TaskName?: string;
        TaskSeqId?: number;
        TaskSid?: number;
        WOTemplateId?: string;
        WOTempTaskId?: number;
    }
}