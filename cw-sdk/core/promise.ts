﻿export namespace PromiseTypes {

    export class AbortablePromise<T> extends Promise<T> {
        public static REASON_ABORTED: string = 'ABORTED';
        constructor(executor: (resolve: (value?: T) => void, reject: (reason?: any) => void) => void) {
            super(executor);
        }
        abort: () => void;
    }
}