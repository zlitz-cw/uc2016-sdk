﻿import * as Core from './bundles/core';
import * as ServiceInterface from './bundles/service-interfaces';
import {AuthenticationService, AuthenticationServiceTypes, ApiService, IApiService } from './api/services/general/authenticationservice';
import {WorkOrderService, WorkOrderServiceTypes} from './api/services/ams/workorderservice';

import AbortablePromise = Core.PromiseTypes.AbortablePromise;


//Create a new api service
let apiService: IApiService = new ApiService("localhost/citywork.webapp");

//Create an authentication service and a workorder service
let authService: AuthenticationServiceTypes.IAuthenticationService = new AuthenticationService(apiService);
let woService: WorkOrderServiceTypes.IWorkOrderService = new WorkOrderService(apiService);

//Authenticate, then get the user, then search work orders
apiService.login("pw", "pw").then((authResponse) => {
    
    //Get the user info
    return authService.User({ LoginName: "pw" });

}, (error) => {

    console.error(new Error('Error Authenticating user'));
    console.error(error);

}).then((response: AuthenticationServiceTypes.Responses.User) => {

    let val: Core.Types.CWUser = response.Value;
    console.log(val);
    
    //Search workorders
    let request: WorkOrderServiceTypes.Requests.Search = {
        MaxResults: 10,
        ActualFinishDateIsNull: true
    };

    return woService.Search(request);

}, (error) => {

    console.error(new Error('Error getting user'));
    console.error(error);

}).then((response: WorkOrderServiceTypes.Responses.Search) => {

    let val: string[] = response.Value;
    console.log(val);

}, (error) => {

    console.error(new Error('Error searching workorders'));
    console.error(error);
});
