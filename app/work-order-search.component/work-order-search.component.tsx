import * as React from 'react';
import { ApiServiceFactory } from '../api-service-factory';
import { WorkOrderService, WorkOrderServiceTypes } from '../../cw-sdk/api/services/ams/workorderservice';
import { ServiceEnums } from '../../cw-sdk/core/service-enums';
import { WO_EDIT_URL } from '../constants';



export class WorkOrderSearch extends React.Component<IWorkOrderSearchProps,IState> {

    //--------------------------------------------------
    // Private
    //--------------------------------------------------
    private _woService: WorkOrderServiceTypes.IWorkOrderService;


    //====================================================================================================
    // PUBLIC
    //====================================================================================================
    public constructor() {
        super();

        this._woService = new WorkOrderService(ApiServiceFactory.getService());

        this.state = {};
    }



    /**
     * 
     */
    public onChange_txtWorkOrderIds(event: React.FormEvent<HTMLInputElement>) {
        let value = (event.target as HTMLInputElement).value || '';

        let ids = value.split(',').map(a => a.trim()).filter(a => a.length > 0);
        this.setState({woIdsToFind: ids});
    }



    /**
     * 
     */
    public onClick_search(): void {

        let ids = this.state.woIdsToFind || [];

        if (ids.length < 1) {
            return;
        }

        let searchRequest: WorkOrderServiceTypes.Requests.Search = {
            WorkOrderIds: ids
        };

        this._woService.Search(searchRequest)
        .then(response => {

            let error = !response || response.Status !== ServiceEnums.CoreResponseStatus.Ok;
            if (error) {
                throw new Error('Error performing search');
            } else {

                let woIds = response.Value || [];
                return this._woService.ByIds({ WorkOrderIds: woIds });                
            }
        })
        .then(response => {

            let error = !response || response.Status !== ServiceEnums.CoreResponseStatus.Ok;
            if (error) {
                throw new Error('Error loading work orders');
            } else {

                let wos = response.Value || [];
                
                let items: WoResult[] = wos.map(wo => {
                    let retVal: WoResult = {
                        description: wo.Description,
                        id: wo.WorkOrderId,
                        link: `${WO_EDIT_URL}${wo.WorkOrderId}`
                    };

                    return retVal;
                });

                this.setState({ workOrders: items});
            }
        })
        .catch(error => {
            console.log(error);
            alert('Error');
        });
    }



    /**
     * React hook
     */
    public render(): JSX.Element {

        let woResults: JSX.Element;
        if ((this.state.workOrders || []).length > 0) {

            woResults = (
                <div>
                    <div>Found {this.state.workOrders.length} work orders</div>
                    {
                        this.state.workOrders.map(a => {
                            return (
                                <div>
                                   <a href={a.link}>{a.id} - {a.description}</a>
                                </div>
                            )
                        })
                    }
                </div>
            );
        }


        return (
            <div>
                <div>Work Order Search</div>
                <div>
                    <input type="text" placeholder="Work Order Ids" onChange={this.onChange_txtWorkOrderIds.bind(this)}/>
                    <button disabled={(this.state.woIdsToFind || []).length < 1} onClick={this.onClick_search.bind(this)}>Search</button>
                </div>
                {woResults}
            </div>
        );
    }
}

export interface IWorkOrderSearchProps {

}

interface IState {
    woIdsToFind?: string[];
    workOrders?: WoResult[];
}

interface WoResult {
    id: string;
    description: string;
    link: string;
}