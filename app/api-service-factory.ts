import { ApiService, IApiService } from '../cw-sdk/http/api-service';
export { ApiService, IApiService } from  '../cw-sdk/http/api-service' 
import { BASE_URL } from './constants'

export class ApiServiceFactory {

    //--------------------------------------------------
    // PRIVATE
    //--------------------------------------------------
    private static _apiService: IApiService | null = null;

    //====================================================================================================
    // PUBLIC
    //====================================================================================================
    /**
     * 
     */
    public static getService(): IApiService | null {

        if (this._apiService === null) {
            this._apiService = new ApiService(BASE_URL);
        }

        return this._apiService;
    }
}