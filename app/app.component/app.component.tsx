import * as React from 'react';
import { ApiService, ApiServiceFactory, IApiService } from '../api-service-factory';
import { AuthenticationService } from '../../cw-sdk/api/services/General/AuthenticationService';
import { ServiceEnums } from '../../cw-sdk/core/service-enums';
import { WorkOrderSearch } from '../work-order-search.component/work-order-search.component';

export class App extends React.Component<IAppProps, IState> {

    //====================================================================================================
    // PUBLIC
    //====================================================================================================
    public constructor() {
        super();

        this.state = {
            isLoggedIn: false
        }
    }



    /**
     * 
     */
    public onClick_login(): void {
        
        let apiService: IApiService = ApiServiceFactory.getService();
        let authService = new AuthenticationService(apiService);

        apiService.login(this.state.userName, this.state.password)
        .then(response => {
            
            if (!response || !response.Value || response.Status !== ServiceEnums.CoreResponseStatus.Ok) {
                alert('Authentication failed');
                this.setState({isLoggedIn: false})
            } else {

                alert('Logged in!');
                this.setState({isLoggedIn: true})
            }
        })
        .catch(error => {
            console.log(error);
            alert('Authentication Failed');
            this.setState({isLoggedIn: false})
        });
    }



    /**
     * React hook
     */
    public render(): JSX.Element {

        let displayElement: JSX.Element;
        if (!this.state.isLoggedIn) {
            displayElement = (
                <div>
                    <input type="text" placeholder="User Name" onChange={ e => this.setState({ userName: (e.target as HTMLInputElement).value })} />
                    <input type="password" placeholder="password" onChange={ e => this.setState({ password: (e.target as HTMLInputElement).value })} />
                    <button onClick={this.onClick_login.bind(this)}>Login</button>
                </div>
            );
        } else {
            displayElement = <WorkOrderSearch></WorkOrderSearch>
        }


        return displayElement;
    }
}


export interface IAppProps {

}

interface IState {
    isLoggedIn?: boolean;
    password?: string;
    userName?: string;
}

